package ChatStuff;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EtchedBorder;




@SuppressWarnings("serial")
public class ChatPanel extends JPanel {
	private JTextArea outgoing;
	private JTextArea incoming;
	private JButton send;
	private ArrayList<String> UserIPList;
	private ArrayBlockingQueue<String> chatQueue;
	private Integer port;
	private ChatThread chatter;

	EtchedBorder etchedBorder = new EtchedBorder(EtchedBorder.RAISED, Color.green, Color.BLACK);

	public ChatPanel() throws IOException { //might need to pass UserIPList as a parameter.
		setBackground(Color.BLUE);
		setBorder(etchedBorder);
		send = new JButton("send");
		port = 8888;
		setLayout(new GridLayout(2, 1));
		Receiver receiver = new Receiver(8888, incoming, chatQueue);
		receiver.start();
		
		UserIPList = new ArrayList<String>();
		
		UserIPList.add(Inet4Address.getLocalHost().getHostAddress());
		
		outgoing = makeTextArea("outgoing", this);
		incoming = makeTextArea("incoming", this);
		incoming.setEditable(false);
		add(send);
		
		
		send.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					send(outgoing.getText(), UserIPList.get(0), port);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}});
		}
		
		private void send(String msg, String host, int port) throws IOException {
			if (chatter != null && chatter.isGoing()) {
				chatter.halt();
			}
			chatter = new ChatThread(msg, host, port, chatQueue);
			new Receiver(port, incoming, chatQueue).start();
			chatter.start();
		}
	

	
	private JTextArea makeTextArea(String title, ChatPanel where) {
		JTextArea t = new JTextArea();
		JScrollPane scroller = new JScrollPane(t);
		scroller.setBorder(BorderFactory.createTitledBorder(title));
		where.add(scroller); 
		return t;
	}
	public static void main(String[] args) throws IOException {
		JFrame testFrame = new JFrame("Testing");
		
		testFrame.setSize(600, 400);
		testFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		testFrame.setTitle("Testing");
		testFrame.setLayout(new BorderLayout());
		ChatPanel testChat = new ChatPanel();
		testFrame.add(testChat);
		testFrame.setVisible(true);
		
	}


}
