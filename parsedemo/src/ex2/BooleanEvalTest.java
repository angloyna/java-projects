package parsedemo;
import static org.junit.Assert.*;

import org.junit.*;


import edu.hendrix.grambler.ParseException;


public class BooleanEvalTest {
	BooleanEval evaluator;
	
	@Before
	public void setup() {
		evaluator = new BooleanEval();
	}

	@Test
	public void test() throws ParseException {
		assertFalse(evaluator.eval("x or y or z;"));
	}
	
	@Test(expected=ParseException.class)
	public void syntaxError() throws ParseException {
		evaluator.eval("x or y z");
	}

	@Test
	public void bigTest() throws ParseException {
		assertTrue(evaluator.eval("not (true and false) or false;"));
	}
	
	@Test
	public void bigTest2() throws ParseException {
		assertTrue(evaluator.eval("not (true and false) and true;"));
	}
	
	@Test
	public void varTest1() throws ParseException {
		assertTrue(evaluator.eval("x = true;\nx;"));
	}
}
