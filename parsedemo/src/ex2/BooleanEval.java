package ex2;

import java.util.HashSet;
import java.util.Set;

import edu.hendrix.grambler.Grammar;
import edu.hendrix.grambler.ParseException;
import edu.hendrix.grambler.Tree;


public class BooleanEval {
	private Grammar g;
	private Set<String> symbols;
	
	public BooleanEval() {
		g = new BooleanGrammar();
		symbols = new HashSet<String>();
		symbols.add("true");
	}
	
	public boolean eval(String input) throws ParseException {
		return evalTree(g.parse(input));
	}
	
	private boolean evalTree(Tree t) {
		if (t.isNamed("symbol")) {
			return symbols.contains(t.toString());
		} else if (t.getNumChildren() == 1) {
			return evalTree(t.getChild(0));
		} else if (t.isNamed("lines")) {
			evalTree(t.getNamedChild("lines"));
			return evalTree(t.getNamedChild("line"));
		} else if (t.isNamed("line")) {
			return evalTree(t.getChild(0));
		} else if (t.isNamed("assign")) {
			boolean value = evalTree(t.getNamedChild("orExpr"));
			if (value) {
				symbols.add(t.getNamedChild("symbol").toString());
			}
			return value;
		} else if (t.isNamed("orExpr")) {
			boolean left = evalTree(t.getNamedChild("orExpr"));
			boolean right = evalTree(t.getNamedChild("andExpr"));
			return left || right;
		} else if (t.isNamed("andExpr")) {
			boolean left = evalTree(t.getNamedChild("andExpr"));
			boolean right = evalTree(t.getNamedChild("notExpr"));
			return left && right;
		} else if (t.isNamed("notExpr")) {
			return !evalTree(t.getNamedChild("parenExpr"));
		} else if (t.isNamed("parenExpr")) {
			return evalTree(t.getNamedChild("orExpr"));
		} else {
			throw new IllegalArgumentException("Tree has unexpected name: " + t.getName());
		}
	}
}
