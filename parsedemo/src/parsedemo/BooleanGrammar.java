package parsedemo;
public class BooleanGrammar extends edu.hendrix.grambler.Grammar {
    public BooleanGrammar() {
        super();
        addProduction("lines", new String[]{"lines", "cr", "line"}, new String[]{"line"});
        addProduction("line", new String[]{"assign"}, new String[]{"orExpr"});
        addProduction("cr", new String[]{"'\r\n'"}, new String[]{"'\n'"});
        addProduction("assign", new String[]{"symbol", "\"\\s*\"", "'='", "\"\\s*\"", "orExpr"});
        addProduction("orExpr", new String[]{"orExpr", "\"\\s+\"", "'or'", "\"\\s+\"", "andExpr"}, new String[]{"andExpr"});
        addProduction("andExpr", new String[]{"andExpr", "\"\\s+\"", "'and'", "\"\\s+\"", "notExpr"}, new String[]{"notExpr"});
        addProduction("notExpr", new String[]{"'not'", "\"\\s+\"", "parenExpr"}, new String[]{"parenExpr"});
        addProduction("parenExpr", new String[]{"'('", "\"\\s*\"", "orExpr", "\"\\s*\"", "')'"}, new String[]{"symbol"});
        addProduction("symbol", new String[]{"\"[A-Za-z]+\""});
    }
}

