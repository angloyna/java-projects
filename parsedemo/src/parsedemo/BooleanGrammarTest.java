package parsedemo;

import static org.junit.Assert.*;
import org.junit.*;


public class BooleanGrammarTest {
	
	
	@Test
	public void test109() {
		edu.hendrix.grambler.Grammar g = new BooleanGrammar();
		edu.hendrix.grambler.Tree t = g.parse2("not (x or y and (z or w)) and not z");
		assertEquals(false, t.isError());
	}

	@Test
	public void test319() {
		edu.hendrix.grambler.Grammar g = new BooleanGrammar();
		edu.hendrix.grambler.Tree t = g.parse2("x = true\ny = false\nnot (x or y and (z or w)) and not z");
		assertEquals(false, t.isError());
	}

}

