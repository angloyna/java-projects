//This is for SINGLY linked lists
//COMPLETE

public class IntSNode {
	private int value;
	private IntSNode next;
	
	IntSNode() {
		value = 0;
		next = null;
	}
	
	IntSNode( int value ) {
		this.value = value;
		next = null;
	}
	
	// accessor methods
	public int getValue() {
		return value;
	}
	
	public IntSNode getNext() {
		return next;
	}
	
	public void setValue( int val ) {
		value = val;
	}
	
	public void setNext( IntSNode node ) {
		next = node;
	}
}
