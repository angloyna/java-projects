
public class DLList {
	private IntDNode head;
	private IntDNode tail;
	private int list_length;
	
	DLList() {
		head = null;
		tail = null;
		list_length = 0;
	}
	
	public void add( int value ) {  //COMPLETE
		//adds new value at the end (tail) of the list
		IntDNode node = new IntDNode( value );
		list_length++;
		if( head == null ) {  //empty list
			head = tail = node;
		} else {
			tail.setNext(node);
			node.setPrev( tail );
			tail = node;
		}
	}
	
	public void addFirst( int value ) {
		//adds the new value at the head of the list
		IntDNode node = new IntDNode (value);
		list_length++;
		if (head == null) {
			head = tail = node;
		} else {
			node.setNext(head);
			head.setPrev(node);
			head = node;
		}
	}
	
	public void add( int pos, int value ) {
		//pos refers to (array/list) index position
		int n = 0;
		IntDNode cur = head;
		IntDNode node = new IntDNode (value);
		if (pos == 0) {
			addFirst(value);
			return;
		}
		if (pos == (list_length)) {
			add(value);
			return;
		}
		if (pos < list_length) {
			if (pos <= (list_length/2)) {
				while (n < (pos - 1)) {
					cur = cur.getNext();
					n++;
				}
			}
			if (pos > (list_length/2)) {
				cur = tail;
				while (n < (list_length - pos)) {
					cur = cur.getPrev();
					n++;
				}
			}
			node.setNext(cur.getNext());
			cur.setNext(node);
			node.setPrev(cur);
			node.getNext().setPrev(node);
			list_length++;
		}
	}
	
	public void removeFirst() {
		//removes the value at the head of the list
		if (list_length > 1) {
			head = head.getNext();
			head.setPrev(null);
			list_length--;
			return;
		}
		if (list_length == 1) {
			head = tail = null;
			list_length--;
			return;
		}
	}
	
	public void removeLast() {
		//removes the value at the tail of the list
		if (list_length == 1) {
			head = tail = null;
			list_length--;
		}
		if (list_length > 1) {
			tail.getPrev().setNext(null);
			tail = tail.getPrev();
			list_length--;
		}
	}
	
	public void remove( int pos ) {
		//removes the value at index 'pos' if 'pos' is meaningful
		int n = 0;
		IntDNode cur = new IntDNode();
		cur = head;
		if (pos == 0) {
			removeFirst();
		}
		if (pos == (list_length - 1)) {
			removeLast();
		}
		if (pos < (list_length - 1) && (pos > 0)) {
			if (pos <= (list_length / 2)) {
				while (n < (pos - 1)) {
					cur = cur.getNext();
					n++;
				}
			}
			if (pos > (list_length/2)) {
				cur = tail;
				while (n < (list_length - pos)) {
					cur = cur.getPrev();
					n++;
				}
			}
			cur.setNext(cur.getNext().getNext());
			cur.getNext().setPrev(cur);
			list_length--;
		}
	}
	
	public int size() {  //COMPLETE
		return list_length;
	}
	
	public int[] getArray() {
		//creates and returns an array containing the entries in the LList 
		// in the order that they occur
		int[] temp = new int[list_length];
		for( int idx = 0; idx < list_length; idx++ ) { 
			temp[idx] = get(idx); 
		}
		return temp;
	}
	
	public int getFirst() { 
		//return the value at the head of the list
		//if list is empty, return Integer.MAX_VALUE
		if (list_length == 0) {
			return Integer.MAX_VALUE;
		} else {
			return head.getValue();
		}
	}
	
	public int getLast() {
		//return the value at the end(tail) of the list
		//if list is empty, return Integer.MAX_VALUE
		if (list_length == 0) {
			return Integer.MAX_VALUE;
		} else {
			return tail.getValue();
		}
	}
	
	public int get( int pos ) {
		//returns the value in the list at position pos
		//  if "pos" is meaningless, return Integer.MAX_VALUE
		int n = 0;
		IntDNode cur = new IntDNode();
		cur = head;
		if (pos == 0) {
			getFirst();
		}
		if (pos == (list_length - 1)) {
			getLast();
		}
		if ((pos >= list_length) || (pos < 0)) {
			return Integer.MAX_VALUE;
		} else {
			if (pos <= (list_length / 2)) {
				while (n < pos) {
					cur = cur.getNext();
					n++;
				}
			}
			if (pos > (list_length / 2)) {
				cur = tail;
				while (n < ((list_length - pos) - 1)) {
					cur = cur.getPrev();
					n++;
				}
			}
		return cur.getValue();
		}
	}
	
	public void set( int pos, int value ) {
		//sets the value in index 'pos' to 'value' if "pos" is meaningful
		IntDNode cur = new IntDNode();
		cur = head;
		int n = 0;
		if (pos == 0) {
			head.setValue(value);
		}
		if (pos == (list_length - 1)) {
			tail.setValue(value);
		}
		if ((pos > 0) && (pos < (list_length - 1))) {
			if (pos <= (list_length / 2)) {
				while (n < pos) {
					cur = cur.getNext();
					n++;
				}
			}
			if (pos > (list_length / 2)) {
				cur = tail;
				while (n < ((list_length - pos) - 1)) {
					cur = cur.getPrev();
					n++;
				}
			}
			cur.setValue(value);
		}
	}
	
	public void clear() {
		head = tail = null;
		list_length = 0;
	}
	
	public boolean isEmpty() {  //COMPLETE
		return list_length == 0;
	}
	
	public boolean contains( int val ) {
		IntDNode n = head;
		if( n == null ) { return false; }
		while( n != null ) { // while n is not the null reference
			if( n.getValue() == val ) {
				return true;
			}
			n = n.getNext();			
		}
		return false;			
	}
	
	public int indexOf( int val ) {
		int idx = -1;
		int i = 0;
		IntDNode n = head;
		if( n == null ) { 
			return idx;
		} else {
			while (i < list_length) {
				if (n.getValue() == val) {
					return i;
				}
				i++;
				n = n.getNext();
			}
			return idx;
		}
	}
}