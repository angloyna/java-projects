//SINGLY linked list implementation

public class SLList {
	private IntSNode head;
	private IntSNode tail;
	private int list_length;
	
	SLList() { //COMPLETE
		head = null;
		tail = null;
		list_length = 0;
	}
	
	public void add( int value ) {  //COMPLETE  
		//adds new value at the end (tail) of the list
		IntSNode node = new IntSNode( value );
		list_length++;
		if( head == null ) {  //empty list
			head = tail = node;
		} else {
			tail.setNext(node);
			tail = node;
		}
	}
	
	public void addFirst( int value ) { 
		//adds the new value at the head of the list WORKS ON PAPER
		IntSNode node = new IntSNode (value);
		list_length++;
		if (head == null) {
			head = tail = node;
		} else {
			node.setNext(head);
			head = node;
		}
	}
	
	public void add( int pos, int value ) {
		//pos refers to (array/list) index position	WORKS ON PAPER
		int n = 0;
		IntSNode cur = head;
		IntSNode node = new IntSNode (value);
		if (pos == 0) {
			addFirst(value);
			return;
		}
		if (pos == (list_length)) {
			add(value);
			return;
		}
		if (pos < list_length) {
			while (n < (pos - 1)) {
				cur = cur.getNext();
				n++;
			}
			node.setNext(cur.getNext());
			cur.setNext(node);
			list_length++;
		}
	}
		
	
	public void removeFirst() {  
		//removes the value at the head of the list
		// if list is empty, do nothing
		if (list_length > 1) {
			head = head.getNext();
			list_length--;
			return;
		}
		if (list_length == 1) {
			head = tail = null;
			list_length--;
			return;
		}
	
	}
	
	public void removeLast() {
		//removes the value at the tail of the list
		// if list is empty, do nothing
		IntSNode cur = new IntSNode();
		cur = head;
		int n = 0;
		if (list_length == 1) {
			head = tail = null;
			list_length--;
		}
		if (list_length > 1) {
			while (n < (list_length - 2)) {
				cur = cur.getNext();
				n++;
			}
			cur.setNext(null);
			tail = cur;
			list_length--;
		}

	}
	
	public void remove( int pos ) {
		//removes the value at index 'pos' if 'pos' is meaningful
		// otherwise do nothing
		int n = 0;
		IntSNode cur = new IntSNode();
		cur = head;
		if (pos == 0) {
			removeFirst();
		}
		if (pos == (list_length - 1)) {
			removeLast();
		}
		if (pos < (list_length - 1) && (pos > 0)) {
			while (n < (pos - 1)) {
				cur = cur.getNext();
				n++;
			}
			cur.setNext(cur.getNext().getNext());
			list_length--;
		}
	}
	
	public int size() { //COMPLETE
		return list_length;
	}
	
	public int[] getArray() { 
		//creates and returns an array containing the entries in the LList 
		// in the order that they occur
		int[] temp = new int[list_length];
		for( int idx = 0; idx < list_length; idx++ ) { 
			temp[idx] = get(idx); 
		}
		return temp;

	}
	
	public int getFirst() { 
		//return the first value in the list
		//if list is empty, return Integer.MAX_VALUE
		if (list_length == 0) {
			return Integer.MAX_VALUE;
		} else {
			return get(0);
		}
	}
	
	public int getLast() { 
		//return the value at the end(tail) of the list
		// if list is empty, return Integer.MAX_VALUE
		if (list_length == 0) {
			return Integer.MAX_VALUE;
		} else {
			return get((list_length - 1));
		}
	}
	
	public int get( int pos ) {
		//returns the value in the list at position pos
		//  if "pos" is meaningless, return Integer.MAX_VALUE
		int n = 0;
		IntSNode cur = new IntSNode();
		cur = head;
		if ((pos >= list_length) || (pos < 0)) {
			return Integer.MAX_VALUE;
		} else {
			while (n < pos) {
				cur = cur.getNext();
				n++;
			}
			return cur.getValue();
		}
	}
	
	public void set( int pos, int value ) {
		//sets the value in index 'pos' to 'value' if "pos" is meaningful
		//  otherwise do nothing
		IntSNode cur = new IntSNode();
		cur = head;
		int n = 0;
		
		if ((pos >= 0) && (pos < list_length)) {
			while (n < pos) {
				cur = cur.getNext();
				n++;
			}
			cur.setValue(value);
		}
	}
	
	public void clear() { 
		//remove all entries in the list
		head = tail = null;
		list_length = 0;
	}
	
	public boolean isEmpty() {  //COMPLETE
		return list_length == 0;
	}
	
	public boolean contains( int val ) {  //COMPLETE
		IntSNode n = head;
		if( n == null ) { return false; }
		while( n != null ) { // while n is not the null reference
			if( n.getValue() == val ) {
				return true;
			}
			n = n.getNext();			
		}
		return false;			
	}
	
	public int indexOf( int val ) {
		//same as in the DArray assignment (returns -1 if val not found)
		int idx = -1;
		int i = 0;
		IntSNode n = head;
		if( n == null ) { 
			return idx;
		} else {
			while (i < list_length) {
				if (n.getValue() == val) {
					return i;
				}
				i++;
				n = n.getNext();
			}
			return idx;
		}
		

    //complete this method
	}
	
}
