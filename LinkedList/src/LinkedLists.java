import java.util.Random;

public class LinkedLists {

	public static void main(String[] args) {
		
		 //write the code to test your implementations
		System.out.println("Testing the Doubly Linked List");
		DLList d_list = new DLList();							//testing the doubly linked list implementation, these methods work
		LinkedLists my_list = new LinkedLists();				//the same way for the singly linked list
		d_list.add(24);											//populating the list using working add method
		d_list.add(67);
		d_list.add(89);
		d_list.add(105);
		System.out.println("Populating the list with 4 values");
		System.out.println("d_list.add(24)");
		System.out.println("d_list.add(67)");
		System.out.println("d_list.add(89)");
		System.out.println("d_list.add(105)");
		System.out.print("Array after adding 4 values to test getArray() method: ");
		my_list.printArray(d_list.getArray(), 0, 3);			//print populated array, indicates getArray() method is functioning
		System.out.println("Array should have 4 values with each value added at end of list");
		d_list.addFirst(899);									//testing addFirst() method
		System.out.println("Testing addFirst() method");
		System.out.println("d_list.addFirst(899)");
		System.out.print("Array after calling addFirst(): " );
		my_list.printArray(d_list.getArray(), 0, 4);
		System.out.println("List length: " + d_list.size());
		d_list.add(0, 45);
		System.out.println("Testing add(pos, val) method");
		System.out.println("d_list.add(0, 45)");				//testing that the add() method with position parameter works
		System.out.print("array after calling add(pos, val) method: ");
		my_list.printArray(d_list.getArray(), 0, 5);			//in the same way that the addFirst() method does
		System.out.println("List length: " + d_list.size());
		System.out.println("adding at position 0 should work just as addFirst() does.");
		d_list.add(2,78);
		System.out.println("Testing add(pos, val) method to ensure that it adds properly to first half of list");
		System.out.println("d_list.add(2, 78)");				//testing that the add() method with position parameter works
		System.out.print("Array after adding at position 2: ");
		my_list.printArray(d_list.getArray(), 0, 6);			//when inserting a value into the list
		System.out.println("List length: " + d_list.size());
		System.out.println("Testing add(pos, val) method to ensure that it adds properly to second half of list");
		d_list.add(5, 270);
		System.out.println("d_list.add(5, 270)");				//testing that the add() method with position parameter for DDLList works
		System.out.print("Array after adding to position 5: ");
		my_list.printArray(d_list.getArray(), 0, 7);			//even when looping through list from the tail.
		System.out.println("List length: " + d_list.size());
		System.out.println("Testing add(pos, val) method to ensure that it adds to the end of the list just as add() method does");
		d_list.add(8, 99);
		System.out.println("d_list.add(8, 99)");				//testing that the add() method with position parameter works
		System.out.print("Array after attempting to add at position 8: ");
		my_list.printArray(d_list.getArray(), 0, 8);			//in the same way that the add(val) method does when adding to the end
		System.out.println("List length: " + d_list.size());	//of the list
		
		System.out.println("Testing removeFirst() method");
		d_list.removeFirst();									//Testing removeFirst() method
		System.out.println("d_list.removeFirst()");
		System.out.print("Array after calling removeFirst() method: ");
		my_list.printArray(d_list.getArray(), 0, 8);
		System.out.println("List length: " + d_list.size());
		System.out.println("Testing remove() method to ensure it properly removes from first half of list");
		d_list.remove(2);										//Testing remove() method by removing something from first half of list
		System.out.println("d_list.remove(2)");
		System.out.print("Array after removing value at position 2: ");
		my_list.printArray(d_list.getArray(), 0, 8);
		System.out.println("List length: " + d_list.size());
		d_list.remove(5);										//Testing remove() method by removing something from second half of list
		System.out.println("Testing remove() method to ensure it properly removes from second half of list");
		System.out.println("d_list.remove(5)");
		System.out.print("Array after removing from positon 5: ");
		my_list.printArray(d_list.getArray(), 0, 8);
		System.out.println("List length: " + d_list.size());
		d_list.remove(5);										//Testing remove() method by removing last to show that it works
		System.out.println("Testing remove() method to ensure that it removes from list in the same way that removeLast() does.");
		System.out.println("d_list.remove(5)");					//in the same way the removeLast() method works
		System.out.print("Array after removing from position 5: ");
		my_list.printArray(d_list.getArray(), 0, 8);
		System.out.println("List length: " + d_list.size());
		d_list.removeLast();									//Testing removeLast() method
		System.out.println("Testing removeLast() method");
		System.out.println("d_list.removeLast()");
		System.out.print("Array after calling removeLast(): ");
		my_list.printArray(d_list.getArray(), 0, 8);
		System.out.println("List length: " + d_list.size());
		
		System.out.print("Current contents of list: ");
		my_list.printArray(d_list.getArray(), 0, 10);
		System.out.println("Testing the getFirst(), getLast(), and get() methods");
		System.out.println("Calling getFirst()");
		System.out.println("First value in list: " + d_list.getFirst()); //Testing getFirst() method
		System.out.println("Calling getLast()");
		System.out.println("Last value in list: " + d_list.getLast());		//Testing getLast() method
		System.out.println("Calling get() method to find position 2 value, first value, and last value");
		System.out.println("Value at position 2: " + d_list.get(2));		// Testing get() method to make sure it works properly
		System.out.println("First value in list: " + d_list.get(0));			//when finding values at both ends of array as well as matching
		System.out.println("Last value in list: " + d_list.get((d_list.size()) - 1)); //the function of the getFirst() and getLast() methods
		
		System.out.println("Testing the set() method");
		d_list.set(0, 0);									//Testing set() method
		System.out.println("d_list.set(0, 0)");
		System.out.print("Array after setting position 0 to value 0: ");
		my_list.printArray(d_list.getArray(), 0, 8);
		System.out.println("List length: " + d_list.size());
		d_list.set(1, 100);
		System.out.println("d_list.set(1, 100)");
		System.out.print("Array after setting positon 1 to 100: ");
		my_list.printArray(d_list.getArray(), 0, 8);
		System.out.println("List length: " + d_list.size());
		d_list.set(2, 200);								
		System.out.println("d_list.set(2, 200)");
		System.out.print("Array after setting positon 2 to 200: ");
		my_list.printArray(d_list.getArray(), 0, 8);
		System.out.println("List length: " + d_list.size());
		d_list.set(3, 300);									
		System.out.println("d_list.set(3, 300)");
		System.out.print("Array after setting positon 3 to 300: ");
		my_list.printArray(d_list.getArray(), 0, 8);
		System.out.println("List length: " + d_list.size());
		
		System.out.println("Testing the clear method");
		System.out.print("Array before calling clear: ");
		my_list.printArray(d_list.getArray(), 0, 8);
		d_list.clear();									//Testing clear() method
		System.out.println("d_list.clear()");
		System.out.print("Array after calling clear: ");
		my_list.printArray(d_list.getArray(), 0, 8);
		System.out.println("List length: " + d_list.size());
		
		System.out.println("Testing the empty list conditions for each method");
		System.out.print("Current state of Array: ");
		my_list.printArray(d_list.getArray(), 0, 8);
		d_list.set(1, 400);									//Testing empty list conditions for set() method
		System.out.println("d_list.set(1, 400)");
		System.out.print("Array after calling set(): ");
		my_list.printArray(d_list.getArray(), 0, 8);
		System.out.println("List length: " + d_list.size());
		d_list.removeFirst();									//Testing empty list conditions for removeFirst() method
		System.out.println("d_list.removeFirst()");
		System.out.print("Array after calling removeFirst(): ");
		my_list.printArray(d_list.getArray(), 0, 8);
		System.out.println("List length: " + d_list.size());
		d_list.removeLast();									//Testing empty list conditions for removeLast() method
		System.out.println("d_list.removeLast()");
		System.out.print("Array after calling removeLast(): ");
		my_list.printArray(d_list.getArray(), 0, 8);
		System.out.println("List length: " + d_list.size());
		d_list.remove(3);									//Testing empty list conditions for remove() method
		System.out.println("d_list.remove(3)");
		System.out.print("Array after calling remove(3): ");
		my_list.printArray(d_list.getArray(), 0, 8);
		System.out.println("List length: " + d_list.size());
		d_list.get(0);									//Testing empty list conditions for get() method
		System.out.println("d_list.get(0)");
		System.out.print("Array after calling get(0): ");
		System.out.println("Value at position 0: " + d_list.get(0)); //returns MAXVALUE
		
		System.out.println("Testing indexOf() method");
		d_list.add(25);							
		System.out.println("d_list.add(25)");
		d_list.add(67);							
		System.out.println("d_list.add(67)");
		System.out.print("Array after adding two values: ");
		my_list.printArray(d_list.getArray(), 0, 8);
		System.out.println("Index of Value 67: " + d_list.indexOf(67)); //Testing indexOf() method
		System.out.println("Index of Value 788: " + d_list.indexOf(788));
		
		System.out.println("Testing the Singly Linked List");
		SLList s_list = new SLList();							//testing the singly linked list implementation				
		s_list.add(24);											//populating the list using working add method
		s_list.add(67);
		s_list.add(89);
		s_list.add(105);
		System.out.println("Populating the list with 4 values");
		System.out.println("s_list.add(24)");
		System.out.println("s_list.add(67)");
		System.out.println("s_list.add(89)");
		System.out.println("s_list.add(105)");
		System.out.print("Array after adding 4 values to test getArray() method: ");
		my_list.printArray(s_list.getArray(), 0, 3);			//print populated array, indicates getArray() method is functioning
		System.out.println("Array should have 4 values with each value added at end of list");
		
		
		s_list.addFirst(899);									//testing addFirst() method
		System.out.println("Testing addFirst() method");
		System.out.println("s_list.addFirst(899)");
		System.out.print("Array after calling addFirst(): " );
		my_list.printArray(s_list.getArray(), 0, 4);
		System.out.println("List length: " + s_list.size());
		s_list.add(0, 45);
		System.out.println("Testing add(pos, val) method");
		System.out.println("s_list.add(0, 45)");				//testing that the add() method with position parameter works
		System.out.print("array after calling add(pos, val) method: ");
		my_list.printArray(s_list.getArray(), 0, 5);			//in the same way that the addFirst() method does
		System.out.println("List length: " + s_list.size());
		System.out.println("adding at position 0 should work just as addFirst() does.");
		s_list.add(2,78);
		System.out.println("Testing add(pos, val) method to ensure that it inserts values properly");
		System.out.println("s_list.add(2, 78)");				//testing that the add() method with position parameter works
		System.out.print("Array after adding at position 2: ");
		my_list.printArray(s_list.getArray(), 0, 6);			//when inserting a value into the list
		System.out.println("List length: " + s_list.size());
		System.out.println("Testing add(pos, val) method to ensure that it adds to the end of the list just as add() method does");
		s_list.add(s_list.size(), 99);
		System.out.println("s_list.add(s_list.size(), 99)");				//testing that the add() method with position parameter works
		System.out.print("Array after attempting to add at end of list: ");
		my_list.printArray(s_list.getArray(), 0, 8);			//in the same way that the add(val) method does when adding to the end
		System.out.println("List length: " + s_list.size());	//of the list
		
		System.out.println("Testing removeFirst() method");
		s_list.removeFirst();									//Testing removeFirst() method
		System.out.println("s_list.removeFirst()");
		System.out.print("Array after calling removeFirst() method: ");
		my_list.printArray(s_list.getArray(), 0, 8);
		System.out.println("List length: " + s_list.size());
		System.out.println("Testing remove() method to ensure it properly removes from middle of list");
		s_list.remove(2);										
		System.out.println("s_list.remove(2)");
		System.out.print("Array after removing value at position 2: ");
		my_list.printArray(s_list.getArray(), 0, 8);
		System.out.println("List length: " + s_list.size());
		s_list.remove((s_list.size() - 1));										//Testing remove() method by removing last to show that it works
		System.out.println("Testing remove() method to ensure that it removes from list in the same way that removeLast() does.");
		System.out.println("d_list.remove(s_list.size() - 1)");					//in the same way the removeLast() method works
		System.out.print("Array after removing from end of list: ");
		my_list.printArray(s_list.getArray(), 0, 8);
		System.out.println("List length: " + s_list.size());
		s_list.removeLast();									//Testing removeLast() method
		System.out.println("Testing removeLast() method");
		System.out.println("s_list.removeLast()");
		System.out.print("Array after calling removeLast(): ");
		my_list.printArray(s_list.getArray(), 0, 8);
		System.out.println("List length: " + s_list.size());
		
		System.out.print("Current contents of list: ");
		my_list.printArray(s_list.getArray(), 0, 10);
		System.out.println("Testing the getFirst(), getLast(), and get() methods");
		System.out.println("Calling getFirst()");
		System.out.println("First value in list: " + s_list.getFirst()); //Testing getFirst() method
		System.out.println("Calling getLast()");
		System.out.println("Last value in list: " + s_list.getLast());		//Testing getLast() method
		System.out.println("Calling get() method to find position 2 value, first value, and last value");
		System.out.println("Value at position 2: " + s_list.get(2));		// Testing get() method to make sure it works properly
		System.out.println("First value in list: " + s_list.get(0));			//when finding values at both ends of array as well as matching
		System.out.println("Last value in list: " + s_list.get((s_list.size()) - 1)); //the function of the getFirst() and getLast() methods
		
		System.out.println("Testing the set() method");
		s_list.set(0, 0);									//Testing set() method
		System.out.println("s_list.set(0, 0)");
		System.out.print("Array after setting position 0 to value 0: ");
		my_list.printArray(s_list.getArray(), 0, 8);
		System.out.println("List length: " + s_list.size());
		
		System.out.println("Testing the clear method");
		System.out.print("Array before calling clear: ");
		my_list.printArray(s_list.getArray(), 0, 8);
		s_list.clear();									//Testing clear() method
		System.out.println("s_list.clear()");
		System.out.print("Array after calling clear: ");
		my_list.printArray(s_list.getArray(), 0, 8);
		System.out.println("List length: " + s_list.size());
		
		System.out.println("Testing the empty list conditions for each method");
		System.out.print("Current state of Array: ");
		my_list.printArray(s_list.getArray(), 0, 8);
		s_list.set(1, 400);									//Testing empty list conditions for set() method
		System.out.println("s_list.set(1, 400)");
		System.out.print("Array after calling set(): ");
		my_list.printArray(s_list.getArray(), 0, 8);
		System.out.println("List length: " + s_list.size());
		s_list.removeFirst();									//Testing empty list conditions for removeFirst() method
		System.out.println("s_list.removeFirst()");
		System.out.print("Array after calling removeFirst(): ");
		my_list.printArray(s_list.getArray(), 0, 8);
		System.out.println("List length: " + s_list.size());
		s_list.removeLast();									//Testing empty list conditions for removeLast() method
		System.out.println("s_list.removeLast()");
		System.out.print("Array after calling removeLast(): ");
		my_list.printArray(s_list.getArray(), 0, 8);
		System.out.println("List length: " + s_list.size());
		s_list.remove(3);									//Testing empty list conditions for remove() method
		System.out.println("s_list.remove(3)");
		System.out.print("Array after calling remove(3): ");
		my_list.printArray(s_list.getArray(), 0, 8);
		System.out.println("List length: " + s_list.size());
		s_list.get(0);									//Testing empty list conditions for get() method
		System.out.println("s_list.get(0)");
		System.out.print("Array after calling get(0): ");
		System.out.println("Value at position 0: " + s_list.get(0)); //returns MAXVALUE
		
		System.out.println("Testing indexOf() method");
		s_list.add(25);							
		System.out.println("s_list.add(25)");
		s_list.add(67);							
		System.out.println("s_list.add(67)");
		System.out.print("Array after adding two values: ");
		my_list.printArray(s_list.getArray(), 0, 8);
		System.out.println("Index of Value 67: " + s_list.indexOf(67)); //Testing indexOf() method
		System.out.println("Index of Value 788: " + s_list.indexOf(788));
	}
	
	public void printArray( int[] array, int startIdx, int endIdx ) {
		if( startIdx < 0 ) { startIdx = 0; }
		if( endIdx >= array.length ) {endIdx = array.length - 1; }
		for( int idx = startIdx; idx <= endIdx; idx++ ) {
			System.out.print( array[ idx ] + " ");
		}
		System.out.println(); 
	}
	
	public int getRandomBetween( int minVal, int maxVal, Random rnd ) {
		if( maxVal < minVal ) { // just in case the numbers were accidently switched
			int temp = maxVal;
			maxVal = minVal;
			minVal = temp;
		}
		return rnd.nextInt( maxVal - minVal + 1 ) + minVal;
	}

}
