//This is for DOUBLY linked lists
//COMPLETE

public class IntDNode {
	private int value;
	private IntDNode next;
	private IntDNode prev;  // "previous", allows for linear traversal in opposite order (tail to head)
	
	IntDNode() {
		value = 0;
		prev = next = null;
	}
	
	IntDNode( int value ) {
		this.value = value;
		prev = next = null;
	}
	
	// accessor methods
	public int getValue() {
		return value;
	}
	
	public IntDNode getNext() {
		return next;
	}
	
	public IntDNode getPrev() {
		return prev;
	}
	
	public void setValue( int val ) {
		value = val;
	}
	
	public void setNext( IntDNode node ) {
		next = node;
	}
	
	public void setPrev( IntDNode node ) {
		prev = node;
	}
}
