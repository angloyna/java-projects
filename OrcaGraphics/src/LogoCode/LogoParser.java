package LogoCode;

public class LogoParser extends edu.hendrix.grambler.Grammar {
    public LogoParser() {
        super();
        addProduction("lines", new String[]{"lines", "cr", "line"}, new String[]{"line"});
        addProduction("cr", new String[]{"'\r\n'"}, new String[]{"'\n'"});
        addProduction("line", new String[]{"assign"}, new String[]{"command"});
        addProduction("list", new String[]{"list", "item"}, new String[]{"item"});
        addProduction("item", new String[]{"assign"}, new String[]{"assign", "\"\\s*\""});
        addProduction("assign", new String[]{"symbol", "\"\\s*\"", "'='", "\"\\s*\"", "orExpr"}, new String[]{"repeat"});
        addProduction("repeat", new String[]{"\"\\s*\"", "'repeat'", "\"\\s*\"", "orExpr", "\"\\s+\"", "bracklst"}, new String[]{"procedure"}, new String[]{"'repeat'", "\"\\s*\"", "var", "\"\\s*\"", "bracklst"}, new String[]{"bracklst"});
        addProduction("procedure", new String[]{"'to'", "\"\\s+\"", "name", "\"\\s+\"", "parameter", "\"\\s+\"", "bracklst"}, new String[]{"'to'", "\"\\s+\"", "name", "\"\\s+\"", "bracklst"}, new String[]{"parameter"});
        addProduction("parameter", new String[]{"':'", "word"}, new String[]{"number"}, new String[]{"command"});
        addProduction("command", new String[]{"'pendown'"}, new String[]{"'pd'"}, new String[]{"'penup'"}, new String[]{"'pu'"}, new String[]{"'home'"}, new String[]{"'clearscreen'"}, new String[]{"'cs'"}, new String[]{"'showturtle'"}, new String[]{"'st'"}, new String[]{"'hideturtle'"}, new String[]{"'ht'"}, new String[]{"argDirective"});
        addProduction("argDirective", new String[]{"argCommand", "\"\\s+\"", "orExpr"}, new String[]{"argCommand"});
        addProduction("argCommand", new String[]{"'forward'"}, new String[]{"'fd'"}, new String[]{"'back'"}, new String[]{"'bk'"}, new String[]{"'left'"}, new String[]{"'lt'"}, new String[]{"'right'"}, new String[]{"'rt'"}, new String[]{"repeat"});
        addProduction("bracklst", new String[]{"'['", "\"\\s*\"", "list", "\"\\s*\"", "']'"}, new String[]{"orExpr"});
        addProduction("orExpr", new String[]{"orExpr", "\"\\s+\"", "'or'", "\"\\s+\"", "andExpr"}, new String[]{"andExpr"});
        addProduction("andExpr", new String[]{"andExpr", "\"\\s*\"", "'and'", "\"\\s*\"", "var"}, new String[]{"notExpr"});
        addProduction("notExpr", new String[]{"'not'", "\"\\s*\"", "var"}, new String[]{"lessExpr"});
        addProduction("lessExpr", new String[]{"lessExpr", "\"\\s+\"", "'<'", "\"\\s+\"", "greatExpr"}, new String[]{"greatExpr"});
        addProduction("greatExpr", new String[]{"greatExpr", "\"\\s+\"", "'>'", "\"\\s+\"", "greatEqExpr"}, new String[]{"greatEqExpr"});
        addProduction("greatEqExpr", new String[]{"greatEqExpr", "\"\\s+\"", "'>='", "\"\\s+\"", "lessEqExpr"}, new String[]{"lessEqExpr"});
        addProduction("lessEqExpr", new String[]{"lessEqExpr", "\"\\s+\"", "'<='", "\"\\s+\"", "subExpr"}, new String[]{"subExpr"});
        addProduction("subExpr", new String[]{"subExpr", "\"\\s*\"", "'-'", "\"\\s*\"", "addExpr"}, new String[]{"addExpr"});
        addProduction("addExpr", new String[]{"addExpr", "\"\\s*\"", "'+'", "\"\\s*\"", "DivExpr"}, new String[]{"DivExpr"});
        addProduction("DivExpr", new String[]{"DivExpr", "\"\\s*\"", "'/'", "\"\\s*\"", "MultExpr"}, new String[]{"MultExpr"});
        addProduction("MultExpr", new String[]{"MultExpr", "\"\\s*\"", "'*'", "\"\\s*\"", "ParenExpr"}, new String[]{"ParenExpr"});
        addProduction("ParenExpr", new String[]{"'('", "\"\\s*\"", "item", "\"\\s*\"", "')'"}, new String[]{"orExpr"}, new String[]{"var"});
        addProduction("var", new String[]{"symbol"}, new String[]{"word"}, new String[]{"number"});
        addProduction("name", new String[]{"\"[A-Za-z]+\""});
        addProduction("symbol", new String[]{"\"[A-Za-z]\""}, new String[]{"word"});
        addProduction("word", new String[]{"\"[A-Za-z]+\""});
        addProduction("number", new String[]{"\"[0-9]+\""});
    }
}

