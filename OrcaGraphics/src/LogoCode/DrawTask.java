package LogoCode;

import java.util.HashSet;
import java.util.TimerTask;

import UnderlyingOBJSTRUCT.OrcaManager;

public class DrawTask extends TimerTask {
	int i;
	OrcaManager om;
	String[] lines; 
	HashSet<String> NoArgList;
	HashSet<String> ArgList;
	
	public DrawTask(HashSet<String> NoArgList, HashSet<String> ArgList, OrcaManager om, String[] lines, int i) {
		this.i = i;
		this.om = om;
		this.lines = lines;
		this.NoArgList = NoArgList;
		this.ArgList = ArgList;
	}
	@Override
	public void run() {
		String ArgInput[] = lines[i].split(" ");
		String command = ArgInput[0];
		if (isNoArgDirective(command)) {
			tellOrca(command);
		} else if (isArgDirective(command)) {
			int argument = Integer.parseInt(ArgInput[1]);
			moveOrca(command, argument);
		}
		
	}
	
	private void tellOrca(String string) {
		if (isHome(string)) {
			om.orca.resetPosition();
		} else if (isPenUp(string)) {
			om.orca.penUp(om);	
		} else if (isPenDown(string)) {
			om.orca.penDown(om);
		} else if (isClearScreen(string)) {
			om.orca.clearscreen();
		} else if (isShowTurtle(string)) {
			om.orca.showTurtle();
		} else if (isHideTurtle(string)) {
			om.orca.hideTurtle();
		}
		om.gpanel.repaint();
		
	}

	private void moveOrca(String string, int arg) {
		if (isForward(string)) {
			om.orca.moveForward(om, arg);
		} else if (isBack(string)) {
			om.orca.moveBack(om, arg);	
		} else if (isLeft(string)) {
			om.orca.turnLeft(om, arg);
		} else if (isRight(string)) {
			om.orca.turnRight(om, arg);
		}
		om.gpanel.repaint();
	}
	
	private boolean isForward(String string) {
		return string.equals("forward") || string.equals("fd");
	}
	
	private boolean isBack(String string) {
		return string.equals("back") || string.equals("bk");
	}
	
	private boolean isLeft(String string) {
		return string.equals("left") || string.equals("lt");
	}
	
	private boolean isRight(String string) {
		return string.equals("right") || string.equals("rt");
	}


	private boolean isNoArgDirective(String string) {
		return NoArgList.contains(string);
	}
	
	private boolean isArgDirective(String string) {
		return ArgList.contains(string);
	}
	private boolean isHome(String string) {
		return (string.equals("home"));
	}
	
	private boolean isPenUp(String string) {
		return (string.equals("penup") || string.equals("pu"));
	}
	
	private boolean isPenDown(String string) {
		return (string.equals("pendown") || (string.equals("pd")));
	}
	
	private boolean isClearScreen(String string) {
		return (string.equals("clearscreen") || string.equals("cs"));
	}
	
	private boolean isShowTurtle(String string) {
		return string.equals("showturtle") || string.equals("st");
	}
	
	private boolean isHideTurtle(String string) {
		return string.equals("hideturtle") || string.equals("ht");
	}
}