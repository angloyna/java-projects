package LogoCode;


import edu.hendrix.grambler.Grammar;
import edu.hendrix.grambler.ParseException;
import edu.hendrix.grambler.Tree;

import java.util.HashMap;

import javax.swing.JOptionPane;

import UnderlyingOBJSTRUCT.OrcaManager;


public class LogoEvaluator {
	private Grammar g;
	private HashMap<String, String> symbols;
	private String left, right, value, name;
	private HashMap<String, Tree> procedures;
	private Tree restOfTree;
	private OrcaManager om;
	
	public LogoEvaluator() {
		g = new LogoParser();
		symbols = new HashMap<String, String>();
		procedures = new HashMap<String, Tree>();
	}
	
	public String eval(String input) throws ParseException {
		return evalTree(g.parse(input));
	}
	private String evalTree(Tree t) {
		if (t.isNamed("symbol")) {
			return symbols.get(t.toString());
		} else if (t.isNamed("number")) {
			return t.toString();
		} else if (t.isNamed("argCommand")) {
			return t.toString();
		} else if (t.isNamed("name")) {
			return evalTree(procedures.get(t.toString()));
		} else if (t.isNamed("word")) {
			return symbols.get(t.toString());
		} else if (t.getNumChildren() == 1) {
			return evalTree(t.getChild(0));
		} else if (t.isNamed("lines")) {
			return evalTree(t.getNamedChild("lines")) + 
					'\n' + evalTree(t.getNamedChild("line"));
		} else if (t.isNamed("line")) {
			return evalTree(t.getChild(0));
		} else if (t.isNamed("list")) {
			return evalTree(t.getNamedChild("list")) + 
					' ' + evalTree(t.getNamedChild("item"));
		} else if (t.isNamed("item")) {
			return evalTree(t.getChild(0));
		} else if (t.isNamed("assign")) {
			value = evalTree(t.getNamedChild("orExpr"));
			symbols.put(t.getNamedChild("symbol").toString(), value);
			return value;	
		} else if (t.isNamed("procedure")) {
			name = t.getNamedChild("name").toString();
			restOfTree = t.getNamedChild("bracklst");
			procedures.put(name, restOfTree);
			return evalTree(restOfTree);
		}  else if (t.isNamed("repeat")) {
			name = t.getNamedChild("repeat").toString();
			left = evalTree(t.getNamedChild("var"));
			right = evalTree(t.getNamedChild("bracklst"));
			return name + ' ' + left + ' ' + right;
		} else if (t.isNamed("command")) {
			return t.toString();	
		} else if (t.isNamed("argDirective")) {
			left = t.getNamedChild("argCommand").toString();
			right = evalTree(t.getNamedChild("orExpr"));
			return left + ' ' + right;
		} else if (t.isNamed("bracklst")) {
			return evalTree(t.getNamedChild("list"));
		} else if (t.isNamed("orExpr")) {
			left = evalTree(t.getNamedChild("orExpr"));
			right = evalTree(t.getNamedChild("andExpr"));
			return String.valueOf(Boolean.parseBoolean(left) || 
					Boolean.parseBoolean(right));
		} else if (t.isNamed("andExpr")) {
			left = evalTree(t.getNamedChild("andExpr"));
			right = evalTree(t.getNamedChild("var"));
			return String.valueOf(Boolean.parseBoolean(left) & 
					Boolean.parseBoolean(right));	
		}
		else if (t.isNamed("notExpr")) {
			return String.valueOf(!Boolean.parseBoolean(evalTree(t.getNamedChild("var"))));
		} else if (t.isNamed("subExpr")) {
			left = evalTree(t.getNamedChild("subExpr"));
			right = evalTree(t.getNamedChild("addExpr"));
			return String.valueOf(Integer.parseInt(left) - 
					Integer.parseInt(right));
		} else if (t.isNamed("addExpr")) {
			left = evalTree(t.getNamedChild("addExpr"));
			right = evalTree(t.getNamedChild("DivExpr"));
			return String.valueOf(Integer.parseInt(left) + 
					Integer.parseInt(right));
		} else if (t.isNamed("DivExpr")) {
			left = evalTree(t.getNamedChild("DivExpr"));
			right = evalTree(t.getNamedChild("MultExpr"));
			return String.valueOf(Integer.parseInt(left) / 
					Integer.parseInt(right));
		} else if (t.isNamed("MultExpr")) {
			left = evalTree(t.getNamedChild("MultExpr"));
			right = evalTree(t.getNamedChild("ParenExpr"));
			return String.valueOf(Integer.parseInt(left) * 
					Integer.parseInt(right));
		}	else if (t.isNamed("lessExpr")) {
			left = evalTree(t.getNamedChild("lessExpr"));
			right = evalTree(t.getNamedChild("greatExpr"));
			return String.valueOf(Integer.parseInt(left) < 
					Integer.parseInt(right));	
		}	else if (t.isNamed("greatExpr")) {
			left = evalTree(t.getNamedChild("greatExpr"));
			right = evalTree(t.getNamedChild("greatEqExpr"));
			return String.valueOf(Integer.parseInt(left) > 
					Integer.parseInt(right));	
		}	else if (t.isNamed("greatEqExpr")) {
			left = evalTree(t.getNamedChild("greatEqExpr"));
			right = evalTree(t.getNamedChild("lessEqExpr"));
			return String.valueOf(Integer.parseInt(left) >= 
					Integer.parseInt(right));	
		}	else if (t.isNamed("lessEqExpr")) {
			left = evalTree(t.getNamedChild("lessEqExpr"));
			right = evalTree(t.getNamedChild("subExpr"));
			return String.valueOf(Integer.parseInt(left) <= 
					Integer.parseInt(right));	
		}	else if (t.isNamed("ParenExpr")) {
			return evalTree(t.getNamedChild("item"));
		} else {
			JOptionPane.showMessageDialog(null, "Commands could not be execute. Reconsider your syntax.");
			throw new IllegalArgumentException("Tree has unexpected name: " + 
		t.getName());
		}
	}
}
