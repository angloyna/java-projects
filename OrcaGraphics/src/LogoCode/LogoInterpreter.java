package LogoCode;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Timer;

import UnderlyingOBJSTRUCT.OrcaManager;


public class LogoInterpreter {
		String rawCommands;
		OrcaManager orcaManager;
		Timer timer;
		int scheduletime;
		int scheduleincrem;
		HashSet<String> NoArgList;
		HashSet<String> ArgList;
		
		public LogoInterpreter(OrcaManager om) {
			orcaManager = om;
			scheduleincrem = 1000;
			NoArgList = new HashSet<String>(Arrays.asList("pendown", "penup", "pu", "pd", "home", "clearscreen", "cs", 
					"showturtle", "hideturtle", "st", "ht"));
			ArgList = new HashSet<String>(Arrays.asList("forward", "fd", "back", "bk", "left", "lt", "right", "rt"));
		}
		
		public void InterpretInput(String evalResult) {
			timer = new Timer();
			scheduletime = orcaManager.getTimerWait();
			orcaManager.setTimer(timer);
			rawCommands = evalResult;
			String lines[] = rawCommands.split("\\r?\\n");
			for (int i = 0; i < lines.length; i++) {
					orcaManager.getTimer().schedule(new DrawTask(NoArgList, ArgList, orcaManager, lines, i), scheduletime);
					scheduletime += orcaManager.getTimerWait();
			}

		}
}
