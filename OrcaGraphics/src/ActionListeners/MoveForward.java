package ActionListeners;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import UnderlyingOBJSTRUCT.Orca;
import UnderlyingOBJSTRUCT.OrcaManager;


public class MoveForward implements ActionListener {
	Orca orca;
	Graphics g;
	private OrcaManager orcaManager;

	public MoveForward(OrcaManager om) {
		this.orcaManager = om;
		this.orca = orcaManager.getOrca();
		this.g = orcaManager.getGraphics();
	}

@Override
	public void actionPerformed(ActionEvent arg0) {
		orca.moveForward(orcaManager, 25);
		orcaManager.getGPanel().repaint();
	}
}