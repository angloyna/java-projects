package ActionListeners;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import UnderlyingOBJSTRUCT.Orca;
import UnderlyingOBJSTRUCT.OrcaManager;


public class TurnLeft implements ActionListener {
	Orca orca;
	Graphics g;
	OrcaManager orcaManager;

	public TurnLeft(OrcaManager om) {
		this.orcaManager = om;
		this.orca = orcaManager.getOrca();
		this.g = orcaManager.getGraphics();
	}

@Override
	public void actionPerformed(ActionEvent arg0) {
		orca.turnLeft(orcaManager, 5);
		orca.moveForward(orcaManager, 25);
		orcaManager.getGPanel().repaint();
	}
}