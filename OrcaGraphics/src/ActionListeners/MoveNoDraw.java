package ActionListeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import UnderlyingOBJSTRUCT.OrcaManager;


public class MoveNoDraw implements ActionListener {
	private OrcaManager orcaManager;
	private JButton penControl;
	
	public MoveNoDraw(OrcaManager om, JButton penControl) {
		orcaManager = om;
		this.penControl = penControl;
	}

	@Override
		public void actionPerformed(ActionEvent e) {
			orcaManager.getACPanel().setMode(orcaManager.getACPanel().getMode().advance());
			penControl.setText(orcaManager.getACPanel().getMode().advance().toString());
		}
		
}