package ActionListeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import UnderlyingOBJSTRUCT.OrcaManager;


public class UndoLastMove implements ActionListener {
	private OrcaManager orcaManager;
	
	public UndoLastMove(OrcaManager om) {
		this.orcaManager = om;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		orcaManager.getOrca().undoLastMove();
		orcaManager.getGPanel().repaint();
		
	}
}