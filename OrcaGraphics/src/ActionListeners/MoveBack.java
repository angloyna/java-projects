package ActionListeners;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import UnderlyingOBJSTRUCT.Orca;
import UnderlyingOBJSTRUCT.OrcaManager;


public class MoveBack implements ActionListener {
	Orca orca;
	Graphics g;
	private OrcaManager orcaManager;

	public MoveBack(OrcaManager om) {
		this.orcaManager = om;
		this.orca = orcaManager.getOrca();
		this.g = orcaManager.getGraphics();
	}

@Override
	public void actionPerformed(ActionEvent arg0) {
		orca.moveBack(orcaManager, 25);
		orcaManager.getGPanel().repaint();
	}
}