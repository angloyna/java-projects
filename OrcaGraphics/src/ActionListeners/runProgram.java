package ActionListeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextArea;

import LogoCode.LogoEvaluator;
import UnderlyingOBJSTRUCT.OrcaManager;

import edu.hendrix.grambler.ParseException;

public class runProgram implements ActionListener {
	private OrcaManager orcaManager;
	private JTextArea programText;
	
	public runProgram(OrcaManager om, JTextArea programText) {
		orcaManager = om;
		this.programText = programText;
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			orcaManager.runProgram();
			orcaManager.getInterpreter().InterpretInput(new LogoEvaluator().eval(programText.getText()));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	
}