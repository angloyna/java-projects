package ActionListeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import UnderlyingOBJSTRUCT.OrcaManager;


public class ClearScreen implements ActionListener {
	private OrcaManager orcaManager;
	
	public ClearScreen(OrcaManager om) {
		orcaManager = om;
	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
		orcaManager.getOrca().clearOrcaPath();
		orcaManager.getGPanel().repaint();
		orcaManager.getOrca().resetPosition();
		orcaManager.resetDirection();
		
	}
}