package ActionListeners;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import UnderlyingOBJSTRUCT.OrcaManager;

public class ChangeSpeed implements ChangeListener {
	private OrcaManager orcaManager;
	
	public ChangeSpeed(OrcaManager om) {
		this.orcaManager = om;
	}
	@Override
	public void stateChanged(ChangeEvent arg0) {
		if (isOne()) {
			setSpeed(2000);
		} else if (isTwo()) {
			setSpeed(1500);
		} else if (isThree()) {
			setSpeed(1000);
		} else if (isFour()) {
			setSpeed(500);
		} else if (isFive()) {
			setSpeed(0);
		}

	}
	
	public boolean isOne() {
		if (getSpeedSliderVal() == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isTwo() {
		if (getSpeedSliderVal() == 2) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isThree() {
		if (getSpeedSliderVal() == 3) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isFour() {
		if (getSpeedSliderVal() == 4) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isFive() {
		if (getSpeedSliderVal() == 5) {
			return true;
		} else {
			return false;
		}
	}
	
	public int getSpeedSliderVal() {
		return orcaManager.getSpeedSlider().getValue();
	}
	
	public void setSpeed(int speed) {
		orcaManager.setTimerWait(speed);
	}

}
