package ActionListeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import UnderlyingOBJSTRUCT.OrcaManager;


public class stopProgram implements ActionListener {
	private OrcaManager om;
	
	public stopProgram(OrcaManager om) {
		this.om = om;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		om.haltProgram();

	}

}
