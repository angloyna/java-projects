package ActionListeners;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import UnderlyingOBJSTRUCT.Orca;
import UnderlyingOBJSTRUCT.OrcaManager;


public class TurnRight implements ActionListener {
	Orca orca;
	Graphics g;
	private OrcaManager orcaManager;

	public TurnRight(OrcaManager om) {
		this.orcaManager = om;
		this.orca = orcaManager.getOrca();
		this.g = orcaManager.getGraphics();
	}
//WHY 90 AND 25
@Override
	public void actionPerformed(ActionEvent arg0) {
		orca.turnRight(orcaManager, 5);
		orca.moveForward(orcaManager, 25);
		orcaManager.getGPanel().repaint();
	}
}