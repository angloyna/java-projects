package orcagraphics;

import java.awt.*;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.EtchedBorder;

import UnderlyingOBJSTRUCT.Line;
import UnderlyingOBJSTRUCT.Orca;
import UnderlyingOBJSTRUCT.OrcaManager;

@SuppressWarnings("serial")
public class GraphicsPanel extends JPanel {
	EtchedBorder etchedBorder = new EtchedBorder(EtchedBorder.RAISED, Color.green, Color.BLACK);
	Orca orca;
	ArrayList<Line> LineList;
	OrcaManager orcaManager;
	Graphics g;
	
	public GraphicsPanel(Graphics g, OrcaManager om) {
		super();
		setPreferredSize(new Dimension(450,450));
		setBackground(Color.blue);
		setBorder(etchedBorder);
		orca = new Orca(200, 200, 45, 45);
		setVisible(true);
		om.setOrca(orca);
		orcaManager = om;
		
		this.g = g;
		
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		orca.draw(g);
	}
	
	public void changeLineColor(String color) {
		g.setColor(Color.getColor(color));
	}
}
