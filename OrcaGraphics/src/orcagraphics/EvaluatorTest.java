package orcagraphics;

import static org.junit.Assert.*;

import javax.swing.JOptionPane;

import org.junit.Before;
import org.junit.Test;

import LogoCode.LogoEvaluator;
import UnderlyingOBJSTRUCT.OrcaManager;

import edu.hendrix.grambler.ParseException;

public class EvaluatorTest {
	private LogoEvaluator eval;
	private OrcaGraphics orcaGraphics;
	
	@Before
	public void setup() {
		eval = new LogoEvaluator();
		orcaGraphics = new OrcaGraphics();
	}
	
	@Test
	public void test1() throws ParseException {
		assertEquals("6", eval.eval("3 * 2"));
	}
	
	@Test
	public void test2() throws ParseException {
		assertEquals("5", eval.eval("3 + 2"));
	}
	
	@Test (expected=ParseException.class)
	public void testEvalException() throws ParseException {
		eval.eval("words;");
	}
	
	@Test 
	public void testLessThan() throws ParseException {
		assertEquals("false", eval.eval("5 < 4"));
	}
	
	@Test
	public void test3() throws ParseException {
		assertEquals("1", eval.eval("3 - 2"));
	}
	
	@Test
	public void testPenup() throws ParseException {
		assertEquals("penup", eval.eval("penup"));
	}
	
	@Test
	public void testGreaterEq() throws ParseException {
		assertEquals("true", eval.eval("3 >= 3"));
	}
	
	@Test
	public void testGreaterEq2() throws ParseException {
		assertEquals("true", eval.eval("5 >= 3"));
	}
	
	@Test
	public void testLessEq() throws ParseException {
		assertEquals("true", eval.eval("3 <= 3"));
	}
	
	@Test
	public void testLessEq2() throws ParseException {
		assertEquals("true", eval.eval("1 <= 3"));
	}
	
	@Test
	public void test4() throws ParseException {
		assertEquals("3", eval.eval("10 / 3"));
	}

	@Test
	public void test5() throws ParseException {
		assertEquals("11", eval.eval("9 + 2"));
	}
	
	@Test
	public void testAdd() throws ParseException {
		assertEquals("6", eval.eval("2 + 4"));
	}

	@Test
	public void testAssignment() throws ParseException {
		assertEquals("4", eval.eval("x = 4"));
	}
	
	@Test
	public void test7() throws ParseException {
		assertEquals("14", eval.eval("(2 + 5) * 2"));
	}
	
	@Test
	public void test8() throws ParseException {
		assertEquals("14", eval.eval("(2 + 5) * 2"));
	}
	
	@Test
	public void test9() throws ParseException {
		assertEquals("30", eval.eval("((7 - 3) + 2) * 5"));
	}
	
	@Test
	public void test10() throws ParseException {
		assertEquals("forward 10",(eval.eval("forward 10")));
	}
	
	@Test
	public void test11() throws ParseException {
		assertEquals("fd 13 rt 76",(eval.eval("[fd (8+5) rt 76]")));
	}
	
	@Test
	public void test12() throws ParseException {
		assertEquals("repeat 8 fd 8 rt 76", (eval.eval("repeat 8 [fd 8 rt 76]")));
	}
	
	@Test
	public void testAndExpr() throws ParseException {
		assertEquals("true", (eval.eval("9 and 9")));
	}
	
	@Test
	public void testOrExpr() throws ParseException {
		assertEquals("true", (eval.eval("9 or 9")));
	}
	
	@Test
	public void testNotExpr() throws ParseException {
		assertEquals("true", (eval.eval("not 9")));
	}
	
	@Test
	public void testProcedure() throws ParseException {
		assertEquals("fd 9", (eval.eval("to make :size [fd 9]")));
	}
	
	@Test
	public void testSymbol() throws ParseException {
		assertEquals("4", (eval.eval("x = 4")));
	}
	
	@Test
	public void testGreater() throws ParseException {
		assertEquals("true", eval.eval("5 > 2"));
	}


}
