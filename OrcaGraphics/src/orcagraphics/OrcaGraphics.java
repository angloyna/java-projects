package orcagraphics;

import java.awt.*;

import javax.swing.*;

import LogoCode.LogoInterpreter;
import UnderlyingOBJSTRUCT.OrcaManager;

@SuppressWarnings("serial")
public class OrcaGraphics extends JFrame {
	private GraphicsPanel graphicsPanel;
	private ArrowControlPanel ACPanel;
	private Graphics2D g;
	public OrcaManager orcamanager;
	public LogoInterpreter Interpreter;

	
	public OrcaGraphics() {
		setSize(620, 620);
		setTitle("Orca Graphics");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setLayout(new FlowLayout());
		setResizable(false);
		orcamanager = new OrcaManager();
		Interpreter = new LogoInterpreter(orcamanager);
		graphicsPanel = new GraphicsPanel(g, orcamanager);
		orcamanager.setGPanel(graphicsPanel);
		orcamanager.setGraphics(g);
		orcamanager.setInterpreter(Interpreter);
		add(graphicsPanel);
		add(new SettingsControlPanel(orcamanager));
		add(new LogoProgramEditor(orcamanager));
		ACPanel = new ArrowControlPanel(orcamanager);
		add(ACPanel);
		orcamanager.setACPanel(ACPanel);
		
		
		
	}
	
	
	public static void main(String[] args) {
		new OrcaGraphics().setVisible(true);

	}

}
