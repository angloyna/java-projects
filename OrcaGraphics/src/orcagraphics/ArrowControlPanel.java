package orcagraphics;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.plaf.basic.BasicArrowButton;

import UnderlyingOBJSTRUCT.Mode;
import UnderlyingOBJSTRUCT.OrcaManager;

import ActionListeners.ClearScreen;
import ActionListeners.MoveBack;
import ActionListeners.MoveForward;
import ActionListeners.MoveNoDraw;
import ActionListeners.TurnLeft;
import ActionListeners.TurnRight;
import ActionListeners.UndoLastMove;

@SuppressWarnings("serial")
public class ArrowControlPanel extends JPanel {
	private EtchedBorder etchedBorder = new EtchedBorder(EtchedBorder.RAISED, Color.green, Color.BLACK);
	private BasicArrowButton upButton = new BasicArrowButton(SwingConstants.NORTH);
	private BasicArrowButton downButton = new BasicArrowButton(SwingConstants.SOUTH);
	private BasicArrowButton leftButton = new BasicArrowButton(SwingConstants.WEST);
	private BasicArrowButton rightButton = new BasicArrowButton(SwingConstants.EAST);
	private Mode mode;
	private OrcaManager orcaManager;
	

	public JButton penControl = new JButton("Pen Up");
	private JButton clearButton = new JButton("Clear Screen");
	private JButton undoButton = new JButton("Undo");
	

	public ArrowControlPanel(OrcaManager om) {
		orcaManager = om;
		setPreferredSize(new Dimension(120, 120));
		setBackground(Color.pink);
		setBorder(etchedBorder);
		mode = Mode.PEN_DOWN;
		add(upButton);
		add(downButton);
		add(leftButton);
		add(rightButton);
		add(penControl);
		add(clearButton);
		add(undoButton);
		upButton.addActionListener(new MoveForward(orcaManager));
		downButton.addActionListener(new MoveBack(orcaManager));
		leftButton.addActionListener(new TurnLeft(orcaManager));
		rightButton.addActionListener(new TurnRight(orcaManager));
		penControl.addActionListener(new MoveNoDraw(orcaManager, penControl));
		clearButton.addActionListener(new ClearScreen(orcaManager));
		undoButton.addActionListener(new UndoLastMove(orcaManager));
		setVisible(true);
	}
	
	public void setMode(Mode m) {
		mode = m;
	}
	
	public Mode getMode() {return mode;}

}