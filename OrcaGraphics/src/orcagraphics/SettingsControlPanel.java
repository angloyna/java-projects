package orcagraphics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.EtchedBorder;

import ActionListeners.ChangeSpeed;
import UnderlyingOBJSTRUCT.OrcaManager;

@SuppressWarnings("serial")
public class SettingsControlPanel extends JPanel {
	EtchedBorder etchedBorder = new EtchedBorder(EtchedBorder.RAISED, Color.green, Color.BLACK);
	String[] colorStrings = {"pink", "red", "yellow", "green", "black", "white"};
	@SuppressWarnings({ "unchecked", "rawtypes" })
	JComboBox colorSelector = new JComboBox(colorStrings);
	OrcaManager orcaManager;
	static int Speed_MIN = 1;
	static int Speed_MAX = 5;
	static int Speed_INIT = 3;
	JLabel SpeedLabel = new JLabel("Adjust Speed: ");
	
	
	public SettingsControlPanel(OrcaManager om) {
		orcaManager = om;
		setPreferredSize(new Dimension(120, 450));
		setBackground(Color.GRAY);
		setBorder(etchedBorder);
		setVisible(true);
		add(new JLabel("Adjust Color: "));
		add(colorSelector);
		colorSelector.setSelectedIndex(0);
		colorSelector.addActionListener(new changeColor());
		
		
		JSlider orcaSpeed = new JSlider(JSlider.HORIZONTAL,
		                                      Speed_MIN, Speed_MAX, Speed_INIT);
		orcaManager.setSpeedSlider(orcaSpeed);
		orcaSpeed.addChangeListener(new ChangeSpeed(orcaManager));
		orcaSpeed.setPreferredSize(new Dimension(110, 65));
		orcaManager.setTimerWait(Speed_INIT);

		orcaSpeed.setMajorTickSpacing(10);
		orcaSpeed.setMinorTickSpacing(1);
		orcaSpeed.setPaintTicks(true);
		orcaSpeed.setPaintLabels(true);
		add(SpeedLabel);
		add(orcaSpeed);
		
	}

	
	
	private class changeColor implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (colorStrings[colorSelector.getSelectedIndex()] == "pink") {
				orcaManager.setLineColor(Color.pink);
			} if (colorStrings[colorSelector.getSelectedIndex()] == "black") {
				orcaManager.setLineColor(Color.black);
			} if (colorStrings[colorSelector.getSelectedIndex()] == "white") {
				orcaManager.setLineColor(Color.white);
			} if (colorStrings[colorSelector.getSelectedIndex()] == "yellow") {
				orcaManager.setLineColor(Color.yellow);
			} if (colorStrings[colorSelector.getSelectedIndex()] == "green") {
				orcaManager.setLineColor(Color.green);
			} if (colorStrings[colorSelector.getSelectedIndex()] == "red") {
				orcaManager.setLineColor(Color.red);
			}
			
		}
		
	}
}
