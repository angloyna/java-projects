package orcagraphics;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EtchedBorder;

import UnderlyingOBJSTRUCT.OrcaManager;


import ActionListeners.runProgram;
import ActionListeners.stopProgram;

import edu.hendrix.grambler.ParseException;

@SuppressWarnings("serial")
public class LogoProgramEditor extends JPanel {
		JScrollPane scrollPane = new JScrollPane();
		EtchedBorder etchedBorder = new EtchedBorder(EtchedBorder.RAISED, Color.green, Color.BLACK);
		JButton runButton = new JButton("Run");
		JButton CommandButton = new JButton("Enter");
		JButton stopButton = new JButton("Stop");
		JTextArea programText = new JTextArea();
		JScrollPane scrollEditor = new JScrollPane();
		JTextArea editorArea = new JTextArea();
		
		
	public LogoProgramEditor(OrcaManager om) {
		setPreferredSize(new Dimension(450, 120));
		setBackground(Color.cyan);
		setBorder(etchedBorder);
		scrollPane.setBackground(Color.BLACK);
		scrollPane.setVisible(true);
		scrollPane.setPreferredSize(new Dimension(350, 20));
		scrollPane.getViewport().add(programText);
		scrollEditor.setBackground(Color.BLACK);
		scrollEditor.setVisible(true);
		scrollEditor.setPreferredSize(new Dimension(430, 40));
		scrollEditor.getViewport().add(editorArea);
		add(scrollPane);
		add(CommandButton);
		add(scrollEditor);
		add(runButton);
		add(stopButton);
		runButton.addActionListener(new runProgram(om, editorArea));
		stopButton.addActionListener(new stopProgram(om));
		CommandButton.addActionListener(new runProgram(om, programText));
		setVisible(true);
	}
	
	
}


