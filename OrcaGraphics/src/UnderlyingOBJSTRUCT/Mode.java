package UnderlyingOBJSTRUCT;

public enum Mode {
	
	PEN_DOWN {
		@Override
		public boolean getValue(boolean current) {return !current;}
		public String toString() {return "Pen Down";}
	}, 
	PEN_UP {
		@Override
		public boolean getValue(boolean current) {return false;}
		public String toString() {return "Pen Up";}
	},;
	
	abstract public boolean getValue(boolean current);
	
	public Mode advance() {
		if (this.ordinal() < 1) {
			int next = this.ordinal() + 1;
			return Mode.values()[next];
		} else {
			int next = 0;
			return Mode.values()[next];
		}
	}
}