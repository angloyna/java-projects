package UnderlyingOBJSTRUCT;


import java.awt.Graphics;


public class Line {
	private int x2, y2, x, y;
	private OrcaManager om;
	
	public Line(OrcaManager om, int x1, int y1, int x2, int y2) {
		this.x = x1;
		this.y = y1;
		this.y2 = y2;
		this.x2 = x2;
		this.om = om;
	}
	
	public void draw(Graphics g) {
		g.setColor(om.lineColor);
		g.drawLine(x, y, x2, y2);
	}
	
	public int getStartX() {
		return this.x;
	
	}
	
	public int getStartY() {
		return this.y;
	}
}
