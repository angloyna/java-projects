package UnderlyingOBJSTRUCT;

import java.awt.Color;
import java.awt.Graphics;
import java.util.HashMap;
import java.util.Timer;

import javax.swing.JSlider;

import orcagraphics.ArrowControlPanel;
import orcagraphics.GraphicsPanel;

import LogoCode.LogoInterpreter;
import edu.hendrix.grambler.Tree;
public class OrcaManager {
	public Orca orca;
	Graphics g;
	public GraphicsPanel gpanel;
	ArrowControlPanel ACPanel;
	LogoInterpreter Interpreter;
	Color lineColor = Color.pink;
	double direction;
	boolean runProgram;
	Timer timer;
	int timerWait;
	JSlider SpeedSlider;
	HashMap<String, Tree> procedures;
	
	public OrcaManager() {
		direction = 90*(Math.PI/180);
		
	}
	
	public void setGraphics(Graphics graphics) {
		g = graphics;
	}
	
	public void setSpeedSlider(JSlider slider) {
		SpeedSlider = slider;
	}
	
	public JSlider getSpeedSlider() {
		return SpeedSlider;
	}
	
	public void setTimerWait(int time) {
		timerWait = time;
	}
	
	public int getTimerWait() {
		return timerWait;
	}
	
	public void setLineColor(Color color) {
		lineColor = color;
	}
	
	public Graphics getGraphics() {
		return g;
	}
	
	public void runProgram() {
		runProgram = true;
	}
	
	public void setTimer(Timer timer) {
		this.timer = timer;
	}
	
	public Timer getTimer() {
		return timer;
	}
	
	public void haltProgram() {
		timer.cancel();
	}
	
	public void setInterpreter(LogoInterpreter LogoInterpreter) {
		Interpreter = LogoInterpreter;
	}
	
	public LogoInterpreter getInterpreter() {
		return Interpreter;
	}
	
	public void setOrca(Orca o) {
		orca = o;
	}
	
	public Orca getOrca() {
		return orca;
	}
	
	
	public GraphicsPanel getGPanel() {
		return gpanel;
	}
	
	public void setGPanel(GraphicsPanel gPanel) {
		gpanel = gPanel;
	}
	
	public void setACPanel(ArrowControlPanel ACP) {
		ACPanel = ACP;
	}
	
	public ArrowControlPanel getACPanel() {
		return ACPanel;
	}
	
	public void setDirection(double radians) {
		direction += radians;
	}

	public double getDirection() {
		return direction;
	}

	public boolean isRunnable() {
		return runProgram;
	}
	
	public void resetDirection() {
		direction = 90 * (Math.PI/180);
	}
	
	
}
