package UnderlyingOBJSTRUCT;

import java.awt.*;
import java.util.ArrayList;



public class Orca {
	private int width, height, x, y;
	private ArrayList<Line> LineList;
	private boolean isVisible;

	  
	  public Orca(int x, int y, int w, int h) {
		  width = w;
		  height = h;
		  this.x = y;
		  this.y = y;
		  LineList = new ArrayList<Line>();
		  isVisible = true;
		  
	  }
	
	public void clearOrcaPath() {
		LineList.clear();
	}

	public void draw(Graphics g) { 
		  for (Line i:LineList) {
			  i.draw(g);
		  }
		  g.setColor(Color.GREEN);
		  if (isVisible) {
			g.fillRect(x, y, width, height);
		  }
	  }

	public void resetPosition() {
		this.x = 215;
		this.y = 220;
		
	}

	public void undoLastMove() {
		if (!LineList.isEmpty()) {
			this.x = LineList.get(LineList.size()-1).getStartX() - this.width/2;
			this.y = LineList.get(LineList.size()-1).getStartY() - this.height/2;
			LineList.remove(LineList.size() - 1);
		}
		
		
	}
	
	public void moveForward(OrcaManager om, int spaces) {
		if (om.getACPanel().getMode().toString() == "Pen Down") {
			Number temp1 = spaces*Math.sin(om.direction);
			Number temp2 = spaces*Math.cos(om.direction);
			LineList.add(new Line(om, (this.x+this.width/2), (this.y+this.height/2), ((this.x+this.width/2)+temp2.intValue()), ((this.y+this.height/2)-(temp1.intValue()))));
		  }
		  this.y -= spaces*Math.sin(om.direction);
		  this.x += spaces*Math.cos(om.direction);
	}
	  
	public void moveBack(OrcaManager om, int spaces) {
		  if (om.getACPanel().getMode().toString() == "Pen Down") {
			  Number temp1 = spaces*Math.sin(om.direction);
			  Number temp2 = spaces*Math.cos(om.direction);
			  LineList.add(new Line(om, (this.x+this.width/2), (this.y+this.height/2), ((this.x+this.width/2)-temp2.intValue()), ((this.y+this.height/2)+(temp1.intValue()))));
		  }
		  this.y += spaces*Math.sin(om.getDirection());
		  this.x  -= spaces*Math.cos(om.getDirection());
	}
	
	public void turnLeft(OrcaManager om, double spaces) {
		om.setDirection(Math.toRadians(spaces));
		
	}
	
	public void turnRight(OrcaManager om, double spaces) {
		om.setDirection(Math.toRadians(-spaces));
	}
	
	public void clearscreen() {
		LineList.clear();
		resetPosition();
	}
	
	public void showTurtle() {
		isVisible = true;
	}
	
	public void hideTurtle() {
		isVisible = false;
	}
	
	public void penUp(OrcaManager om) {
		om.getACPanel().setMode(Mode.PEN_UP);
		om.ACPanel.penControl.setText(Mode.PEN_UP.toString());
	}
	
	public void penDown(OrcaManager om) {
		om.getACPanel().setMode(Mode.PEN_DOWN);
		om.ACPanel.penControl.setText(Mode.PEN_DOWN.toString());
	}
	
	
}



