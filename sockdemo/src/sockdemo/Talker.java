package sockdemo;

import java.io.*;
import java.util.Scanner;
import java.net.Socket;


public class Talker {
	
	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Usage: Talker port host");
			System.exit(1);
		}

		int port = Integer.parseInt(args[0]);
		String host = args[1];
		Socket s = null;
		try {
			s = new Socket(host, port);
			Scanner in = new Scanner(System.in);
			PrintWriter writer = new PrintWriter(s.getOutputStream());
			System.out.println("Connection to " + host + " is open");
			String cont = "";
			while (!cont.matches("n.*")) {
				StringBuilder msg = new StringBuilder();
				System.out.println("Begin message; type ! by itself when done");
				String line = in.nextLine();
				while (!line.equals("!")) {
					msg.append(line);
					msg.append("\n");
					line = in.nextLine();
				}

				writer.print(msg);
				writer.flush();
				System.out.println("message sent; awaiting response...");

				BufferedReader responses = 
						new BufferedReader
						(new InputStreamReader(s.getInputStream()));
				while (!responses.ready()){}
				while (responses.ready()) {
					System.out.println(responses.readLine());
				}

				System.out.print("Another message? ");
				cont = in.nextLine();
			}
			in.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			try {
				if (s != null) {s.close();}
			} catch (IOException ioe) {
				System.out.println("error closing socket");
			}
		}
	}
}
