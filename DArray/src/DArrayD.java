
public class DArrayD {
	
	private int[] array;  //internal "static" array
	private int size;  //number of "meaningful" entries in the array
	private int capacity;  //length of the array -  should correspond to array.length - here only for convenience
	private int num_copy_assignments;  //number of variable assignments in adding new entries, including the copying of 
                                       // the values of one array into another
	
	DArrayD() {  //"default" constructor (no passed parameters)
		//COMPLETE
		this.capacity = 10;  // "this" refers to the instance of the class (very similar to Python's "self")
		this.size = 0;
		this.array = new int[capacity];
		this.num_copy_assignments = 0;
	}

	DArrayD( int initialCapacity ) { //second constructor, allows user to set the initial capacity of the internal array
		// write this constructor
		this.capacity = initialCapacity;
		this.size = 0;
		this.array = new int[capacity];
		this.num_copy_assignments = 0;

	}
	
	private void enlarge() {
		//make a new array with twice the capacity, copy the entries from the existing array into
		// this new one, and increment num_copy_assignments by the total number of copy assignments
		int[] arr;
		capacity = 2 * capacity;
		arr = new int[capacity];
		for (int idx = 0; idx < size; idx++) {
			arr[idx] = array[idx];
			num_copy_assignments++;
		}
		array = arr;
	}
	
	public boolean isEmpty() {
		//returns whether the array is 'empty' (no meaningful values stored)
		if (size == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public int num_entries() {
		//returns the number of elements in 'array' - COMPLETE
		return size;
	}
	
	public int capacity() {
		//returns the capacity of the internal array - COMPLETE
		return capacity;
	}
	
	public void trimToSize() {
		//resize the capacity of the array so that it holds EXACTLY the 
		// meaningful entries with no additional unused slots
		//IF there are no meaningful entries make the resulting array's
		//  length = 1
		int[] arr;
		if (size > 0) {
			if (size < capacity) {
				capacity = size;
				arr = new int[capacity];
				for (int k = 0; k < size; k++) {
					arr[k] = array[k];
				}
				array = arr;
			}
		}
		else {
			size = 1;
			capacity = size;
			arr = new int[capacity];
			array = arr;
		}
	}
	
	public void add( int val ) {
		//adds the value 'val' to the end of the array, increment num_copy_assignments
		if ((size + 1) > capacity) {
			enlarge();
		}
		array[size] = val;
		size++;
		num_copy_assignments++;
	}
	
	public void add( int pos, int val ) {
		//adds the value 'val' in the array at position 'pos', shifting entries to the right if need be
		// and increases the capacity of array if needed
		//if the value of pos is not valid this implementation "ignores" the request
		//DO NOT NEED TO INCREMENT num_copy_assignments
		if ((size + 1) > capacity) {
			enlarge();
		}
		if (pos <= (size - 1)) {
			int[] arr = new int[(size + 1)];
			for (int h = pos; h < size; h++) {
				arr[h] = array[h];
			}
			array[pos] = val;
			for (int p = pos; p < size; p++) {
				array[(p + 1)] = arr[p];
			}
			size++;
		}
		if (pos == size) {
			array[pos] = val;
			size++;
		}
	}
	
	public void clear() {
		//removes the meaningful entries from array
		for (int j = 0; j < size; j++) {
			array[j] = 0;		
		}
		size = 0;
	}
	
	public boolean contains( int val ) {
		//returns whether array contains the value 'val' - COMPLETE
		if( size == 0 ) { return false; }
		for( int idx = 0; idx < size; idx++ ) {
			if( array[ idx ] == val ) { return true; }
		}
		return false;  //if it never returns in the loop 'val' is not in the array
	}
	
	public int indexOf( int val ) { 
		//returns the index/position of the first occurrence of 'val' or -1 if 'val' is not in the array
		for (int i = 0; i < size; i++) {
			if (array[i] == val) {
				return i;
			}
		}
		return -1;

	}
	
	public int get( int pos ) {
		//returns the value in array at position 'pos'
	    // for this implementation if pos is meaningless we will return the value Integer.MAX_VALUE
		// COMPLETE
		if( (pos < 0) || (pos >= size) ) { return Integer.MAX_VALUE; }
		return array[ pos ];
	}
	
	public void remove( int pos ) {
		//removes the value in array at position 'pos' and shifts everything down
		//for this implementation if the value of pos is meaningless we will do nothing
		int[] arr = new int[capacity];
		if (pos < size) {
			for (int n = 0; n < pos; n++) {
				arr[n] = array[n];
			}
			for (int n = pos; n < (size - 1); n++) {
				arr[n] = array[(n + 1)];
			}
			array = arr;
			size--;
		}
	}
	
	public void set( int pos, int val ) {
		//sets the value in array at position 'pos' to 'val'
		//for this implementation if 'pos' is meaningless we will do nothing
		if (pos < size) {
			array[pos] = val;
		}
	}
	
	public int[] getArray() {
		//returns a copy of array whose capacity is exactly the number of entries in array
		// COMPLETE
		int[] temp = new int[size];
		for( int idx = 0; idx < size; idx++ ) { 
			temp[ idx ] = array[ idx ]; 
		}
		return temp;
	}
	
	public int get_num_copy_assignments() {
		// COMPLETE
		return num_copy_assignments;
	}

}
