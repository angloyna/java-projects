
import java.util.Random;

public class DArray {

	public static void main(String[] args) {
		DArrayC c_array;  //instance of DArrayC
		DArrayD d_array;  //instance of DArrayD
		DArray my_app;  //instance of DArray
		float[] d_percent_used = new float[200];
		float[] c_percent_used = new float[200]; 
		int[] d_copy_assignments = new int[ 200 ];
		int[] c_copy_assignments = new int[ 200 ];
		int[] num_entries = new int[ 200 ];

		
		c_array = new DArrayC();
		d_array = new DArrayD();		
		my_app = new DArray();
		Random my_rand = new Random();
		
		
		
		//First test your implementation of the two classes DArrayC and DArrayD
		//Systematically add to/delete from/the two instances in such a way that
		// the reader can CLEARLY and EASILY see that the interface is working as
		// it should be
		
		DArrayC test_c = new DArrayC(40);
		DArrayD test_d = new DArrayD(40);
		
		System.out.println("Capacity of test_c array: " + test_c.capacity()); //Tests constructor's ability to set the capacity of the array (C)
		
		System.out.print("Capacity of test_d array: " + test_d.capacity()); //Test constructor's ability to set the capacity of the array (D)
		
		test_c.add(7);	//Tests the add method for C and D arrays
		test_d.add(7);
		System.out.println("test_c.add(7)");			//method call printed to show method in the console next to results
		System.out.println("test_d.add(7)");
		my_app.printArray(test_c.getArray(), 0, test_c.num_entries());
		my_app.printArray(test_d.getArray(), 0, test_d.num_entries());
		test_c.add(4);
		test_d.add(4);
		System.out.println("test_c.add(4)");
		System.out.println("test_d.add(4)");
		my_app.printArray(test_c.getArray(), 0, test_c.num_entries());
		my_app.printArray(test_d.getArray(), 0, test_d.num_entries());
		System.out.println("test_c array contains value 7: " + test_c.contains(7)); //Testing contains() method
		System.out.println("test_d array contains value 7: " + test_d.contains(7));
		System.out.println("index of value 7 in test_d: " + test_d.indexOf(7));		//Tests indexOf() method for C and D								
		System.out.println("index of value 7 in test_c: " + test_c.indexOf(7));		//For both arrays, indexOf() method should return
		System.out.println("index of value 6 in test_d: " + test_d.indexOf(6));		// '0' for value 7 and '-1' for value 6.
		System.out.println("index of value 6 in test_c: " + test_c.indexOf(6));
		
		System.out.println("Number entries in test_d: " + test_d.num_entries());		//Tests the num_entries() method for C and D, indicating whether size is properly updating			
		System.out.println("Number entries in test_c: " + test_c.num_entries());		//Should verify that there are 2 entries in each array.
		
		
		test_d.add(1, 18);			//Tests the add() (analogous to insert) method for both arrays.
		test_c.add(1, 18);			//The value '18' should be placed at position 1 between 7 and 4 in both arrays.
		System.out.println("test_d.add(1, 18)");
		System.out.println("test_c.add(1, 18)");
		my_app.printArray(test_c.getArray(), 0, test_c.num_entries());
		my_app.printArray(test_d.getArray(), 0, test_d.num_entries());
		System.out.println("Number entries in test_d: " + test_d.num_entries());			//Checks number entries in each array. Should be 3 for each array.
		System.out.println("Number entries in test_c: " + test_c.num_entries());
		
		test_d.remove(1);										//Tests the remove() method for each array by removing the value at position 1.
		test_c.remove(1);
		System.out.println("test_d.remove(1)");
		System.out.println("test_c.remove(1)");
		my_app.printArray(test_c.getArray(), 0, test_c.num_entries());
		my_app.printArray(test_d.getArray(), 0, test_d.num_entries());
		System.out.println("Number entries in test_d: " + test_d.num_entries());//Checks number entries in each array. Should be 2 for each array.
		System.out.println("Number entries in test_c: " + test_c.num_entries());
		
		test_d.trimToSize();									//Tests the trimToSize() method by checking the capacity after the call
		test_c.trimToSize();									//After call to this method, capacity of each array should be 2.
		System.out.println("test_d.trimToSize()");
		System.out.println("test_c.trimToSize()");
		System.out.println("Capacity of test_d: " + test_d.capacity());
		System.out.println("Capacity of test_c: " + test_c.capacity());
		
		test_d.clear();											//Tests clear() method for both arrays, checks number of entries of each array.
		test_c.clear();											//after clear, there should be zero entries in each array.
		System.out.println("test_d.clear()");
		System.out.println("test_c.clear()");
		System.out.println("Number entries in test_c: " + test_c.num_entries());
		System.out.println("Number entries in test_d: " + test_d.num_entries());
		
		test_d.trimToSize();									//Another trimToSize() test to check for empty array case.
		test_c.trimToSize();
		System.out.println("test_d.trimToSize()");
		System.out.println("test_c.trimToSize()");
		System.out.println("Number entries of test_d: " + test_d.num_entries());	//if trimToSize() is working properly, should return 1.
		System.out.println("Number entries of test_c: " + test_c.num_entries());
		
		
		System.out.println("test_d is empty: " + test_d.isEmpty());					//Tests isEmpty() method. Should return 'true' if working properly.
		System.out.println("test_c is empty: " + test_c.isEmpty());
		test_d.add(8);											//Adds value to both arrays to test the 'false' return for isEmpty() method.
		test_c.add(8);
		System.out.println("test_d.add(8)");
		System.out.println("test_c.add(8)");
		System.out.println("test_d is empty:  " + test_d.isEmpty());			//Should return 'false' for both arrays
		System.out.println("test_c is empty: " + test_c.isEmpty());
		
		test_d.add(27);									//adding values to each array to begin test of set() method
		test_c.add(27);
		test_d.add(14);
		test_c.add(14);
		System.out.println("test_d.add(27)");
		System.out.println("test_c.add(27)");
		System.out.println("test_d.add(14)");
		System.out.println("test_c.add(14)");
		my_app.printArray(test_c.getArray(), 0, test_c.num_entries());
		my_app.printArray(test_d.getArray(), 0, test_d.num_entries());
		test_d.set(1, 600);
		test_c.set(1, 600);
		System.out.println("test_d.set(1, 600)");
		System.out.println("test_c.set(1, 600)");
		my_app.printArray(test_c.getArray(), 0, test_c.num_entries());
		my_app.printArray(test_d.getArray(), 0, test_d.num_entries());
		
		test_d = new DArrayD();																		//Testing the enlarge() method
		test_c = new DArrayC();
		System.out.println("Intial d and c capacity: " + test_d.capacity() + test_c.capacity());
		for (int i = 0; i < 10; i++) {																//adding consecutive numbers to full capacities
			test_d.add(i, i);
		}
		
		for (int i = 0; i < 100; i++) {
			test_c.add(i, i);
		}
		
		System.out.println("Capacity after adding from loops: " + test_d.capacity() + test_c.capacity());
		test_d.add(10, 56);																				//adding one more to call an enlarge()
		test_c.add(100, 56);	
		System.out.println("Adding one more value to cause a capacity enlargement: " + test_d.capacity() + test_c.capacity());
		System.out.println("Value at position 9 in test_d: " + test_d.get(9));							//checking the new capacities and the values that
		System.out.println("Value at position 99 in test_c: " + test_c.get(99));						//straddle the old and new capacity line
		System.out.println("Value at position 10 in test_d: " + test_d.get(10));						// new capacities should be 20 and 300.
		System.out.println("Value at position 100 in test_c: " + test_c.get(100));


		
		//Next, run 200 instances of instantiating c_array and d_array choosing a random
		// number between 1000 and 50,000, and "adding" that many meaningful values to
		//each of the arrays.  Collect the requested values from each run and store them in 
		// the associated arrays.
		int rand_num;
		float entries;
		
		for (int i = 0; i < 200; i++) {
			rand_num = my_app.getRandomBetween(1000, 50000, my_rand); //generating new random number every iteration
			c_array = new DArrayC();			//instantiating each array every iteration
			d_array = new DArrayD();
			
			for (int k = 0; k < rand_num; k++) { //populating arrays
				c_array.add(k);
				d_array.add(k);
			}
			
			d_copy_assignments[i] = d_array.get_num_copy_assignments();
			c_copy_assignments[i] = c_array.get_num_copy_assignments();
			num_entries[i] = rand_num;
			entries = num_entries[i];						//storing number of entries as a float to allow for division below
			d_percent_used[i] = entries / d_array.capacity();
			c_percent_used[i] = entries / c_array.capacity();
		}
		
		//Compute and print the average number of copy assignments and percent of
		//final array used for the two hundred runs and output these values to the user on
		// the console in narrative fashion.
		
		System.out.println("Average number of entries: " + my_app.average(num_entries));
		System.out.println("Average number of copy assignments for d_array: " + my_app.average(d_copy_assignments));
		System.out.println("Average number of copy assignments for c_array: " + my_app.average(c_copy_assignments));
		System.out.println("Percent of final array used for two hundred runs for d_array: " + (my_app.average(d_percent_used) * 100) + "%");
		System.out.println("Percent of final array used for two hundred runs for c_array: " + (my_app.average(c_percent_used) * 100) + "%");
		
	}
	
	public void printArray( int[] array, int startIdx, int endIdx ) {
		if( startIdx < 0 ) { startIdx = 0; }
		if( endIdx >= array.length ) {endIdx = array.length - 1; }
		for( int idx = startIdx; idx <= endIdx; idx++ ) {
			System.out.print( array[ idx ] + " ");  //autoboxing, converting all components to strings behind the scene
		}
		System.out.println(); // carriage return
	}
	
	public int getRandomBetween( int minVal, int maxVal, Random rnd ) {
		if( maxVal < minVal ) { // just in case the numbers were accidently switched
			int temp = maxVal;
			maxVal = minVal;
			minVal = temp;
		}
		return rnd.nextInt( maxVal - minVal + 1 ) + minVal;
	}
	
	public int average( int[] array ) {
		int sum = 0;
		for( int idx = 0; idx < array.length; idx++ )
			sum += array[idx];
		return sum/array.length;			            
	}
	
	public float average( float[] array ) {
		float sum = 0;
		for( int idx = 0; idx < array.length; idx++ )
			sum += array[idx];
		return sum/array.length;
	}

}
