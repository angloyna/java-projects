import java.util.Random;

public class Favorites {
	private String welcome;
	//can instantiate an array with a list of comma separated values within "{  }"
	private String[] dwarves = { "Fili", "Kili", "Oin", "Gloin", "Balin", "Bifur", "Bofur", "Bombur",
			                     "Dwalin","Dori", "Nori", "Ori", "Thorin Oakenshield" };
	//This is the same as doing a "new" and then populating the entries
	private String[] wizards = { "Gandalf", "Saruman", "Radagast" };
	private int numG = 0;
	private int Gidx = 0;
	private int numS = 0;
	private int Sidx = 1;
	private int Ridx = 2;
	private int numR = 0;
	private int fav_wizard_idx = -1;
	private Random r;
	
	//there are two constructors, one taking NO input and the other a single string
	//Java knows which one to use based on the input parameters!
	public Favorites() {
		welcome = "Hello.  My, you seem quite exemplary!";
		r = new Random();
	}
	
	public Favorites( String welcome ) {
		this.welcome = welcome;  //"this" refers to the actual class variable (like Python's "self")
		r = new Random();
	}
	
	public String hello() {
		return welcome;
	}
	
	public String dwarf() {
		return dwarves[getDwarfIdx()];
	}
	
	public String wizard() {
		return wizards[getWizardIdx()];
	}
	
	public boolean favoriteWizard() {
		return fav_wizard_idx >= 0;
	}
	
	private int getDwarfIdx() {
		return r.nextInt(dwarves.length);  //REMEMBER, array length is NOT a method (no parentheses)
	}
	
	private int getWizardIdx() {
		if( fav_wizard_idx >= 0 ) {
			return fav_wizard_idx;
		} else {
			int idx = r.nextInt(wizards.length);
			if( idx == Gidx ) {
				numG++;
				if( numG > 2 ) {
					fav_wizard_idx = Gidx;
				}
			} else if( idx == Sidx ) {
				numS++;
				if( numS > 2 ) {
					fav_wizard_idx = Sidx;
				}
			} else {   // must be Radagast
				numR++;
				if( numR > 2 ){
					fav_wizard_idx = Ridx;
				}
			}
			return idx;
		} //outer else		
	}	
}
