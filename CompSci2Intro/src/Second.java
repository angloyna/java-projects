import java.util.Random;

public class Second {

	public static void main(String[] args) {
		int my_int_array [];   //declaration of array object
		int [] my_second_int_array;  //another form for such a declaration
		//the value of each of my_int_array and my_second_int_array
		// will be a MEMORY LOCATION !! Right now Java has not given them
		// a value (not inititalized ).
		
		my_int_array = new int [10];
		//Now my_int_array has been initialized.  If the OS was able to give us enough
		//  CONTIGUOUS memory to hold the values of 10 ints, it will have reserved that
		//  memory block and returned a reference to the memory location of the first 
		//  byte of the block. Java then stores that reference in my_int_array.
		System.out.println( "The value of my_int_array is " + my_int_array + "\n" );
		
		//Java also "cleans out" the memory block allocated for this array, setting each entry(byte) to "zero".
		
		System.out.println( "The value of my_int_array[6] is " + my_int_array[6] + "\n" );
		
		//We will create an instance of Java's (pseudo-)random number generator
		//This constructor/initializer uses the clock time as seed, but you can specify the seed
		Random my_random = new Random();  
		
		for( int idx = 0; idx < my_int_array.length; idx++) {  //length is an public instance variable, NOT a method !!
			my_int_array[idx] = my_random.nextInt(10);  // random integer between 0 and 9 inclusive		
		}
		// (Almost) all for loop blocks require a pair of "curly braces" around the executable block of the loop
		//  This is analogous to the colon/tab indention rule in Python
		for( int idx = 0; idx < my_int_array.length; idx++ ) {
			System.out.print(my_int_array[idx]);
			System.out.print( " " );
		}
		System.out.println("\n"); //skip a line in the output
		
		//Now let's assign the value of my_int_array to my_second_int_array
		my_second_int_array = my_int_array;
		
		//I'm getting tired of retyping and/or copying/pasting the same code over and over again
		// so I'm gonna put and use some utility methods in class Second
		Second my_instance = new Second();  //in order to use such a class method I need an instance of the class
		
		//Let's look at what is in my_second_int_array
		System.out.println( "The value of my_int_array is " + my_int_array );
		System.out.println( "The value of my_second_int_array is " + my_second_int_array );
		System.out.println();

		System.out.println( "The values in my_int_array:");
		my_instance.printArray( my_int_array );		
		System.out.println();

		System.out.println( "The values in my_second_int_array:");
		my_instance.printArray( my_second_int_array );
		System.out.println();
		
		System.out.println( "Gonna call the method zeroOut on my_int_array\n" );
		
		for( int idx = 0; idx < my_int_array.length; idx+=2 ) {
			my_instance.zeroOut( my_int_array, idx );
		}
		System.out.println( "The values in my_int_array:");
		my_instance.printArray( my_int_array );		
		System.out.println();

		System.out.println( "The values in my_second_int_array:");
		my_instance.printArray( my_second_int_array );
	}
	
	public void zeroOut( int[] array, int position ) {
		if( (position >= array.length) || (position < 0) ) { return; }  //  "||" means "or",  "&&" means "and"
		array[position] = 0;
	}
	
	public void printArray( int[] array ) {
		for( int idx = 0; idx < array.length; idx++ ) {
			System.out.print( array[idx] + " " );
		}
		System.out.println(); //don't forget a line feed
	}

}
