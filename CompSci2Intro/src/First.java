
import java.util.Random;

public class First {

	public static void main(String[] args) {
		//We first show how to push output to the console
		System.out.println("Hello, World!");
		
		//Now let's declare some variables of primitive types
		//Declare the int variable my_int and assign (initialize) it (with) a value
		int my_int = 3;
		//Declare the int variable my_second_int but don't assign it a value
		int my_second_int;
		my_second_int = 0x1A;  //can use hexadecimal integers, using the leading 0x (zero x) to identify it as such
		//Let's see what we have
		System.out.print("The value of my_int is ");
		System.out.println(my_int);  //println is an abbreviated form for print followed by a linefeed
		System.out.print("The value of my_second_int is ");
		System.out.println(my_second_int);
		my_int = 011;  //Also can use octal integers (base 8), leading 0 (zero) to identify as such
		System.out.print("Now the value of my_int is ");
		System.out.println(my_int);
		
		//Now some conditional statements
		if( my_int < my_second_int ) {
			System.out.println("The value of my_int is the smaller of the two");
			my_int *= 5;  //increase five-fold
		}
		else if( my_int > my_second_int ) {
			System.out.println("The value of my_second_int is the smaller of the two");
			my_second_int *= 2;  //double it
		}
		else {
			System.out.println("The value of my_int is the same as that of my_second_int");
			System.out.println("Goodness, isn't this just as exciting as watching paint dry?");
		}
		
		//Now some looping
		for( int idx = 0; idx < 5; idx++ ) {
			System.out.println( "mind numbing!");
		}
		
		boolean tired_yet = false; 
		double meaningless_number = -2.00014;
		Random r = new Random();  //get instance of Random number generator class in java.utils
		int random_value = r.nextInt();  //get the next integer in the list of random numbers
		while( !tired_yet ) {
			System.out.println(meaningless_number);
			if(random_value > 100000) {
				tired_yet = true;
			}
			random_value = r.nextInt();
		}		

	}

}
