
public class BaseClass {

	public static void main(String[] args) {
		BaseClass app = new BaseClass();  // Tis uses a "default" (bare minimum) constructor provided by Java
		Favorites fav = new Favorites( "Spam! Spam! Spam!");  //This uses the second constructor
		
		//need instance of this class in order to access the public methods below
		app.speak("Hi, Java app!");
		app.respond( fav.hello() );
		for( int idx = 0; idx < 4; idx++ ) {
			app.speak( "Name a dwarf you like.");
			app.respond( fav.dwarf() );
		}
		
		while( !fav.favoriteWizard() ) {  // "!" = "not"
			app.speak( "What is your favorite wizard?" );
			app.respond( fav.wizard() );
		}
		
		app.speak( "GOOD GRIEF!  WHAT IS YOUR FAVORITE WIZARD???");
		app.respond( fav.wizard() );
		app.speak( "WHAT WAS IT?" );
		app.respond( fav.wizard() );
		app.speak( "AGAIN?" );
		app.respond( fav.wizard() );		

	}
	
	//I really get tired of constantly typing or copy/pasting "System.out.println"
	public void speak( String s ) {
		System.out.println( s );
	}
	
	public void respond( String s ) {
		System.out.println( "   " + s );
		System.out.println();  //blank line
	}

}
