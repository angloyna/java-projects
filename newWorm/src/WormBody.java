
class WormBody {

  /* YOU MUST CAREFULLY DECIDE AND MAINTAIN HOW YOU WILL
     ASSOCIATE THE HEAD OF THE WORM WITH THE HEAD OF
     WORMBODY (SINGLY LINKED LIST) !!!!!
     This class also provides an iterator.
  */

  private WormSegment head, tail, iter;
  
  WormBody() {
	  head = tail = iter = null;
  }
  
  public void addToHead( WormSegment p ) {
	// p becomes the new head OF THE WORM
	  if (empty()) {
			head = tail = p;
	  }	else {
		tail.setNext(p);
		tail = p;
		}
  }

  public WormSegment rmTail(){ 
	//remove and return the tail OF THE WORM
	  WormSegment temp = head;
	  head = head.getNext();
	  return temp;
		  
	
  }

  public boolean empty() { return (this.head == null); }//returns whether the worm body is empty
  public WormSegment getHead() { return this.tail; } //returns the head of the WORM
  public WormSegment getTail() {return this.head; };
  
  // The following three methods are iterator methods.
  // This allows for an external agent to run through
  // the data structure WITHOUT KNOWING ITS UNDERLYING
  // STRUCTURE
  
  public void start() { iter = head; }//set iter to the "head" of  WormBody
  
  public WormSegment nextElement() {
    WormSegment ws = iter;
    iter = iter.getNext();
    return ws;
  }
  
  public boolean moreElements() { return iter != null; }
  
}
