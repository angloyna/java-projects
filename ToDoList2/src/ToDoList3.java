import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.*;


@SuppressWarnings("serial")
public class ToDoList3 extends JFrame {

	private JTextField newTaskField;
	private JTextField removeTaskField;
	private JPanel newTaskPanel;
	private JPanel curTaskPanel;
	private JPanel removeTaskPanel;
	private JButton AddTaskButton;
	private JButton RemoveTaskButton;
	private JLabel TaskFieldLabel;
	private JLabel RemoveTaskLabel;
	private JCheckBox newCheckBox;
	private ArrayList<JCheckBox> ComponentList;
	private HashMap RemoveButtonFinder;
	

	
	
	public ToDoList3() {
		setSize(600, 700);
		setTitle("To-Do List");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setContentPane(new JLabel(new ImageIcon("C:\\Users\\anglo_000\\Pictures\\LegalPadBackGroundImage.jpg")));
		setLayout(new FlowLayout());
		setResizable(false);
		ComponentList = new ArrayList<JCheckBox>();

		//Creation of the JPanel that will hold TextField and ADD Button to add new task
		newTaskPanel = new JPanel();
		newTaskPanel.setLayout(new FlowLayout());
		newTaskPanel.setOpaque(false);
		
		//creation of TextField and 'ADD' button
		TaskFieldLabel = new JLabel("New Task: "); //Labels the Text Box for new task entry.
		newTaskPanel.add(TaskFieldLabel);
		
		newTaskField = new JTextField(20); 		//Creates TextField box for new task entry.
		newTaskPanel.add(newTaskField);
	
		AddTaskButton = new JButton("ADD");		//Creates ADD button
		AddTaskButton.addActionListener(new AddTask());
		//newTaskField.addKeyListener(new AddTask());
		newTaskPanel.add(AddTaskButton);
		
		
		add(newTaskPanel); 		//ADDS newTaskPanel to JFrame
		
				
		
		
		//Creation of JPanel for Current To-Do List
		curTaskPanel = new JPanel();
		//curTaskPanel.setLayout(new BoxLayout(curTaskPanel, BoxLayout.Y_AXIS));
		curTaskPanel.setLayout(new FlowLayout());
		curTaskPanel.setOpaque(false);
		curTaskPanel.setPreferredSize(new Dimension(440,550));

		add(curTaskPanel);
		
		//Creation of the JPanel that will hold TextField and ADD Button to add new task
		removeTaskPanel = new JPanel();
		removeTaskPanel.setLayout(new FlowLayout());
		removeTaskPanel.setOpaque(false);
		
		RemoveTaskLabel = new JLabel("Remove which number task? "); 
		removeTaskPanel.add(RemoveTaskLabel);
		
		removeTaskField = new JTextField(20); 
		removeTaskPanel.add(removeTaskField);
	
		RemoveTaskButton = new JButton("REMOVE");		
		RemoveTaskButton.addActionListener(new RemoveTask());
		removeTaskPanel.add(RemoveTaskButton);
		
		
		add(removeTaskPanel); 


	}
	
	private class AddTask implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub			
			
			if (newTaskField.getText().trim().equals("") != true) { //If user wrote something...
					//Creation of CheckBox associated with newly added task
				newCheckBox = new JCheckBox(newTaskField.getText());	//create a checkbox
				ComponentList.add(newCheckBox);
				newCheckBox.setAlignmentX(LEFT_ALIGNMENT);
				newCheckBox.setAlignmentY(TOP_ALIGNMENT);
				newCheckBox.setOpaque(true);
				newCheckBox.setPreferredSize(new Dimension(300,30));

				newTaskField.setText("");
				curTaskPanel.add(newCheckBox);
				
				JButton newRemoveButt = new JButton("Remove");
				newRemoveButt.setPreferredSize(new Dimension(90, 30));
				newRemoveButt.setAlignmentX(LEFT_ALIGNMENT);
				newRemoveButt.setAlignmentY(TOP_ALIGNMENT);
				curTaskPanel.add(newRemoveButt);
				
				
				curTaskPanel.revalidate();
				//}
			}
		}
	
	}
	
	
	
	public class RemoveTask implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if (removeTaskField.getText().trim().equals("") != true) { //If user wrote something...
				try {
					int input = Integer.parseInt(removeTaskField.getText().trim());
					if ((input < ComponentList.size())) {					
						newTaskField.setText("");
						curTaskPanel.remove(ComponentList.get((input - 1)));
						ComponentList.remove(ComponentList.get((input - 1)));
						removeTaskField.setText("");
						curTaskPanel.revalidate();
					}
				}	
		        catch (NumberFormatException arg) {
		           System.out.println("This is not a number");
		           System.out.println(arg.getMessage());
		        }
				
			}
			
		}

	}

	public static void main(String[] args) {
		new ToDoList3().setVisible(true);
	}
}

	

