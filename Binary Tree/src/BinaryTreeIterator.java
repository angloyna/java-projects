import java.util.Iterator;

public class BinaryTreeIterator {

	public static void main( String [] argv) {
		//The following is a partial template for code to test
		// your implementation of BinaryTree.java and BTNode.java
		//It does not include testing code for all of the methods
		//you are requested to implement.
		BinaryTree b_tree = new BinaryTree();
		b_tree.insert( new BTNode( 5 ) );
        System.out.println( "5 is the root node" );
		b_tree.insert( new BTNode( 2 ) );
		System.out.println( "2 is 5's left child" );
		b_tree.insert( new BTNode( 10 ) );
		System.out.println( "10 is 5's right child" );
		b_tree.insert( new BTNode( 3 ) );
		System.out.println( "3 is 2's right child" );
		b_tree.insert( new BTNode( 9 ) );
		System.out.println( "9 is 10's left child" );
		b_tree.insert( new BTNode( 4 ) );
		System.out.println( "4 is 3's right child" );
		b_tree.insert( new BTNode( 12 ) );
		System.out.println( "12 is 10's right child" );
		b_tree.insert( new BTNode( 8 ) );
		System.out.println( "8 is 9's left child" );
		
		//Testing numLeaves() method
		System.out.println("number leaves of current tree: " + b_tree.numLeaves());
		
		//Testing numEntries() method
		System.out.println("number entries of current tree: " + b_tree.numEntries());
		
		//The following code tests the contains method
		System.out.println( "Tree contains 5: " + b_tree.contains(5));
		System.out.println( "Tree contains 2: " + b_tree.contains(2));
		System.out.println( "Tree contains 10: " + b_tree.contains(10));
		System.out.println( "Tree contains 3: " + b_tree.contains(3));
		System.out.println( "Tree contains 9: " + b_tree.contains(9));
		System.out.println( "Tree contains 12: " + b_tree.contains(12));
		System.out.println( "Tree contains 4: " + b_tree.contains(4));
		System.out.println( "Tree contains 8: " + b_tree.contains(8));
		System.out.println( "Tree contains 1: " + b_tree.contains(1));
		System.out.println( "Tree contains 6: " + b_tree.contains(6));
		System.out.println( "Tree contains 7: " + b_tree.contains(7));
		System.out.println( "Tree contains 11: " + b_tree.contains(11));
		System.out.println( "Tree contains 87: " + b_tree.contains(87));
		
		
		System.out.println( "A pre-order traversal of this tree gives the following list:" );
		Iterator<Integer> pre = b_tree.pre_iterator();
		while( pre.hasNext() ) {
			System.out.print( pre.next() + " " );
		}
		System.out.println();
		
		System.out.println( "An in-order traversal of this tree gives the following list:" );
		Iterator<Integer> in = b_tree.in_iterator();
		while( in.hasNext() ) {
			System.out.print( in.next() + " " );
		}
		System.out.println();
		
		System.out.println( "A post-order traversal of this tree gives the following list:" );
		Iterator<Integer> post = b_tree.post_iterator();
		while( post.hasNext() ) {
			System.out.print( post.next() + " " );
		}
		System.out.println();
		
		
		
		//The following code tests the verticalFlip() method
		b_tree.verticalFlip();
		System.out.println("Status of Tree after verticalFlip()");
		System.out.println( "A pre-order traversal of this tree after verticalFlip() gives the following list:" );
		Iterator<Integer> iter = b_tree.pre_iterator();
		while( iter.hasNext() ) {
			System.out.print( iter.next() + " " );
		}
		System.out.println();
		b_tree.verticalFlip();
				
		//The following code tests the height() method
		System.out.println("Testing the height() method");
		System.out.println( "A pre-order traversal of this tree gives the following list:" );
		pre = b_tree.pre_iterator();
		while( pre.hasNext() ) {
			System.out.print( pre.next() + " " );
		}
		System.out.println();
		System.out.println("height of current tree: " + b_tree.height());
		b_tree.insert(new BTNode(7));		
		System.out.println("height of tree after insertion of node with value 7: " + b_tree.height());
		b_tree.remove(10);
		b_tree.remove(12);
		System.out.println("tree contains 10: " + b_tree.contains(10));
		System.out.println("tree contains 12: " + b_tree.contains(12));
		System.out.println("height of tree after removal of nodes with values 10 and 12: " + b_tree.height());
		
		//Testing numLeaves() method
		System.out.println("Testing the numLeaves() method");
		b_tree.insert(new BTNode(10));
		b_tree.insert(new BTNode(12));
		System.out.println( "A pre-order traversal of this tree gives the following list:" );
		pre = b_tree.pre_iterator();
		while( pre.hasNext() ) {
			System.out.print( pre.next() + " " );
		}
		System.out.println("number leaves of current tree: " + b_tree.numLeaves());
		b_tree.insert(new BTNode(1)); //inserting 1 onto the tree adds another leaf onto the node with value 2
		System.out.println("number leaves after insertion of 1 from tree: " + b_tree.numLeaves());
		
		//Testing tree that only has a root node
		System.out.println("Testing tree case which only has a root node");
		BinaryTree root_tree = new BinaryTree();
		root_tree.insert(new BTNode(10));
		System.out.println("Tree contains 10: " + root_tree.contains(10));
		System.out.println("number entries of current tree: " + root_tree.numEntries());
		System.out.println("number leaves of current tree: " + root_tree.numLeaves());
		System.out.println("height of tree: " + root_tree.height());
		
		System.out.println( "A pre-order traversal of this tree gives the following list:" );
		pre = root_tree.pre_iterator();
		while( pre.hasNext() ) {
			System.out.print( pre.next() + " " );
		}
		System.out.println();
		
		System.out.println( "An in-order traversal of this tree gives the following list:" );
		in = root_tree.in_iterator();
		while( in.hasNext() ) {
			System.out.print( in.next() + " " );
		}
		System.out.println();
		
		System.out.println( "A post-order traversal of this tree gives the following list:" );
		post = root_tree.post_iterator();
		while( post.hasNext() ) {
			System.out.print( post.next() + " " );
		}
		System.out.println();
		
		//Testing empty tree case
		System.out.println("Testing empty tree case by removing root from above tree");
		root_tree.remove(10);
		System.out.println("Tree contains 10: " + root_tree.contains(10));
		System.out.println("number entries of current tree: " + root_tree.numEntries());
		System.out.println("number leaves of current tree: " + root_tree.numLeaves());
		System.out.println("height of tree: " + root_tree.height());
		//Testing verticalFlip() method for empty case
		root_tree.verticalFlip();
		
		
		//Testing iterators for empty tree
		System.out.println( "A pre-order traversal of this tree gives the following list:" );
		pre = root_tree.pre_iterator();
		while( pre.hasNext() ) {
			System.out.print( pre.next() + " " );
		}
		System.out.println();
		
		System.out.println( "An in-order traversal of this tree gives the following list:" );
		in = root_tree.in_iterator();
		while( in.hasNext() ) {
			System.out.print( in.next() + " " );
		}
		System.out.println();
		
		System.out.println( "A post-order traversal of this tree gives the following list:" );
		post = root_tree.post_iterator();
		while( post.hasNext() ) {
			System.out.print( post.next() + " " );
		}
		System.out.println();
	}

}
