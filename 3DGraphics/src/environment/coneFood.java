package environment;

import java.awt.Color;

import com.threed.jpct.CollisionEvent;
import com.threed.jpct.CollisionListener;
import com.threed.jpct.Object3D;
import com.threed.jpct.Primitives;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.World;

public class coneFood extends Object3D implements CollisionListener {
	private SimpleVector position;
	private World world;
	private float scale;
	private Object3D food;

	public coneFood(SimpleVector vector, World world, int resolution, float scale) {
		super(resolution);
		position = vector;
		this.world = world;
		this.scale = scale;
		food = Primitives.getCube(scale);
		food.setOrigin(position);
		food.setCollisionMode(Object3D.COLLISION_CHECK_SELF);
		food.setCollisionOptimization(true);
		food.addCollisionListener(this);
	}
	
	public void giveTexture(String texname) {
		this.setTexture("gift");
		this.calcTextureWrap();
		this.setLighting(Object3D.LIGHTING_ALL_ENABLED);
		this.setAdditionalColor(Color.WHITE);
		this.compileAndStrip();
	}

	@Override
	public void collision(CollisionEvent ce) {
		// TODO Auto-generated method stub
		Object3D obj = ce.getSource();
		SimpleVector impactLocation = ce.getFirstContact();
	}

	@Override
	public boolean requiresPolygonIDs() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public Object3D getSphere() {
		return food;
	}
	
}
