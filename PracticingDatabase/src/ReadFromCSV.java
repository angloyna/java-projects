import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;



public class ReadFromCSV {
		public JFrame frame;
		private JTextArea textarea;
	
	public ReadFromCSV() {
		frame = new JFrame();
		frame.setPreferredSize(new Dimension(600,700));
		textarea = new JTextArea();
		textarea.setPreferredSize(new Dimension(400, 500));
		frame.add(textarea);
		final JFileChooser fc = new JFileChooser();
		int returnVal = fc.showOpenDialog(frame);
		File file = fc.getSelectedFile();
        BufferedReader br = null;
        
		try {
 
			String sCurrentLine;
 
			br = new BufferedReader(new FileReader(file));
 
			while ((sCurrentLine = br.readLine()) != null) {					
						String[] parts = sCurrentLine.split(",");
						for (String i : parts) {
							textarea.append(i + " ");
						}
						textarea.append("\n");
			}
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		new ReadFromCSV().frame.setVisible(true);

	}
}
