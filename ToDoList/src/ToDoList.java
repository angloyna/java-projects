

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.*;

import javax.swing.*;


@SuppressWarnings("serial")
public class ToDoList extends JFrame {

	private JTextField newTaskField;
	private JPanel newTaskPanel;
	private JButton AddTaskButton;
	private JLabel TaskFieldLabel;
	private String newTask;
	private JCheckBox newCheckBox;
	private JTextArea newTaskLabel;
	private JPanel curTaskPanel; 
	private JPanel addTaskPanel;
	private ArrayList<JPanel> jPanelList;
	
	private GridBagConstraints addTaskPanelConst;
	private int panelIndexTracker;
	
	
	public ToDoList() {
		setSize(600, 700);
		setTitle("To-Do List");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setContentPane(new JLabel(new ImageIcon("C:\\Users\\anglo_000\\Pictures\\LegalPadBackGroundImage.jpg")));
		setLayout(new FlowLayout());
		setResizable(false);
		jPanelList = new ArrayList<JPanel>();
		panelIndexTracker = 0;

		//Creation of the JPanel that will hold TextField and ADD Button to add new task
		newTaskPanel = new JPanel();
		newTaskPanel.setLayout(new FlowLayout());
		newTaskPanel.setOpaque(true);
		
		//creation of TextField and 'ADD' button
		TaskFieldLabel = new JLabel("New Task: "); //Labels the Text Box for new task entry.
		newTaskPanel.add(TaskFieldLabel);
		
		newTaskField = new JTextField(20); 		//Creates TextField box for new task entry.
		newTaskPanel.add(newTaskField);
	
		AddTaskButton = new JButton("ADD");		//Creates ADD button
		AddTaskButton.addActionListener(new AddTask());
		newTaskField.addKeyListener(new AddTask());
		newTaskPanel.add(AddTaskButton);
		
		
		add(newTaskPanel); 		//ADDS newTaskPanel to JFrame
		
		
		//Creation of JPanel for Current To-Do List
		curTaskPanel = new JPanel();
		curTaskPanel.setLayout(new GridBagLayout());
		curTaskPanel.setOpaque(false);
		curTaskPanel.setPreferredSize(new Dimension(430,600));
		curTaskPanel.setAlignmentX(LEFT_ALIGNMENT);
		curTaskPanel.setAlignmentY(TOP_ALIGNMENT);

		
		add(curTaskPanel);
		
		addTaskPanelConst = new GridBagConstraints();
		addTaskPanelConst.fill = GridBagConstraints.HORIZONTAL;
		addTaskPanelConst.anchor = GridBagConstraints.PAGE_START;
		addTaskPanelConst.gridx = 0;
		addTaskPanelConst.gridy = 0;
		addTaskPanelConst.weightx = 1;
		addTaskPanelConst.weighty = 1;
		
		for (int i = 0; i < 20; i++) {
			jPanelList.add(new JPanel());
			jPanelList.get(i).setOpaque(false);
			curTaskPanel.add(jPanelList.get(i), addTaskPanelConst);
			addTaskPanelConst.gridy +=1;
		}


	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new ToDoList().setVisible(true);
	}
	
	private class AddTask implements ActionListener, KeyListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub			
			if (panelIndexTracker < 15) {
				if (newTaskField.getText().trim().equals("") != true) { //If user wrote something...
					newTask = newTaskField.getText(); //get User Input, save to variable
					
					
					
					//Creation of JLabel containing newly added task
					newTaskLabel = new JTextArea(newTask); //create a label for new task
					newTaskLabel.setOpaque(false);
					newTaskLabel.setEditable(false);
					newTaskLabel.setLineWrap(true);
					newTaskLabel.setAlignmentX(LEFT_ALIGNMENT);
					newTaskLabel.setAlignmentY(TOP_ALIGNMENT);
					newTaskLabel.setMaximumSize(new Dimension(380, 20));
					
					//Creation of CheckBox associated with newly added task
					newCheckBox = new JCheckBox();	//create a checkbox
					//newCheckBox.setOpaque(false);
					newCheckBox.setAlignmentX(LEFT_ALIGNMENT);
					newCheckBox.setAlignmentY(TOP_ALIGNMENT);
					newCheckBox.setMaximumSize(new Dimension(20, 20));
	
					jPanelList.get(panelIndexTracker).add(newCheckBox);
					jPanelList.get(panelIndexTracker).add(newTaskLabel);
					newTaskField.setText("");
					curTaskPanel.revalidate();
					panelIndexTracker += 1;
				}
			}
		}

		@Override
		public void keyPressed(KeyEvent e) {
			// TODO Auto-generated method stub
			int kc = e.getKeyCode();
			if( kc == KeyEvent.VK_ENTER) {
				if (panelIndexTracker < 15) {
					if (newTaskField.getText().trim().equals("") != true) { //If user wrote something...
						newTask = newTaskField.getText(); //get User Input, save to variable
						
						addTaskPanel = new JPanel();	//create a panel to add to current task list panel
						addTaskPanel.setLayout(new FlowLayout());
						addTaskPanel.setOpaque(true);
						//addTaskPanel.setAlignmentX(LEFT_ALIGNMENT);
						//addTaskPanel.setAlignmentY(TOP_ALIGNMENT);
						
						
						//Creation of JLabel containing newly added task
						newTaskLabel = new JTextArea(newTask); //create a label for new task
						newTaskLabel.setOpaque(false);
						newTaskLabel.setEditable(false);
						newTaskLabel.setLineWrap(true);
						newTaskLabel.setAlignmentX(LEFT_ALIGNMENT);
						newTaskLabel.setAlignmentY(TOP_ALIGNMENT);
						newTaskLabel.setMaximumSize(new Dimension(380, 20));
						
						//Creation of CheckBox associated with newly added task
						newCheckBox = new JCheckBox();	//create a checkbox
						newCheckBox.setOpaque(true);
						newCheckBox.setAlignmentX(LEFT_ALIGNMENT);
						newCheckBox.setAlignmentY(TOP_ALIGNMENT);
						newCheckBox.setMaximumSize(new Dimension(20, 20));
	
						jPanelList.get(panelIndexTracker).add(newCheckBox);
						jPanelList.get(panelIndexTracker).add(newTaskLabel);
						addTaskPanel.setPreferredSize(new Dimension(400,20));
						//curTaskPanel.add(addTaskPanel, addTaskPanelConst);
						//addTaskPanelConst.gridy += 1;
						newTaskField.setText("");
						curTaskPanel.revalidate();
						panelIndexTracker += 1;
					}
				}
			}
				
			
		}

		
		public void keyReleased(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}

		
		public void keyTyped(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}

		
	}
	
}


	
