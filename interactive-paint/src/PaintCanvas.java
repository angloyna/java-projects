import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class PaintCanvas extends JPanel {
       
        private static final long serialVersionUID = 1L;        
        Graphics g2d;
        ArrayList<PointDrawn> circle_list;
        int cur_x, cur_y;
        Color line_color = Color.black;
        boolean first = true;
        int pen_radius = 5;        
        PaintGUI GUIAbove;
        boolean changeDrawType = true;
        String SavingDirectory = System.getProperty("user.dir");
        int tab;
                
        
public PaintCanvas(PaintGUI GUIAbove) {
    super();        
    setLayout(new BorderLayout());
    setBackground(Color.white);      
        circle_list = new ArrayList<PointDrawn>();
        this.GUIAbove = GUIAbove;
}

public void paintComponent(Graphics g2d) {
         super.paintComponent(g2d);
         PaintCanvas.this.PaintThis(g2d);
}

public void PaintThis(Graphics g2d){        
        for(PointDrawn circle: circle_list)
        {
        if(circle.draw){                
                        g2d.setColor(circle.clr);               
                        g2d.fillOval(circle.x1, circle.y1, circle.radius, circle.radius);
                }       
        else{   
                        g2d.setColor(circle.clr);               
                        g2d.drawOval(circle.x1, circle.y1, circle.radius, circle.radius);
                }
         }              
  }

public void drawCircle(Point p){
        if(first){
                first = false;
                cur_x = p.x;
                cur_y = p.y;
                return;
        }       
        PointDrawn add_circ = new PointDrawn(p.x -pen_radius/2, p.y-pen_radius/2, pen_radius , line_color, changeDrawType);
        circle_list.add(add_circ);
        GUIAbove.addAPointToBeSent(add_circ);
        repaint();
}

public void setCurPoint(Point p){
        cur_x = (int) p.getX();
        cur_y = (int) p.getY();
}

public void setColor(Color c){
         line_color = c;
}
public void setRadius(int radi){
         pen_radius = radi;
}
public void resetFirst()
    {first = true;}

public void newScreen(){        
        circle_list.clear();
        repaint();
}

public void addPoints(Vector<PointDrawn> newPoints) {
    circle_list.addAll(newPoints);
    repaint();
}

public void setDrawType(boolean change){
        changeDrawType = change;
}

public Boolean rightDirectory(File f) {
        if (f.getParent().equalsIgnoreCase(SavingDirectory)){
                return true;
        } else {
                return false;
        }

}

public Boolean canSaveFile(File f) {
        Boolean CanSave = rightDirectory(f);
        if(!CanSave) JOptionPane.showMessageDialog(this, "You can not save to that directory.", "Bad directory", JOptionPane.PLAIN_MESSAGE);
        return CanSave;
}

public void savePointsToFile(File f) {
        try {
                FileWriter fstream = new FileWriter(f);
                BufferedWriter out = new BufferedWriter(fstream);
                for(PointDrawn circle: circle_list){
                          String x= Integer.toString(circle.x1);
                          String y= Integer.toString(circle.y1);
                          String radi= Integer.toString(circle.radius);
                          System.out.println("RGB is :   " + circle.clr.getRGB());
                          String clr= Integer.toString(circle.clr.getRGB());
                      out.write(x + ":");
                      out.write(y + ":");
                      out.write(radi + ":");
                      out.write(clr + ":");
                      if(circle.draw){
                          out.write("1:");
                      }
                      else {
                          out.write("2:");
                      }
                      out.write("\n");
                  }
                  out.close();
        } catch (IOException e) {               
                e.printStackTrace();
        }


}

public void saveFile() throws IOException {
        JFileChooser fileChooser = new JFileChooser(SavingDirectory);    
        fileChooser.showSaveDialog(null);
        File f = fileChooser.getSelectedFile();
        if(f != null ) {
        	if(canSaveFile(f)) {
                savePointsToFile(f);
            }
        } else {
                System.out.println("No file selected");
        }
    }       


public void loadFile() throws FileNotFoundException{
         JFileChooser chooser = new JFileChooser(SavingDirectory);
         chooser.setDialogTitle("Pick the file you wish to load");       
         int result = chooser.showDialog(null, "Select");
         if(result != JFileChooser.APPROVE_OPTION){ 
         }         
	     File selected = chooser.getSelectedFile();   
	     GUIAbove.addNewTabForPaintingLoad(selected);
	     repaint();
       
}
public Vector<PointDrawn> getCircles(){
        Vector<PointDrawn> circles = new Vector<PointDrawn>(circle_list);
        return circles;
}
}