import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;



public class newUsersJFrame extends JFrame {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	JTextArea NewHostName;
	JTextArea NewPortNumber;
	PaintGUI AboveJFrame;
	
	public newUsersJFrame(PaintGUI AboveGUI) {
		this.setLayout(new GridLayout(5, 1));
		
		this.setSize(300, 300);
		this.setLocation(500, 500);
		this.AboveJFrame = AboveGUI;
		
		NewHostName =  new JTextArea();
		NewPortNumber =  new JTextArea();
		NewHostName.setBorder(BorderFactory.createLineBorder(Color.black));
		NewPortNumber.setBorder(BorderFactory.createLineBorder(Color.black));
		
		JLabel NewPortLabel = new JLabel("Enter Port Name Below");
		JLabel NewHostLabel = new JLabel("Enter Host Name Below");
		JButton Enter = new JButton("Connect");
		Enter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				AboveJFrame.addNewUser(NewHostName.getText(), NewPortNumber.getText());				
			}
		});
		
		add(NewHostLabel);
		add(NewHostName);
		
		add(NewPortLabel);
		add(NewPortNumber);
		add(Enter);
		
		NewHostName.setRows(5);
		NewPortNumber.setRows(5);
		
		
	}
	
	
}
