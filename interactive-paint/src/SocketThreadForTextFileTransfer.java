import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Vector;


public class SocketThreadForTextFileTransfer extends Thread {
	String hostName;
	int portNumber;
	Socket connectionToReceiver;
	int TypeOfMessage;
	private Vector<String> Text;
	
	public SocketThreadForTextFileTransfer(String hostName, int portNumber, Vector<String> Text) {
		this.hostName = hostName;
		this.portNumber = portNumber;
		this.Text = Text;
	}
	
	public void run() {
		try {
			connectionToReceiver = new Socket(hostName, portNumber);
			if (connectionToReceiver == null) return;
			DataOutputStream dos = new DataOutputStream(connectionToReceiver.getOutputStream());
			dos.writeInt(5);
			dos.writeInt(Text.size());
			for(int line = 0; line < Text.size(); line++) {
				dos.writeUTF(Text.get(line));
			}
			dos.flush();
			connectionToReceiver.close();
		} catch (UnknownHostException e) {			
			e.printStackTrace();
		} catch (IOException e) {			
			e.printStackTrace();
		}
	}
}