import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;


public class SocketThreadForAllDrawingsRequest extends Thread{
	Socket socket;
	HostNameAndPortNumber hostAndPort;
	PaintGUI AboveGUI;
	
	public SocketThreadForAllDrawingsRequest(HostNameAndPortNumber hostAndPort, PaintGUI AboveGUI){
		this.hostAndPort = hostAndPort;
		this.AboveGUI = AboveGUI;
	}
	public void run(){
		int con = AboveGUI.onlineUsers.indexOf(hostAndPort.getHostName());
		try {
			socket = new Socket(hostAndPort.getHostName(), hostAndPort.getPortNumber());
			if (socket == null) return;
			DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
			dos.writeInt(8);
			socket.close();
			String changeHost = "<html><b><font color = green>" + hostAndPort.getHostName() + "</font></b></html>";
            AboveGUI.users.set(con, changeHost);
		} catch (UnknownHostException e) {
			System.out.println("unk");
			
		} catch (IOException e) {
			String changeHost = "<html><b><font color = red>" + hostAndPort.getHostName() + "</font></b></html>";
            AboveGUI.users.set(con, changeHost ); 
		}
	}
}
