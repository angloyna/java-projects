import java.awt.image.BufferedImage;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;




public class SocketThreadForPictureTransfer {
	String hostName;
	int portNumber;
	Socket connectionToReceiver;
	int TypeOfMessage;
	private BufferedImage Image;
	
	public SocketThreadForPictureTransfer(String hostName, int portNumber, BufferedImage img) {
		this.hostName = hostName;
		this.portNumber = portNumber;
		this.Image = img;
	}
	
	public void run() {
		try {			
			connectionToReceiver = new Socket(hostName, portNumber);
			if(connectionToReceiver == null) return;
			int ImageWidth = Image.getWidth();
			int TotalPix = Image.getHeight() * Image.getWidth();
		
			DataOutputStream dos = new DataOutputStream(connectionToReceiver.getOutputStream());
			dos.writeInt(2);
			dos.writeInt(TotalPix);
			dos.writeInt(ImageWidth);
			for(int p = 0; p < TotalPix; p++) {
				dos.writeInt(Image.getRGB(p%ImageWidth, p/ImageWidth));
			}
			dos.flush();
			connectionToReceiver.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
}






