import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;


public class UpdateLoop extends Thread{
        private JTextArea chatArea;
        private JScrollPane scrollPane;
        private LinkedBlockingQueue<DrawingChange> drawingQueue;
    private LinkedBlockingQueue<String> chatQueue;
    String chatUpdate;
    DrawingChange drawingUpdate;
    PaintGUI aboveGUI;
   
        public UpdateLoop(JTextArea chatArea, JScrollPane scrollPane, LinkedBlockingQueue<DrawingChange> drawingQueue, LinkedBlockingQueue<String> chatQueue, PaintGUI aboveGUI){
                this.chatArea = chatArea;
                this.scrollPane = scrollPane;
                this.drawingQueue = drawingQueue;
                this.chatQueue = chatQueue;
                this.aboveGUI = aboveGUI;
        }
        public void run(){
                for(;;){
                        chatUpdate = chatQueue.poll();
                        if(chatUpdate != null){
                                chatArea.append(chatUpdate);
                                scrollPane.getVerticalScrollBar().setValue(scrollPane.getVerticalScrollBar().getMaximum());
                        }
                        drawingUpdate = drawingQueue.poll();
                        if(drawingUpdate != null){
                                aboveGUI.addPoints(drawingUpdate.getChanges(), drawingUpdate.getTab());
                        }
                }
        }
}