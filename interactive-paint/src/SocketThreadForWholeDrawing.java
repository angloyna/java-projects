import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Vector;


public class SocketThreadForWholeDrawing extends Thread{
	
	String hostName;
    int portNumber;
    Socket connectionToReceiver;
    Vector<PointDrawn> Changes;
    String UniqueStringofTabChanged;
    String tabTitle;
    PaintGUI AboveGUI;
    
    public SocketThreadForWholeDrawing(String hostName, int portNumber, String StringofTabChanged, String tabTitle, Vector<PointDrawn> Changes, PaintGUI AboveGUI) {
            this.hostName = hostName;
            this.portNumber = portNumber;
            this.Changes = Changes;
            this.UniqueStringofTabChanged = StringofTabChanged;
            this.tabTitle = tabTitle;
            this.AboveGUI = AboveGUI;
    }
    
    public void run() {
    		String modHostName = hostName.substring(0, hostName.indexOf("."));
	    	int con = AboveGUI.onlineUsers.indexOf(modHostName);
            try {
                    connectionToReceiver = new Socket(hostName, portNumber);
                    if(connectionToReceiver == null){
                    	System.out.println("Null Connectoin in STFWD");
                    	return;
                    }
                    DataOutputStream dos = new DataOutputStream(connectionToReceiver.getOutputStream());
                    dos.writeInt(9);
                    dos.writeUTF(UniqueStringofTabChanged);
                    dos.writeUTF(tabTitle);
                    dos.writeInt(Changes.size());
                    System.out.println("before for");
                    for(int c = 0; c < Changes.size(); c++) {
                            PointDrawn CurChange = Changes.get(c);
                            dos.writeInt(CurChange.x1);
                            dos.writeInt(CurChange.y1);
                            dos.writeInt(CurChange.radius);
                            dos.writeInt(CurChange.clr.getRGB());
                            int FakeBool;
                            if(CurChange.draw) {
                                    FakeBool = 1;
                            } else {
                                    FakeBool = 2;
                            }
                            dos.writeInt(FakeBool);
                            
                            if(c == Changes.size()-1) {
                                    System.out.println("xyr  "  + CurChange.x1 + "  "+ CurChange.y1 + "  "+ CurChange.radius); 
                            }
                    }
                   connectionToReceiver.close();
                   String changeHost = "<html><b><font color = green>" + modHostName + "</font></b></html>";
                   AboveGUI.users.set(con, changeHost);
            } catch (UnknownHostException e) {                   
                    e.printStackTrace();
            } catch (IOException e) {
            	String changeHost = "<html><b><font color = red>" + modHostName + "</font></b></html>";
                AboveGUI.users.set(con, changeHost);  
            }
            
    }
}
