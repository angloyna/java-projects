import java.util.Vector;


public class DrawingChange {
	Vector<PointDrawn> changes;
	String tab;
	
	public DrawingChange(Vector<PointDrawn> changes, String tabStr){
		this.changes = changes;
		tab = tabStr;
	}
	
	public Vector<PointDrawn> getChanges(){
		return changes;
	}
	
	public String getTab(){
		return tab;
	}
}