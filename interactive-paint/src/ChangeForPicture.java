public class ChangeForPicture {
	
	int x;
	int y;
	int radius;
	int NewColor;
	
	//Note: New Color is in RGB, an int
	
	public ChangeForPicture() {
		this.x = -1;
		this.y = -1;
		this.radius = -1;
		this.NewColor = -1;
	}
	
	public ChangeForPicture(int x, int y, int radius, int clr) {
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.NewColor = clr;
	}
}