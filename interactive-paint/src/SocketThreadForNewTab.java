import java.io.DataOutputStream;import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketThreadForNewTab extends Thread {
    String hostName;
    int portNumber;
    Socket connectionToReceiver;
    String TabName;
    String UniqueTabName;
    PaintGUI AboveGUI;
    
    public SocketThreadForNewTab(String hostName, int portNumber, String TabName, String UniqueTabName, PaintGUI AboveGUI) {
            this.hostName = hostName;
            this.portNumber = portNumber;
            this.TabName = TabName;
            this.UniqueTabName = UniqueTabName;
            this.AboveGUI = AboveGUI;
    }
    
    public void run() {
	    	int con = AboveGUI.onlineUsers.indexOf(hostName);
            try {
            	connectionToReceiver = new Socket(hostName, portNumber);
            	if(connectionToReceiver == null ) return;
                    DataOutputStream dos = new DataOutputStream(connectionToReceiver.getOutputStream());
                    dos.writeInt(4);
                    dos.writeUTF(TabName);
                    dos.writeUTF(UniqueTabName);
                    connectionToReceiver.close();
                    String changeHost = "<html><b><font color = green>" + hostName + "</font></b></html>";
                    AboveGUI.users.set(con, changeHost );
            } catch (UnknownHostException e) {                    
                    e.printStackTrace();
            } catch (IOException e) {
            	String changeHost = "<html><b><font color = red>" + hostName + "</font></b></html>";
                AboveGUI.users.set(con, changeHost );
            }
    }
}
