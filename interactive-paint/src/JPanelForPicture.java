import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;


@SuppressWarnings("serial")
public class JPanelForPicture extends JPanel {
	BufferedImage TheImage;
	
	public JPanelForPicture(BufferedImage img ) {
		this.TheImage = img;
	}
	
	@Override
	public void paint(Graphics g ) {
		g.drawImage(TheImage, 10, 10, Color.WHITE, this);
	}
}