import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;


public class SocketThreadForChat extends Thread {
	
	String message;
	String hostName;
	String UserName;
	int portNumber;
	Socket connectionToReceiver;
	PaintGUI AboveGUI;
	
	public SocketThreadForChat(String msg, String hostName, int portNumber, String UserName, PaintGUI AboveGUI) {
		this.message = msg;
		this.hostName = hostName;
		this.portNumber = portNumber;
		this.UserName = UserName;
		this.AboveGUI = AboveGUI;
	}
	
	public void run() {
		int con = AboveGUI.onlineUsers.indexOf(hostName);
		try {
			connectionToReceiver = new Socket(hostName, portNumber);			
			if( connectionToReceiver == null ) return;			
			DataOutputStream Writer = new DataOutputStream(connectionToReceiver.getOutputStream());
			
			Writer.writeInt(1);
			Writer.writeUTF(UserName.concat(" ").concat(message));
			Writer.flush();			
			connectionToReceiver.close();
			String changeHost = "<html><b><font color = green>" + hostName + "</font></b></html>";
            AboveGUI.users.set(con, changeHost );
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			String changeHost = "<html><b><font color = red>" + hostName + "</font></b></html>";
            AboveGUI.users.set(con, changeHost );    
		}
	}
}