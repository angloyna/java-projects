import java.net.*;

import java.util.concurrent.LinkedBlockingQueue;
import java.io.*;
import javax.swing.JTextArea;

public class Receiver extends Thread {
    /**
         * 
         */
        private static final long serialVersionUID = 1L;
        private ServerSocket accepter;
    Boolean destinationReceivedHeader;
    JTextArea chatArea;
    PaintGUI AboveGUI;
    ReceiverThread receiverThread;
    LinkedBlockingQueue<DrawingChange> drawingQueue;
    LinkedBlockingQueue<String> chatQueue;
    
    final int Message = 1;
    final int Picture = 2;
    final int Changes = 3;
    final int NewTab = 4;
    final int TextFile = 5;
    
    public Receiver(int port, JTextArea textArea, PaintGUI AboveGUI, LinkedBlockingQueue<DrawingChange> drawingQueue, LinkedBlockingQueue<String> chatQueue) throws IOException {
        this.AboveGUI = AboveGUI;
        this.chatArea = textArea;
        this.drawingQueue = drawingQueue;
        this.chatQueue = chatQueue;
        accepter = new ServerSocket(port);
    }

    public void run() {
        try {
                        listen();
                } catch (IOException e) {
                        
                        e.printStackTrace();
                }
    }
    
    public void listen() throws IOException {
        for (;;) { 
                Socket SocketConnection = accepter.accept();
                receiverThread = new ReceiverThread(drawingQueue, chatQueue, SocketConnection, AboveGUI);
        }
    }
}
