import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;


public class ReciprocalConnectionThread extends Thread {
    String hostName;
    int portNumber;
    Socket connectionToReceiver;
    String comingFromHostName;
    Socket connection;
    PaintGUI AboveGUI;
    
    public ReciprocalConnectionThread(String host, int port, String comingFromHostName, PaintGUI AboveGUI) {
            this.comingFromHostName = comingFromHostName;
            connectionToReceiver = connection;
            this.hostName = host;
            this.portNumber = port;
            this.AboveGUI = AboveGUI;
    }
    
    public void run() {
            int con = AboveGUI.onlineUsers.indexOf(hostName);
            try {
                         connectionToReceiver = new Socket(hostName, portNumber);
                         if(connectionToReceiver == null) {
                                 return;
                         }
                    DataOutputStream dos = new DataOutputStream(connectionToReceiver.getOutputStream());
                    dos.writeInt(6);
                    dos.writeUTF(comingFromHostName);
                    connectionToReceiver.close();
                    String changeHost = "<html><b><font color = green>" + hostName + "</font></b></html>";
                    AboveGUI.users.set(con, changeHost );
           } catch (UnknownHostException e) {                    
                    e.printStackTrace();
            } catch (IOException e) {                    
                String changeHost = "<html><b><font color = red>" + hostName + "</font></b></html>";
                AboveGUI.users.set(con, changeHost );                    
            }
    }
}