import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Vector;


public class SocketThreadForArrayOfChanges extends Thread {
        
        String hostName;
        int portNumber;
        Socket connectionToReceiver;
        Vector<PointDrawn> Changes;
        String UniqueStringofTabChanged;
        PaintGUI AboveGUI;
        
        public SocketThreadForArrayOfChanges(String hostName, int portNumber, String StringofTabChanged, Vector<PointDrawn> Changes, PaintGUI AboveGUI) {
                this.hostName = hostName;
                this.portNumber = portNumber;
                this.Changes = Changes;
                this.UniqueStringofTabChanged = StringofTabChanged;
                this.AboveGUI = AboveGUI;
        }
        
        public void run() {
	        	int con = AboveGUI.onlineUsers.indexOf(hostName);
                try {
                        connectionToReceiver = new Socket(hostName, portNumber);
                        if(connectionToReceiver == null) return;
                        DataOutputStream dos = new DataOutputStream(connectionToReceiver.getOutputStream());
                        dos.writeInt(3);
                        dos.writeUTF(UniqueStringofTabChanged);                       
                        dos.writeInt(Changes.size());
                        
                        for(int c = 0; c < Changes.size(); c++) {
                                PointDrawn CurChange = Changes.get(c);
                                dos.writeInt(CurChange.x1);
                                dos.writeInt(CurChange.y1);
                                dos.writeInt(CurChange.radius);
                                dos.writeInt(CurChange.clr.getRGB());
                                int FakeBool;
                                if(CurChange.draw) {
                                        FakeBool = 1;
                                } else {
                                        FakeBool = 2;
                                }
                                dos.writeInt(FakeBool);
                                
                                if(c == Changes.size()-1) {
                                }
                        }
                        connectionToReceiver.close();
                        String changeHost = "<html><b><font color = green>" + hostName + "</font></b></html>";
                        AboveGUI.users.set(con, changeHost );
                } catch (UnknownHostException e) {
                        
                } catch (IOException e) {
	                	String changeHost = "<html><b><font color = red>" + hostName + "</font></b></html>";
	                    AboveGUI.users.set(con, changeHost );   
                }
                
        }
}
