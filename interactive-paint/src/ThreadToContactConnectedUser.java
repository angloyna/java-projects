import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;


public class ThreadToContactConnectedUser extends Thread {
    String hostName;
    int portNumber;
    Socket connectionToReceiver;
    String MyHostName;
    PaintGUI AboveGUI;
    
    public ThreadToContactConnectedUser(String host, int Port, String MyHostName, PaintGUI AboveGUI) {
        this.MyHostName = MyHostName;
        this.hostName = host;
        this.portNumber = Port;
        this.AboveGUI = AboveGUI;
    }
    
    public void run() {
	    	int con = AboveGUI.onlineUsers.indexOf(hostName);
            try {
            	connectionToReceiver = new Socket(hostName, portNumber);
            	if ( connectionToReceiver == null ) {
            		System.out.println("Connection is null in Connecting users thread");
            		return;
            	}
                DataOutputStream dos = new DataOutputStream(connectionToReceiver.getOutputStream());
                dos.writeInt(7);
                dos.writeUTF(MyHostName);
                connectionToReceiver.close();
                String changeHost = "<html><b><font color = green>" + hostName + "</font></b></html>";
                AboveGUI.users.set(con, changeHost );
            } catch (UnknownHostException e) {                   
                    e.printStackTrace();
            } catch (IOException e) {
            	String changeHost = "<html><b><font color = red>" + hostName + "</font></b></html>";
                AboveGUI.users.set(con, changeHost );     
            }
    }
}