
public class HostNameAndPortNumber {
	String HostName;
	int PortNumber;
	
	public HostNameAndPortNumber(String HostName, int PortNumber) {
		this.HostName = HostName;
		this.PortNumber = PortNumber;
	}
	
	public String getHostName() {
		return HostName;
	}
	
	public int getPortNumber() {
		return PortNumber;
	}
}
