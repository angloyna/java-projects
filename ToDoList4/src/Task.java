import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;


public class Task {
	private JCheckBox checkButton;
	private JTextArea TaskTextArea;
	private JButton removeButton;
	private Font myFont;

public Task(JPanel panel, String task) {
	myFont = new Font("Courier", Font.BOLD, 18);
	createNewCheckBox(panel);
	createTextArea(panel, task);
	createRemoveButton(panel);
	
}
private void createNewCheckBox(JPanel panel) {
	checkButton = new JCheckBox();
    checkButton.setOpaque(false);
    panel.add(checkButton);
}

private void createRemoveButton(JPanel panel) {
	removeButton = new JButton("Remove");
	removeButton.setPreferredSize(new Dimension(90, 30));
	panel.add(removeButton);
}

private void createTextArea(JPanel panel, String task) {
	TaskTextArea = new JTextArea(task);
	initalizeTextAreaSettings();
	panel.add(TaskTextArea);
}

private void initalizeTextAreaSettings() {
	TaskTextArea.setColumns(42);
	TaskTextArea.setLineWrap(true);
	TaskTextArea.setFont(myFont);
	TaskTextArea.setOpaque(false);
}

public JCheckBox getCheckButton() {
	return checkButton;
}

public JTextArea getTextArea() {
	return TaskTextArea;
}

public JButton getRemoveButton() {
	return removeButton;
}


}

