import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.*;

@SuppressWarnings("serial")
public class ToDoList4 extends JFrame {

	private JTextField newTaskField;
	private JPanel newTaskPanel;
	private JPanel curTaskPanel;
	private JButton AddTaskButton;
	private JLabel TaskFieldLabel;
	private Task newTask;
	private JButton ClearAllSelected;
	private ArrayList<Task> SelectedTasks;
	
	public ToDoList4() {
		setSize(600, 700);
		setTitle("To-Do List");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new FlowLayout());
		setResizable(false);
		SelectedTasks = new ArrayList<Task>();
		
		setUpTaskInputPanel();
		createClearAllButton();
		createCurTaskPanel();
	}
	
	private void setUpTaskInputPanel() {
		createNewTaskPanel();
		createNewTextField();
		createAddButton();
		add(newTaskPanel);
	}
	
	private void createNewTaskPanel() {
		newTaskPanel = new JPanel();
		initalizePanelSettings(newTaskPanel, new Dimension(400, 35));
	}
	
	private void initalizePanelSettings(JPanel panel, Dimension size) {
		panel.setLayout(new FlowLayout());
		panel.setOpaque(false);
		panel.setPreferredSize(size);
	}
	
	private void createNewTextField() {
		createNewTaskLabel();
		newTaskField = new JTextField(20);
		newTaskPanel.add(newTaskField);
	}
	
	private void createNewTaskLabel() {
		TaskFieldLabel = new JLabel("Add Task: ");
		newTaskPanel.add(TaskFieldLabel);
	}
	
	private void createAddButton() {
		AddTaskButton = new JButton("ADD");		//Creates ADD button
		AddTaskButton.addActionListener(new AddTask());
		AddButtonRespondstoEnter();
		newTaskPanel.add(AddTaskButton);
	}
	
	private void AddButtonRespondstoEnter() {
		getRootPane().setDefaultButton(AddTaskButton);
	}
	
	private void createClearAllButton() {
		ClearAllSelected = new JButton("Clear Selected");
		ClearAllSelected.addActionListener(new ClearAllSelected(SelectedTasks));
		add(ClearAllSelected);
	}

	private void createCurTaskPanel() {
		curTaskPanel = new JPanel();
		initalizePanelSettings(curTaskPanel, new Dimension(600,550));
		add(curTaskPanel);
	}
	
	private class ClearAllSelected implements ActionListener {
		ArrayList<Task> SelectedTaskstoRemove;
		
		public ClearAllSelected(ArrayList<Task> array) {
			SelectedTaskstoRemove = array;
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			for (int i = 0; i < SelectedTaskstoRemove.size(); i++) {
				removeTaskFromToDoList(SelectedTaskstoRemove.get(i));
			}
			refreshToDoList();
			SelectedTaskstoRemove.clear();
			
		}
		
	}

	private class AddTask implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {		
			if (UserEnteredText()) { 					
					makeAnewTask();
					newTaskField.setText("");
					refreshToDoList();
			}
		}

	}

	private boolean UserEnteredText() {
		return !newTaskField.getText().trim().equals("");
	}
	
	private void makeAnewTask() {
		createNewTaskItem();
	}
	
	private void createNewTaskItem() {
		newTask = new Task(curTaskPanel, getUserInput());
		newTask.getRemoveButton().addActionListener(new removeTask(newTask));
		newTask.getCheckButton().addActionListener(new SelectListener(newTask, SelectedTasks));
	}
	
	private String getUserInput() {
		return newTaskField.getText().trim();
	}

	private void refreshToDoList() {
		curTaskPanel.revalidate();
		curTaskPanel.repaint();
	}
	
	private class removeTask implements ActionListener {
		private Task taskItem;
		
		public removeTask(Task task) {
			taskItem = task;
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			removeTaskFromToDoList(taskItem);
			refreshToDoList();	
		}
	}

	private void removeTaskFromToDoList(Task task) {	
		curTaskPanel.remove(task.getCheckButton());
		curTaskPanel.remove(task.getTextArea());
		curTaskPanel.remove(task.getRemoveButton());
	}
	
	private class SelectListener implements ActionListener {
		ArrayList<Task> Selected;
		Task curTask;
		
		public SelectListener(Task task, ArrayList<Task> SelectedList) {
			Selected = SelectedList;
			curTask = task;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			Selected.add(curTask);
		}
	}

	public static void main(String[] args) {
		new ToDoList4().setVisible(true);
	}

}
