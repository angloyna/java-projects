
public class MergeSort {
	private int[] sorted;
	
	public void sort( int[] array ) {
		int[] copy = new int[ array.length ];
		System.arraycopy(array, 0, copy, 0, array.length);
		sorted = mergeSort( copy );
	}
	
	private int[] mergeSort( int[] array ) {
		//Implement the mergesort algorithm via recursion
		//Don't forget about the base case(s)!
		if (array.length <= 1) {
			return array;
		}
		int midpt = array.length / 2;
		int[] left = new int[midpt];
		int[] right = new int[array.length - midpt];
		System.arraycopy(array, 0, left, 0, midpt);
		System.arraycopy(array, midpt, right, 0, array.length- midpt);
		left = mergeSort(left);
		right = mergeSort(right);
		merge(left, right, array);
		return array;
	}
	
	private void merge( int[] left, int[] right, int[] merged ) {
		int leftIndex = 0;
		int rightIndex = 0;
		int mergedIndex = 0;

		//merge the left and right arrays.  When one runs out finish 
		// the other into the "merged" array
		while ((leftIndex < left.length) && (rightIndex < right.length)) {
				if (left[leftIndex] > right[rightIndex]) {
					merged[mergedIndex] = right[rightIndex];
					rightIndex++;
				} else {
					merged[mergedIndex] = left[leftIndex];
					leftIndex++;
				}
				mergedIndex++;
		}
		while (leftIndex < left.length) {
				merged[mergedIndex] = left[leftIndex];
				leftIndex++;
				mergedIndex++;
		}
		while (rightIndex < right.length) {
			merged[mergedIndex] = right[rightIndex];
			mergedIndex++;
			rightIndex++;
		}
	}
	
	public int[] getSortedArray() { return sorted; }
	
	public static void main(String[] args) {
		//Test your implementation here.  Remember that you must
		// create an instance of this class in order to use its
		// non-static public methods.
		
		MergeSort sort = new MergeSort();
		int[] array = {67,9,3,4,56};
		System.out.print("Initial Array for Testing Merge Sort: ");
		for (Integer entry : array) {
			System.out.print(entry + " ");
		}
		System.out.println(" ");

		sort.sort(array);
		int[] result = sort.getSortedArray();
		System.out.print("Array after sorting: ");
		for (int i = 0; i < 5; i++) {
			System.out.print(result[i] + " ");
		}
		System.out.println(" ");
		
		//Testing base cases
		int[] array2 = new int[0];
		System.out.print("Initial Array for Testing Merge Sort: ");
		for (Integer entry : array2) {
			System.out.print(entry + " ");
		}
		System.out.println(" ");
		
		sort.sort(array2);
		result = sort.getSortedArray();
		System.out.print("Array after sorting: ");
		for (Integer entry : result) {
			System.out.print(entry + " ");
		}
		System.out.println(" ");
		
		int[] array3 = new int[1];
		array3[0] = 45;
		System.out.print("Initial Array for Testing Merge Sort: ");
		for (Integer entry : array3) {
			System.out.print(entry + " ");
		}
		System.out.println(" ");
		
		sort.sort(array3);
		result = sort.getSortedArray();
		System.out.print("Array after sorting: ");
		for (Integer entry: result) {
			System.out.print(entry + " ");
		}
		System.out.println(" ");
	}
}
