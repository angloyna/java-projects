
import java.util.ArrayList;

public class MaxHeap {
	private ArrayList<Integer> heap;
	
	public MaxHeap() { 
		heap = new ArrayList<Integer>();		
	}
	
	public MaxHeap( int[] entries ) {
		//Complete this constructor
		//move entries into heap var, then heapify.
		heap = new ArrayList<Integer>();
		for (Integer entry : entries) {
			heap.add(entry);
		}
		this.heapify();
	}
	
	public void add( int val ) {
		//Complete this method
		heap.add(heap.size(), val);
		this.up_heap(heap.size() - 1);
	}
	
	public int remove() {
		int val = heap.get( 0 );
		Integer lastEntry = heap.remove( heap.size()-1 );
		if( !heap.isEmpty() ) {
			heap.set( 0, lastEntry );
			down_heap( 0 );
		}
		return val;
	}
	
	private void up_heap( int position ) {
		//Complete this method
		int parentIndex = (position - 1)/2;
		if (parentIndex < 0) {
			return;
		}
		int leftIndex = (2 * parentIndex) + 1;
		int rightIndex = (2 * parentIndex) + 2;
		int parentVal = heap.get(parentIndex);
		if ((leftIndex < heap.size()) && (rightIndex < heap.size())) {
			int leftVal = heap.get(leftIndex);
			int rightVal = heap.get(rightIndex);
			if (leftVal > rightVal) {
				if (leftVal > parentVal) {
					this.swap(parentIndex, leftIndex);
				}
				
			
			
			}
			if (rightVal > leftVal) {
				if (rightVal > parentVal) {
					this.swap(parentIndex, rightIndex);
				}
			}
		}
		if ((leftIndex < heap.size()) && (rightIndex >= heap.size())) {
			int leftVal = heap.get(leftIndex);
			if (leftVal > parentVal) {
				this.swap(parentIndex, leftIndex);
			}
		}
		if ((leftIndex >= heap.size()) && (rightIndex < heap.size())) {
			int rightVal = heap.get(rightIndex);
			if (rightVal > parentVal) {
				this.swap(parentIndex, rightIndex);
			}
		}
		if (parentIndex == 0) {
			return;
		}
		this.up_heap(parentIndex);
	}
	private void down_heap( int position ) {
		//Complete this method
		int parentIndex = position;
		int leftIndex = (2 * parentIndex) + 1;
		int rightIndex = (2 * parentIndex) + 2;
		if ((leftIndex >= heap.size()) && (rightIndex >= heap.size())) {
			return;
		}
		int parentVal = heap.get(parentIndex);
		if ((leftIndex < heap.size()) && (rightIndex < heap.size())) {
			int leftVal = heap.get(leftIndex);
			int rightVal = heap.get(rightIndex);
			if (leftVal > rightVal) {
				if (leftVal > parentVal) {
					swap(parentIndex, leftIndex);
					this.down_heap(leftIndex);
					this.down_heap(parentIndex);
				}
			}
			if (rightVal > leftVal) {
				if (rightVal > parentVal) {
					swap(parentIndex, rightIndex);
					this.down_heap(parentIndex);
					this.down_heap(rightIndex);
				}
			}
		}
		if ((leftIndex < heap.size()) && (rightIndex >= heap.size())) {
			int leftVal = heap.get(leftIndex);
			if (leftVal > parentVal) {
				swap(parentIndex, leftIndex);
				this.down_heap(parentIndex);
				this.down_heap(leftIndex);
			}
		}
		if ((leftIndex >= heap.size()) && (rightIndex < heap.size())) {
			int rightVal = heap.get(rightIndex);
			if (rightVal > parentVal) {
				swap(parentIndex, rightIndex);
				this.down_heap(parentIndex);
				this.down_heap(rightIndex);
			}
		}
	}
	
	private void heapify() {
		//Complete this method
		int index = heap.size() - 2 / 2;
		while (index >= 0) {
			this.down_heap(index);
			index--;
		}
	}
	
	public boolean isEmpty() { return heap.isEmpty(); }
	
	private void swap( int pos1, int pos2 ) {
		Integer temp = heap.get( pos1 );
		heap.set( pos1, heap.get( pos2 ) );
		heap.set( pos2, temp );
	}
	
	//helper method during implementation
	public void printHeap() {
		if( heap.isEmpty() ) {
			System.out.println( "The heap is empty!" );
			return;
		}
		for( Integer entry : heap ) {
			System.out.print( entry + "  " );
		}
		System.out.println();
	}
	
}

