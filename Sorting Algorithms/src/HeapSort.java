
public class HeapSort {
	private int[] sorted;
	private MaxHeap m_heap;
	
	public HeapSort( int[] array ) { 
		sorted = new int[ array.length ];
		m_heap = new MaxHeap( array );
	}
	
	public void sort() {
		//Complete this method
		int sortedIndex = sorted.length - 1;
		while (!m_heap.isEmpty()) {
			sorted[sortedIndex] = m_heap.remove();
			sortedIndex--;
		}
	}
	
	public int[] getSortedArray() { return sorted; }
	
	public static void main(String[] args) {
		//Use this to test your implementation of MaxHeap and 
		//HeapSort.  Don't forget to get an instance of this
		//class in order to access its non-static public methods.
		int[] array = {5,8,2,45,23,6,780,87,34,26};
		System.out.print("Initial array before HeapSort: ");
		for (Integer entry : array) {
			System.out.print(entry + " ");
		}
		System.out.println(" ");
		HeapSort sort = new HeapSort(array);
		sort.sort();
		int[] result = sort.getSortedArray();
		System.out.print("Array after Heapsort: ");
		for (int i = 0; i < result.length; i++) {
			System.out.print(result[i] + " ");
		}
		System.out.println(" ");
		
		int[] array3 = new int[0];
		HeapSort sort3 = new HeapSort(array3);
		System.out.print("(Testing empty array) Initial array before HeapSort: ");
		for (Integer entry : sort3.getSortedArray()) {
			System.out.print(entry + " ");
		}
		System.out.println(" ");
		sort3.sort();
		System.out.print("Array after HeapSort: ");
		int[] result2 = sort3.getSortedArray();
		for (Integer entry : result2) {
			System.out.print(entry + " ");
		}
		System.out.println(" ");
		int[] array4 = new int[1];
		array4[0] = 56;
		HeapSort heap3 = new HeapSort(array4);
		System.out.print("(Testing Heapsort with one element)Array before HeapSort: ");
		for (Integer entry : array4) {
			System.out.print(entry);
		}
		System.out.println(" ");
		heap3.sort();
		int[] result5 = heap3.getSortedArray();
		System.out.print("Array after Heapsort: ");
		for (Integer entry : result5) {
			System.out.print(entry + " ");
		}
		System.out.println(" ");
		MaxHeap heap = new MaxHeap(array);
		System.out.print("Heap before adding: ");
		heap.printHeap();
		heap.add(79331);
		System.out.print("Heap after adding 79331: ");
		heap.printHeap();
		System.out.print("Heap after removing: ");
		heap.remove();
		heap.printHeap();
		heap.add(134);
		System.out.print("Heap after adding 134: ");
		heap.printHeap();
		
		int[] array2 = new int[0];
		MaxHeap heap2 = new MaxHeap(array2);
		System.out.print("(Testing empty array) Heap before adding: ");
		heap2.printHeap();
		heap2.add(34);
		System.out.print("Heap after adding 34: ");
		heap2.printHeap();
	}

}
