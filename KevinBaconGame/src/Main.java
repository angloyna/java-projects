import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;


public class Main {

	public static void main(String[] args) throws FileNotFoundException {
		Parser parser = new Parser();
		boolean running = true;
		UndirectedGraph<String> graph = new UndirectedGraph<String>();
		ArrayList<movieActor> movieActorList = parser.parseFile("imdb_top250.txt");
		for (movieActor i : movieActorList) {
			if (!graph.containsVertex(i.getActorName())) {
				graph.addVertex(i.getActorName());
			}
			if (!graph.containsVertex(i.getMovieName())) {
				graph.addVertex(i.getMovieName());
			}
			if (!graph.containsEdge(i.getActorName(), i.getMovieName())) {
				graph.addEdge(i.getActorName(), i.getMovieName());
			}
		}
		System.out.println(graph.getVertexCount());
		Scanner input = new Scanner(System.in);
		System.out.println("Welcome to the Kevin Bacon Game. This program can tell you the degrees of separation between" +
				" any two actors" + "\n" +"that exist within its database." +
				" When entering the names of the actors, please capitalize each first and last name," + "\n" + "including hyphens for those " +
				"with multiple last names.");
		
		System.out.println("Enter the base Actor: ");
		String baseActor = input.nextLine();
		baseActor = baseActor.replaceAll("[^a-zA-Z\\s-]","");
		baseActor = baseActor.trim();
		System.out.println("You entered " + baseActor + ".");
		Dijkstra Dalgorithm = new Dijkstra(graph, baseActor);
		int failCheck = Dalgorithm.runAlgorithm();
		if (failCheck < 0) {
			running = false;
		}
		while (running) {
			System.out.println("Please enter an actor for whom you would like to know the " + Dalgorithm.getBaseActor() + " number.");
			String destActor = input.nextLine();
			destActor = destActor.replaceAll("[^a-zA-Z\\s-]","");
			destActor = destActor.trim();
			int baconNumber = Dalgorithm.getBaconNumber(destActor);
			if (baconNumber < 0) {
				running = false;
				continue;
			}
			System.out.println("The " + Dalgorithm.getBaseActor() + " number for " + destActor + " is " + baconNumber + ".");
			Dalgorithm.printRoute(destActor);
			System.out.println("Would you like to enter another actor? Please enter yes or no. If you " +
					"would like to enter a new base actor, enter 'new'.");
			String response = input.nextLine();
			response = response.replaceAll("[^A-Za-z]", "");
			response = response.toLowerCase().trim();
			if (response.matches("new")) {
				System.out.println("Please enter the new actor.");
				response = input.nextLine();
				response = response.replaceAll("[^A-Za-z\\s-]", "");
				response = response.trim();
				System.out.println("You entered " + response + ".");
				Dalgorithm = new Dijkstra(graph, response);
				Dalgorithm.runAlgorithm();
				continue;
			}
			if (response.matches("no")) {
				running = false;
				System.out.println("Thanks for playing.");
				input.close();
				continue;
			} if (!response.matches("yes")) {
				System.out.println("I'm sorry. Your response could not be processed. If you would like to continue, please enter yes or no.");
				response = input.nextLine();
				response = response.replaceAll("[^A-Za-z]", "");
				response = (response.toLowerCase()).trim();
			}
		}
	}

}
