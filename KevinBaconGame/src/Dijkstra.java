import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;


public class Dijkstra {
	private HashMap<String, Route<String>> routeMap; //maps string to corresponding route instance
	private HashSet<String> queueSet; //Indicates which vertices are in queue or have been in queue
	private UndirectedGraph<String> graph; //graph that is passed in from main
	private String baseActor; //base actor from main
	private LinkedList<String> queue; //queue for vertices with neighbors needing to be visited
	
public Dijkstra(UndirectedGraph<String> graph, String baseActor) {
	this.graph = graph;
	this.baseActor = baseActor;
	queue = new LinkedList<String>();
	queueSet = new HashSet<String>();
	routeMap = new HashMap<String, Route<String>>();
}

public String getPrevRoute(String cur) {
	return routeMap.get(cur).getPrev();
}

public void setBaseActor(String baseActor) {
	this.baseActor = baseActor;
}

public String getBaseActor() {
	return baseActor;
}

public int getPrevDist(String cur) {
	return routeMap.get(getPrevRoute(cur)).getNumEdges();
}

public int getNewDist(String cur) {
	return (routeMap.get(cur).getNumEdges() + 1);
}

public int runAlgorithm() { //Algorithm finds the route for everything
	String cur;
	Set<String> neighbors;
	if (graph.containsVertex(baseActor)) {
		queue.add(baseActor);
		queueSet.add(baseActor);
		routeMap.put(baseActor, new Route<String>(baseActor, 0));
		while (!queue.isEmpty()) {
			cur = queue.pop();
			if (!routeMap.containsKey(cur)) {
				routeMap.put(cur, new Route<String>(getPrevRoute(cur), getPrevDist(cur)));
			}
			if (graph.getNeighborCount(cur) > 0) {
				neighbors = graph.getNeighbors(cur);
				for (String i : neighbors) {
					if (!queueSet.contains(i)) {
						queue.add(i);
						queueSet.add(i);
						routeMap.put(i, new Route<String>(cur, getNewDist(cur)));
					}
				}
			}
		}
	} else {
		System.out.println("I'm sorry. " + baseActor + " could not be found. Consider reentering the name if you did not use the proper format.");
		return -1;
	}
	return 1;
}

public int getBaconNumber(String destActor) {
	if (!graph.containsVertex(destActor)) {
		System.out.println("I'm sorry. " + destActor + " could not be found in the database. Consider reentering the name if you did not use the proper format.");
		return -1;
	}
	if (!routeMap.containsKey(destActor)) {
		System.out.println("I'm sorry. A path between " + destActor + " and " + baseActor + " could not be found. Consider reentering the name if you did not use the proper format.");
		return -1;
	}
	return routeMap.get(destActor).getNumEdges() / 2;
}

public void printRoute(String destActor) {
	String finalAnswer = "";
	String cur = destActor;
	int count = 0;
	if (destActor.matches(baseActor)) {
		System.out.println(destActor + " is " + baseActor + ".");
	}
	if (!destActor.matches(baseActor)) {
		while (cur != baseActor) {
			if (count == 0) {
				finalAnswer += cur + " was in ";
			}
			if (count != 0 && (count % 2 == 0)) {
				finalAnswer += cur + " who was in ";
			}
			if (count % 2 != 0) {
				finalAnswer += cur + " with ";
			}
			cur = routeMap.get(cur).getPrev();
			count++;
		}
		finalAnswer += cur + ".";
		System.out.println(finalAnswer);
	}
}
}


