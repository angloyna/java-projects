import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;


public class Parser {
	private ArrayList<movieActor> parsedList;

public Parser() {
	parsedList = new ArrayList<movieActor>();
}
public ArrayList<movieActor> parseFile(String filename) throws FileNotFoundException {
	File f = new File(filename);
	Scanner input = new Scanner(f);
	while( input.hasNextLine() )  {
		parsedList.add(this.parsedLine(input.nextLine()));
	}
	input.close();
	return parsedList;
}
public movieActor parsedLine(String arg) {
	String[] names = new String[2];
	movieActor movieActor;
	names = arg.split("\\|");
	if (names[0].endsWith(")")) {
		names[0] = names[0].substring(0,names[0].lastIndexOf(" "));
	}
	movieActor = new movieActor(names[1], names[0]);
	return movieActor;
}

public void printParsedList() {
	for (movieActor k : parsedList) {
		k.printMovieActor();
	}
}

public int ParsedListLength() {
	return parsedList.size();
}

public static void main( String[] args ) throws FileNotFoundException {
	Parser parser = new Parser();
	parser.parseFile("imdb_top250.txt");
	parser.printParsedList();
}
}
