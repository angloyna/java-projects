
public class movieActor {
	private String movieName;
	private String actorName;
	
public movieActor(String movieName, String actorName) {
		this.movieName = movieName;
		this.actorName = actorName;
}

public String getMovieName() {
	return movieName;
}

public String getActorName() {
	return actorName;
}

public void printMovieActor() {
	System.out.println(getActorName());
	System.out.println(getMovieName());
}
}
