import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class JuggleFest {
	private static ArrayList<Circuit> CircuitList;
	private static CircuitParse CircuitParse;
	private static JuggleParse JuggleParse;
	private static ArrayList<Juggler> JugglerList;
	
	public JuggleFest() {
		
	}
	 public static void main(String[] args) {
		 BufferedReader br = null;
		 CircuitList = new ArrayList<Circuit>();
		 CircuitParse = new CircuitParse();
		 JuggleParse = new JuggleParse();
		 JugglerList = new ArrayList<Juggler>();
		 UndirectedGraph<String> graph = new UndirectedGraph<String>();
		 
		 try {
		  
		 String sCurrentLine;
		  
		 br = new BufferedReader(new FileReader("C:\\jugglefest.txt"));
		  
		 while ((sCurrentLine = br.readLine()) != null) {
		 	if (sCurrentLine.startsWith("C")) {
		 			CircuitList.add(CircuitParse.createCircuit(sCurrentLine));
		 			
		 	}
		 	if (sCurrentLine.startsWith("J")) {
		 		JugglerList.add(JuggleParse.createJuggler(sCurrentLine));
		 	}
		 }
		 
		 for (Circuit curCircuit : CircuitList) {
				if (!graph.containsVertex(curCircuit.getCircuitName())) {
					graph.addVertex(curCircuit.getCircuitName());
				}
		}
		 
		for (Juggler curJuggler : JugglerList) {
			if (!graph.containsVertex(curJuggler.getJugglerName())) {
				graph.addVertex(curJuggler.getJugglerName());
			}
			for (String i : curJuggler.getPreferences()) {
				if (!graph.containsEdge(curJuggler.getJugglerName(), i)) {
					graph.addEdge(curJuggler.getJugglerName(), i);
				}
			}
		}
		 } catch (IOException e) {
		 e.printStackTrace();
		 } finally {
		 try {
		 if (br != null)br.close();
		 } catch (IOException ex) {
		 ex.printStackTrace();
		 }
		 } 	}
}
