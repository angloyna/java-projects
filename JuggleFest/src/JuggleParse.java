


public class JuggleParse {
	private Juggler tempJuggler;
	private String unParsedJuggler;
	private String[] tempArray;
	private String[] preferences;
	
	public JuggleParse() {
	}

	public Juggler createJuggler(String sCurrentLine) {
		unParsedJuggler = sCurrentLine;
		tempArray = unParsedJuggler.split(" ");
		preferences = tempArray[5].split(",");
		for (String i : preferences) {
			i.trim();	
		}
		tempJuggler = new Juggler(tempArray[1].trim(), tempArray[2].split(":")[1].trim(), tempArray[3].split(":")[1].trim(), tempArray[4].split(":")[1].trim(), preferences);
		return tempJuggler;
	}

}