


public class CircuitParse {
	private Circuit tempCircuit;
	private String unParsedCircuit;
	private String[] tempArray;
	
	public CircuitParse() {
	}

	public Circuit createCircuit(String sCurrentLine) {
		unParsedCircuit = sCurrentLine;
		tempArray = unParsedCircuit.split(" ");
		tempCircuit = new Circuit(tempArray[1], tempArray[2].split(":")[1], tempArray[3].split(":")[1], tempArray[4].split(":")[1]);
		return tempCircuit;
	}

}
