import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;


public class CircuitTeamMatch {
	//private HashMap<String, Route<String>> routeMap; //maps string to corresponding route instance
	private ArrayList<Juggler> jugglerSet; //Indicates which vertices are in queue or have been in queue
	private UndirectedGraph<String> graph; //graph that is passed in from main
	//private String baseActor; //base actor from main
	private ArrayList<Circuit> CircuitList; //queue for vertices with neighbors needing to be visited
	
	public CircuitTeamMatch(UndirectedGraph<String> graph, ArrayList<Circuit> circuits, ArrayList<Juggler> jugglers) {
		this.graph = graph;
		this.CircuitList = circuits;
		this.jugglerSet = jugglers;

	}
	
	public void runAlgorithm() {
	//no sir. not done.
		
		
		for (Circuit curCircuit : CircuitList) {
			if (!routeMap.containsKey(curCircuit)) {
				routeMap.put(cur, new Route<String>(getPrevRoute(cur), getPrevDist(cur)));
			}
			if (graph.getNeighborCount(cur) > 0) {
				neighbors = graph.getNeighbors(cur);
				for (String i : neighbors) {
					if (!queueSet.contains(i)) {
						queue.add(i);
						queueSet.add(i);
						routeMap.put(i, new Route<String>(cur, getNewDist(cur)));
					}
				}
			}
		}
	}

}
