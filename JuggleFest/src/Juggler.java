import java.util.HashMap;


public class Juggler {
	private String HandEye;
	private String Endurance;
	private String Pizazz;
	private String JugglerName;
	private String[] Preferences;
	private HashMap<String, Integer> PreferenceMatch;
	

	public Juggler(String name, String H, String E, String P, String[] pref) {
		HandEye = H;
		Endurance = E;
		Pizazz = P;
		JugglerName = name;
		Preferences = pref;
	}
	
	public void printE() {
		System.out.println(Endurance);
	}
	
	public void printPref() {
		for (int i = 0; i < 3; i++) {
			System.out.println(Preferences[i]);
		}
	}

	public String getJugglerName() {
		return JugglerName;
	}

	public String[] getPreferences() {
		return Preferences;
	}
}
