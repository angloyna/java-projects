
public class BubbleSorter extends Sorter {

	public BubbleSorter() {
		super();
	}

	@Override
	public void sort(int[] array, int minIndex, int maxIndex) {
		for (int i = minIndex; i <= maxIndex; ++i) {
			for (int j = minIndex; j <= maxIndex - i - 1; ++j) {
				if (array[j] > array[j+1]) {
					swap(array, j, j+1);
				}
			}
		}
	}
	
	public static void main(String[] args) {
		Sorter.test(new BubbleSorter());
	}
}
