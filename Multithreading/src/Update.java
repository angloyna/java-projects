//This class encapsulates the index/value pairs for an array.  Each "assign" that occurs results in an update of
// the value at a specified index in the array
public class Update {
	private int index;
	private int value;
	
	public Update(int index, int value) {
		this.index = index;
		this.value = value;
	}
	
	public void update(int[] array) {
		array[index] = value;
	}
	
	//This method is here for debugging purposes only!  YOu can use it to write to the console while you
	// are developing your solution.
	public String toString() {
		return index + ": [" + value + "]";
	}
	
}
