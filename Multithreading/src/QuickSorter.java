public class QuickSorter extends Sorter {

	public QuickSorter() {
		super();
	}

	@Override
	public void sort( int[] array, int minIndex, int maxIndex ) {
		//make a duplicate copy of the original
		quickSort( array, minIndex, maxIndex);
	}
	
	private void quickSort( int[] array, int left, int right ) {
		int index = partition( array, left, right ); //partition based on value in "middle" of array abd find location of partition split
		if( left < index - 1 ) 
			quickSort( array, left, index - 1 ); 
		if( index < right ) 
			quickSort( array, index, right ); 
	}
	
	private int partition( int array[], int left, int right ) { 
		int left_idx = left, right_idx = right; 
		int pivot = array[(left + right) / 2]; //taking the pivot to be the value in the middle of the array in question
  
		while( left_idx <= right_idx ) { 
			//move left_idx right until the array value is >= pivot
			while( array[left_idx] < pivot ) 
				left_idx++;
			//move right_idx left until the array value is <= pivot
			while( array[right_idx] > pivot ) 
				right_idx--; 
			if( left_idx <= right_idx ) { //left_idx and right_idx have not crossed each other
				swap( array, left_idx, right_idx );
				left_idx++; 
				right_idx--; 
			} 
		} 
		return left_idx; 

	}
	
	public static void main(String[] args) {
		//Although we cannot make an instance of class Sorter we can ask this abstract
		// class to run its test method
		Sorter.test(new QuickSorter());
	}
}