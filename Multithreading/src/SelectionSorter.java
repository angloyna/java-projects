
public class SelectionSorter extends Sorter {
	
	public SelectionSorter(){
		super();
	}
	@Override
	public void sort(int[] array, int minIndex, int maxIndex) {
		int idx_of_min_val;
		for(int idx = minIndex; idx <= maxIndex; idx++) {
			idx_of_min_val = idx;
			for(int run_idx = idx; run_idx <= maxIndex; run_idx++) {
				if( array[run_idx] < array[idx_of_min_val]) {
					idx_of_min_val = run_idx;
				}
			}			   			   
			swap(array, idx, idx_of_min_val);
		}//outer for loop			 
	}
	
	public static void main(String[] args) {
		Sorter.test(new SelectionSorter());
	}
}