
abstract public class Sorter {
	private AnimationThread messenger;
	
	public Sorter() {
		messenger = null;
	}
	
	public void setMessenger(AnimationThread msgr) {
		this.messenger = msgr;
	}
	
	//The following allows for the sorting of portions of the array
	abstract public void sort(int[] array, int minIndex, int maxIndex);
	
	public void sort(int[] array) {
		sort(array, 0, array.length - 1);
	}

	//The following method will tell the animation thread to update the
	// animation by moving the "value" to the new position "i"
	public void assign(int[] array, int i, int value) {
		array[i] = value;
		if (messenger != null) {
			messenger.sendUpdate(new Update(i, value));
		}
	}

	public void swap(int[] array, int i, int j) {
		int temp = array[i];
		assign(array, i, array[j]);
		assign(array, j, temp);
	}
	
	//As in our "main" methods, declaring the following method as static 
	// allows us access to it without having an instance of the class 
	// (which we cannot do since it is an abstract class).
	//This method takes the given array and "shuffles"/"swaps" each entry with
	// another randomly chosen entry to create a "scrambled" array.
	//This method DOES NOT GUARANTEE that any of the entries of the original
	// array actually does get moved, but the probability that no movement
	// actually occurs is extremely tiny.
	public static void scramble(int[] array) {
		for (int i = 0; i < array.length - 1; ++i) {
			int target = (int)(Math.random() * (array.length - i) + i);
			int temp = array[i];
			array[i] = array[target];
			array[target] = temp;
		}
	}
	
	//The same is true here as well
	public static boolean isSorted(int[] array) {
		for (int i = 0; i < array.length - 1; ++i) {
			if (array[i] > array[i+1]) {
				return false;
			}
		}
		return true;
	}
	
	//The same is true for this method as well.  See how we called the previous
	// two static methods below.  This class is
	public static void test(Sorter sorter) {
		int[] nums = new int[100];
		for (int i = 0; i < nums.length; ++i) {
			nums[i] = i;
		}
		
		Sorter.scramble(nums);
		System.out.println("initial: ");
		for (Integer i: nums) {System.out.print(i + " ");}
		System.out.println();
		
		sorter.sort(nums); //We want the SPECIFIC sorting algorithm here!!
		if (Sorter.isSorted(nums)) {
			System.out.println("Sort successful");
			for (Integer i: nums) {System.out.print(i + " ");}
			System.out.println();
		} else {
			System.out.println("Sort flawed");
			for (Integer i: nums) {
				System.out.print(i + " ");
			}
			System.out.println();
		}
	}
}
