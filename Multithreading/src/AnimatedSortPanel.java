
import java.awt.*;
import javax.swing.*;

@SuppressWarnings("serial")
public class AnimatedSortPanel extends JPanel {
	private int[] array;
	
	public AnimatedSortPanel() {
		array = null;  // start with no array
		setBackground(Color.white);
	}
	
	public void reset(int[] array) {  //gives the JPanel a new array to animate
		this.array = array;
	}
	
	public void update(Update update) { //This is the method that updates the JPanel's animation/painting
		update.update(array);
	}
	
	protected void paintComponent(Graphics g) {  //Here is where you can tell what the rows and columns of the animation represent
		super.paintComponent(g);
		if (array != null) {
			g.setColor(Color.red);
			int xSize = Math.max(1, getWidth() / array.length);
			int ySize = Math.max(1, getHeight() / array.length);
			for (int i = 0; i < array.length; ++i) {
				int x = i * getWidth() / array.length;
				int y = array[i] * getHeight() / array.length;
				g.fillRect(x, y, xSize, ySize);
			}
		}
	}
}
