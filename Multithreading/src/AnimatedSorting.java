
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

//This is the primary class for the GUI application.  We will place a JPanel inside
// of a JFrame in which to paint the animation

@SuppressWarnings("serial")
public class AnimatedSorting extends JFrame {
	private int numNums;            //size of the array to sort
	private AnimatedSortPanel asp;  //JPanel to paint animation on
	private AnimationThread messenger;  //thread for the animation
	private JSlider slider;
	
	public AnimatedSorting() {
		setTitle("Animated Sorting");
		setSize(600,600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		getContentPane().setLayout(new BorderLayout()); //Border Layout is a pre-built component manager that
		                                                //allows for easy placement of the components
		
		asp = new AnimatedSortPanel();
		getContentPane().add(asp, BorderLayout.CENTER);
		
		JMenuBar bar = new JMenuBar();
		JMenu sorters = new JMenu("Sorters");
		addSorter("BubbleSorter", sorters, new BubbleSorter());
		addSorter("InsertionSorter", sorters, new InsertionSorter());
		addSorter("SelectionSorter", sorters, new SelectionSorter());
		addSorter("HeapSorter", sorters, new HeapSorter());
		addSorter("QuickSorter", sorters, new QuickSorter());
		//you need to add the Selection, Quick, and Heap Sorters to the JMenu HERE !!
		
		bar.add(sorters); //puts the list of sorters in the menu bar
		setJMenuBar(bar); //adds the menu bar to the JFrame
		
		messenger = new AnimationThread(asp, 10);  //creates the animation thread
		messenger.start();   //starts it
		
		slider = new JSlider(10, 200); //slider bar
		slider.addChangeListener(new ChangeNums()); //listener for the slider bar
		getContentPane().add(slider, BorderLayout.NORTH);
		numNums = slider.getValue();
		slider.setPaintLabels(true);
	}
	
	private class ChangeNums implements ChangeListener {

		@Override
		public void stateChanged(ChangeEvent e) {
			numNums = slider.getValue();
		}
		
	}
	
	private void addSorter(String name, JMenu menu, Sorter sorter) {
		JMenuItem item = new JMenuItem(name);
		item.addActionListener(new SortStarter(sorter));
		menu.add(item);
	}
	
	public static void main(String[] args) {
		new AnimatedSorting().setVisible(true);
	}
	
	private class SortStarter implements ActionListener {  //the Listener to "hear" when a sorting method is
		                                                   // chosen from the menu bar
		private Sorter sorter;
		
		public SortStarter(Sorter sorter) {
			this.sorter = sorter;
		}
		
		public void actionPerformed(ActionEvent e) {  //When the user selects the sorting method she wants
			                                          // we must set up and initialize the context.
			//set up and load the array
			int[] nums = new int[numNums];
			for (int i = 0; i < nums.length; ++i) {
				nums[i] = i;
			}
			
			//scrambles the arrays values
			Sorter.scramble(nums);
			
			//creates a copy of the scrambled array so that the animation thread can use it
			int[] animNums = new int[nums.length];
			for (int i = 0; i < nums.length; ++i) {
				animNums[i] = nums[i];
			}
			
			
			asp.reset(animNums);  //give the animation thread this new array
			messenger.clearQueue(); //clear out the message queue in case the animation thread is 
			                        // still animating a previous sorting process
			sorter.setMessenger(messenger); //give the sorter this new message queue
			//Everything is set up, fire it off!
			new SorterThread(sorter, nums).start(); // creates a "sorting" thread to do the actual sorting of the array
		}
	}
}
