
public class InsertionSorter extends Sorter {

	public InsertionSorter() {
		super();
	}

	@Override
	public void sort(int[] array, int minIndex, int maxIndex) {
		for (int i = minIndex + 1; i <= maxIndex; ++i) {
			int target = array[i];
			int j = i;
			while (j > minIndex && target < array[j-1]) {
				assign(array, j, array[j-1]);
				j -= 1;
			}
			assign(array, j, target);
		}
	}
	
	public static void main(String[] args) {
		//Although we cannot make an instance of class Sorter we can ask this abstract
		// class to run its test method
		Sorter.test(new InsertionSorter());
	}
}
