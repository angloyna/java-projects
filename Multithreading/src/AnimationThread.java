
import java.util.concurrent.*;

public class AnimationThread extends Thread {
	//add your private variables here
	private LinkedBlockingQueue<Update> queue;
	private AnimatedSortPanel sortPanel;
	private long sleeptime;
	
	public AnimationThread(AnimatedSortPanel asp, long interval) {
        //initialize your private variables
		queue = new LinkedBlockingQueue<Update>();
		this.clearQueue();
		sortPanel = asp;
		this.sleeptime = interval;
	}
	
	public void sendUpdate(Update update) {
        /* Your code here */
        /* Place the update in a blocking queue */
		try {
			queue.put(update);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void clearQueue() {
		/*Clear the contents of the queue */
		queue.clear();
	}

	public void run() {
        /* Your code here */
        /* Check the blocking queue for updates.  Then apply them.  */
		try {
			//put all of your code for second comment and then put to sleep
			while (true) {
				sortPanel.update(queue.take());
				sortPanel.repaint();
				sleep(sleeptime);
			}
		}

		catch (InterruptedException e) {
			System.out.println( "Interrupted exception occurred!");
		}	
	}
}
