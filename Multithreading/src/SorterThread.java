
//Thread to actually sort the array
public class SorterThread extends Thread {
	private Sorter sorter;
	private int[] nums;
	
	public SorterThread(Sorter sorter, int[] array) {
		this.sorter = sorter;
		this.nums = array;
	}
	
	@Override
	public void run() {
		sorter.sort(nums);
		//This thread writes to the console window when it has completed the sorting of the array
		if (Sorter.isSorted(nums)) {
			System.out.println("Sort successful");
		} else {
			System.out.println("Sort flawed");
		}
	}
}
