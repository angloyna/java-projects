public class HeapSorter extends Sorter {

	
	public HeapSorter() {
		super();

	}

	@Override
	public void sort(int[] array, int minIndex, int maxIndex) {
		//Complete this method
		heapify(array, minIndex, maxIndex);
		int max = maxIndex;
		while (max >= minIndex) {
			swap(array, minIndex, max);
			max--;
			down_heap(array, minIndex, max, minIndex);
		}
	}
	
	
	private void down_heap(int[] array, int minIndex, int maxIndex, int position ) {
		if( position <= maxIndex ) {
			int child = 2*position + 1;
			if( (child < maxIndex) && array[child] < array[child+1] ) {
				child++;
			}
			if( child <= maxIndex &&  array[child] > array[position] ) {
				swap(array, child, position );
				down_heap(array, minIndex, maxIndex, child );
			}			
		}
	}
	
	private void heapify(int[] array, int minIndex, int maxIndex) {
		for (int pos = ((maxIndex - minIndex) / 2); pos >= minIndex; pos--) {
		    down_heap(array, minIndex, maxIndex, pos);
		}

	}
	
	public static void main(String[] args) {
		Sorter.test(new HeapSorter());
	}
}