package ex2;


import edu.hendrix.grambler.Grammar;
import edu.hendrix.grambler.ParseException;
import edu.hendrix.grambler.Tree;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import java.util.HashSet;
import java.util.Set;

import edu.hendrix.grambler.Grammar;
import edu.hendrix.grambler.ParseException;
import edu.hendrix.grambler.Tree;


public class Evaluator {
	private Grammar g;
	private HashMap<String, String> symbols;
	private Set<String> numbers;
	
	public Evaluator() {
		g = new ArithmeticGrammar();
		symbols = new HashMap<String, String>();
	}
	
	public String eval(String input) throws ParseException {
		return evalTree(g.parse(input));
	}
	
	private String evalTree(Tree t) {
		if (t.isNamed("symbol")) {
			return symbols.get(t.toString());
		} else if (t.isNamed("number")) {
			return t.toString();
		} else if (t.getNumChildren() == 1) {
			return evalTree(t.getChild(0));
		} else if (t.isNamed("lines")) {
			return evalTree(t.getNamedChild("lines")) + '\n' + evalTree(t.getNamedChild("line"));
		} else if (t.isNamed("line")) {
			return evalTree(t.getChild(0));
			
			
		} else if (t.isNamed("assign")) {
			String value = evalTree(t.getNamedChild("subExpr"));
			symbols.put(t.getNamedChild("symbol").toString(), value);
			return value;
			
		} else if (t.isNamed("subExpr")) {
			
			
			String left = evalTree(t.getNamedChild("subExpr"));
			String right = evalTree(t.getNamedChild("addExpr"));
			return String.valueOf((Integer.parseInt(left) - Integer.parseInt(right)));
			
			
		} else if (t.isNamed("addExpr")) {
			
			
			String left = evalTree(t.getNamedChild("addExpr"));
			String right = evalTree(t.getNamedChild("DivExpr"));
			return String.valueOf(Integer.parseInt(left) + Integer.parseInt(right));
			
			
		} else if (t.isNamed("DivExpr")) {
			
			
			String left = evalTree(t.getNamedChild("DivExpr"));
			String right = evalTree(t.getNamedChild("MultExpr"));
			return String.valueOf(Integer.parseInt(left) / Integer.parseInt(right));
			
			
		} else if (t.isNamed("MultExpr")) {
			
			
			String left = evalTree(t.getNamedChild("MultExpr"));
			String right = evalTree(t.getNamedChild("ParenExpr"));
			return String.valueOf(Integer.parseInt(left) * Integer.parseInt(right));
			
			
		}	else if (t.isNamed("ParenExpr")) {
			
			
			return evalTree(t.getNamedChild("subExpr"));
			
			
		} else {
			throw new IllegalArgumentException("Tree has unexpected name: " + t.getName());
		}
	}
}
