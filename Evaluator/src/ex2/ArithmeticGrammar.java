package ex2;

public class ArithmeticGrammar extends edu.hendrix.grambler.Grammar {
    public ArithmeticGrammar() {
        super();
        addProduction("lines", new String[]{"lines", "cr", "line"}, new String[]{"line"});
        addProduction("cr", new String[]{"'\r\n'"}, new String[]{"'\n'"});
        addProduction("line", new String[]{"assign"}, new String[]{"subExpr"});
        addProduction("assign", new String[]{"symbol", "\"\\s*\"", "'='", "\"\\s*\"", "subExpr"});
        addProduction("subExpr", new String[]{"subExpr", "\"\\s*\"", "'-'", "\"\\s*\"", "addExpr"}, new String[]{"addExpr"});
        addProduction("addExpr", new String[]{"addExpr", "\"\\s*\"", "'+'", "\"\\s*\"", "DivExpr"}, new String[]{"DivExpr"});
        addProduction("DivExpr", new String[]{"DivExpr", "\"\\s*\"", "'/'", "\"\\s*\"", "MultExpr"}, new String[]{"MultExpr"});
        addProduction("MultExpr", new String[]{"MultExpr", "\"\\s*\"", "'*'", "\"\\s*\"", "ParenExpr"}, new String[]{"ParenExpr"});
        addProduction("ParenExpr", new String[]{"'('", "\"\\s*\"", "subExpr", "\"\\s*\"", "')'"}, new String[]{"symbol"}, new String[]{"number"});
        addProduction("symbol", new String[]{"\"[A-Za-z]+\""});
        addProduction("number", new String[]{"\"[0-9]+\""});
    }
}

