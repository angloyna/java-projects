import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class SelectListener implements ActionListener {
		ArrayList<Task> Selected;
		Task curTask;
		
		public SelectListener(Task task, ArrayList<Task> SelectedList) {
			Selected = SelectedList;
			curTask = task;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if (Selected.contains(curTask)) {
				Selected.remove(curTask);
			} else
				Selected.add(curTask);
		}
	}