import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;

public class ClearAllSelected implements ActionListener {
		ArrayList<Task> SelectedTaskstoRemove;
		ArrayList<Task> AllTasks;
		JPanel curTaskPanel;
		
		public ClearAllSelected(ArrayList<Task> array, JPanel curTaskPanel, ArrayList<Task> AllTasks) {
			SelectedTaskstoRemove = array;
			this.AllTasks = AllTasks;
			this.curTaskPanel = curTaskPanel;
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {
			for (int i = 0; i < SelectedTaskstoRemove.size(); i++) {
				removeTaskFromToDoList(curTaskPanel, SelectedTaskstoRemove.get(i));
			}
			refreshToDoList();
			SelectedTaskstoRemove.clear();
			
		}
		private void removeTaskFromToDoList(JPanel curTaskPanel, Task task) {
			curTaskPanel.remove(task.getCheckButton());
			curTaskPanel.remove(task.getTextArea());
			AllTasks.remove(AllTasks.indexOf(task));
			
		}
		
		private void refreshToDoList() {
			curTaskPanel.revalidate();
			curTaskPanel.repaint();
		}
		
	}