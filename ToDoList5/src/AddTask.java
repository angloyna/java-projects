import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class AddTask implements ActionListener {
		private Task tempNewTask;
		private ArrayList<Task> AllTasks;
		private JScrollPane curTaskScrollPane;
		private JPanel curTaskPanel;
		private JTextField newTaskField;
		private ArrayList<Task> SelectedTasks;
		private Task newtask;
		
		
		public AddTask(JScrollPane curTaskScrollPane, JPanel panel, ArrayList<Task> AllTasks, JTextField newTaskField, ArrayList<Task> SelectedTasks) {
			this.AllTasks = AllTasks;
			this.curTaskScrollPane = curTaskScrollPane;
			this.curTaskPanel = panel;
			this.newTaskField = newTaskField;
			this.SelectedTasks = SelectedTasks;
			
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if (AllTasks.size() >= 17) {
				JOptionPane.showMessageDialog(curTaskScrollPane, "This to-do list only allows you to enter as many as 17 tasks at a time." + '\n' +
						"Please consider finishing the tasks already in progress and removing them from the to-do list.");
			}
			if (UserEnteredText() && AllTasks.size() < 17) { 	
					tempNewTask = createNewTaskItem(curTaskPanel, getUserInput(), SelectedTasks);
					newTaskField.setText("");
					AllTasks.add(tempNewTask);
					refreshToDoList();
			}
		}
		
		private void refreshToDoList() {
			curTaskScrollPane.revalidate();
			curTaskScrollPane.repaint();
		}
		
		private boolean UserEnteredText() {
			return !newTaskField.getText().trim().equals("");
		}
		
		private String getUserInput() {
			return newTaskField.getText().trim();
		}
		
		private Task createNewTaskItem(JPanel curTaskScrollPane, String tasktext, ArrayList<Task> selectedTasks) {
			newtask = new Task(curTaskScrollPane, tasktext);
			newtask.getCheckButton().addActionListener(new SelectListener(newtask, selectedTasks));
			return newtask;
		}

	}