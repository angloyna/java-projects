import java.awt.*;

import java.util.ArrayList;

import javax.swing.*;

@SuppressWarnings("serial")
public class ToDoList5 extends JFrame {

	private JTextField newTaskField;
	
	private JScrollPane curTaskScrollPane;
	
	private JLabel TaskFieldLabel;
	
	private JButton ClearAllSelected;
	private JButton saveButton;
	private JButton loadButton;
	private JButton clearbutton;
	private JButton AddTaskButton;
	
	private ArrayList<Task> AllTasks;
	private ArrayList<Task> SelectedTasks;
	
	private JPanel tempPanel;
	private JPanel tempTaskPanel;
	private JPanel newTaskPanel;
	private JPanel curTaskPanel;
	
	
	public ToDoList5() {
		initializeToDoList5(new Dimension(600,700));
		
		SelectedTasks = new ArrayList<Task>();
		AllTasks = new ArrayList<Task>();
		
		newTaskPanel = createNewTaskPanel();
		
		curTaskPanel = createCurTaskPanel();
		curTaskScrollPane = createCurTaskPane();
		
		ClearAllSelected = createClearAllButton();
		saveButton = createSaveButton(this);
		loadButton = createLoadButton(this);
		
		setUpTaskInputPanel(newTaskPanel);
		
		add(newTaskPanel);
		add(ClearAllSelected);
		add(saveButton);
		add(loadButton);
		add(curTaskScrollPane);
		
		curTaskScrollPane.getViewport().add(curTaskPanel);
		
	}
	
	private void initializeToDoList5(Dimension size) {
		setSize(size);
		setTitle("To-Do List");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new FlowLayout());
		setResizable(false);
	}
	
	private JScrollPane createCurTaskPane() {
		JScrollPane tempTaskPane = new JScrollPane();
		initializePanelSettings(tempTaskPane, new Dimension(600,595));
		return tempTaskPane;
	}

	private void initializePanelSettings(JScrollPane tempTaskPane, Dimension size) {
		tempTaskPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	    tempTaskPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		tempTaskPane.setOpaque(false);
		tempTaskPane.setPreferredSize(size);
		
	}

	private JButton createLoadButton(ToDoList5 todolist) {
		loadButton = new JButton("Load To-Do List");
		loadButton.addActionListener(new LoadFromFile(todolist, AllTasks, curTaskPanel, SelectedTasks));
		return loadButton;
	}

	private JButton createSaveButton(ToDoList5 todolist) {
		saveButton = new JButton("Save to File");
		saveButton.addActionListener(new SaveToFile(todolist, AllTasks));
		return saveButton;
	}

	private void setUpTaskInputPanel(JPanel newtaskpanel) {
		TaskFieldLabel = createNewTaskLabel();
		newTaskField = createNewTextField();
		AddTaskButton = createAddButton();
		newtaskpanel.add(TaskFieldLabel);
		newtaskpanel.add(newTaskField);
		newtaskpanel.add(AddTaskButton);
	}
	
	private JPanel createNewTaskPanel() {
		tempPanel = new JPanel();
		initializePanelSettings(tempPanel, new Dimension(400, 35));
		return tempPanel;
	}
	
	private void initializePanelSettings(JPanel panel, Dimension size) {
		panel.setLayout(new FlowLayout());
		panel.setOpaque(false);
		panel.setPreferredSize(size);
	}
	
	private JTextField createNewTextField() {
		return new JTextField(20);
	}
	
	private JLabel createNewTaskLabel() {
		return new JLabel("Add Task: ");
	}
	
	private JButton createAddButton() {
		AddTaskButton = new JButton("ADD");	
		AddTaskButton.addActionListener(new AddTask(curTaskScrollPane, curTaskPanel, AllTasks, newTaskField, SelectedTasks));
		AddButtonRespondstoEnter(AddTaskButton);
		return AddTaskButton;
	}
	
	private void AddButtonRespondstoEnter(JButton addbutton) {
		getRootPane().setDefaultButton(addbutton);
	}
	
	private JButton createClearAllButton() {
		clearbutton = new JButton("Clear Selected");
		clearbutton.addActionListener(new ClearAllSelected(SelectedTasks, curTaskPanel, AllTasks));
		return clearbutton;
	}

	private JPanel createCurTaskPanel() {
		tempTaskPanel = new JPanel();
		initializePanelSettings(tempTaskPanel, new Dimension(540,1500));
		return tempTaskPanel;
	}
	
	public JButton TestLoadButton(ToDoList5 todolist) {
		return createLoadButton(todolist);
	}
	
	public JButton TestSaveButton(ToDoList5 todolist) {
		return createSaveButton(todolist);
	}
	
	public JButton TestAddButton() {
		return createAddButton();
	}
	
	public JButton TestClearButton() {
		return createClearAllButton();
	}
	
	public void TestingTextArea(String s) {
		this.newTaskField.setText(s);
	}
	
	public ArrayList<Task> TestingAllTasksGetter() {
		return AllTasks;
	}

	public static void main(String[] args) {
		new ToDoList5().setVisible(true);
	}
	

}
