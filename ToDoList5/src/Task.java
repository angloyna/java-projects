
import java.awt.Font;


import javax.swing.*;


public class Task {
	private JCheckBox checkButton;
	private JTextArea TaskTextArea;
	private Font myFont;

public Task(JPanel curTaskPanel, String task) {
	myFont = new Font("Courier", Font.BOLD, 18);
	createNewCheckBox(curTaskPanel);
	createTextArea(curTaskPanel, task);
	
}
private void createNewCheckBox(JPanel curTaskPanel) {
	checkButton = new JCheckBox();
    checkButton.setOpaque(false);
    curTaskPanel.add(checkButton);
}

private void createTextArea(JPanel curTaskPanel, String task) {
	TaskTextArea = new JTextArea(task);
	initalizeTextAreaSettings();
	curTaskPanel.add(TaskTextArea);
}

private void initalizeTextAreaSettings() {
	TaskTextArea.setColumns(50);
	TaskTextArea.setLineWrap(true);
	TaskTextArea.setFont(myFont);
	TaskTextArea.setOpaque(false);
}

public JCheckBox getCheckButton() {
	return checkButton;
}

public JTextArea getTextArea() {
	return TaskTextArea;
}

public String getTask() {
	return TaskTextArea.getText();
}



}