import static org.junit.Assert.*;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

import org.junit.Before;
import org.junit.Test;


public class ToDoList5Test {
	private Task TestTask;
	private JPanel TestTaskPanel;
	private ToDoList5 todolist;
	private JButton TestButton;
	private ArrayList<Task> TestSelected;
	
	@Before
	public void initialize() {
		todolist = new ToDoList5();
		TestTaskPanel = new JPanel();
		TestSelected = new ArrayList<Task>();
	}

	@Test
	public void testTaskConstructor() {
		TestTask = new Task(TestTaskPanel, "Clean bedroom.");
		assertEquals(TestTask.getTextArea().getText(), "Clean bedroom.");
	}
	
	@Test
	public void testCreateLoadButtonAL() {
		TestButton = todolist.TestLoadButton(todolist);
		assertEquals("Load To-Do List", TestButton.getActionCommand());
		
	}
	
	@Test
	public void testSelectListenerALSelect() {
		TestTask = new Task(TestTaskPanel, "Wash the Dog.");
		TestTask.getCheckButton().addActionListener(new SelectListener(TestTask, TestSelected));
		TestTask.getCheckButton().doClick();
		assertEquals(TestSelected.get(0), TestTask);
	}
	
	@Test
	public void testSelectListenerALUnSelect() {
		TestTask = new Task(TestTaskPanel, "Assassinate the Prime Minister of Malaysia.");
		TestTask.getCheckButton().addActionListener(new SelectListener(TestTask, TestSelected));
		TestTask.getCheckButton().doClick();
		TestTask.getCheckButton().doClick();
		assertEquals(0, TestSelected.size());
	}
	
	@Test
	public void testCreateSaveButton() {
		TestButton = todolist.TestSaveButton(todolist);
		assertEquals("Save to File", TestButton.getActionCommand());
	}
	
	@Test
	public void testCreateAddButton() {
		TestButton = todolist.TestAddButton();
		assertEquals("ADD", TestButton.getActionCommand());
	}
	
	@Test
	public void testAddTaskAL() {
		TestButton = todolist.TestAddButton();
		todolist.TestingTextArea("This should get pushed into the AllTasks Array");
		TestButton.doClick();
		assertEquals("This should get pushed into the AllTasks Array", todolist.TestingAllTasksGetter().get(0).getTask());
	}
	
	@Test
	public void  testCreateClearButton() {
		TestButton = todolist.TestClearButton();
		assertEquals("Clear Selected", TestButton.getActionCommand());
	}
	
	@Test
	public void testRemoveSelectedAL() {
		TestButton = todolist.TestAddButton();
		todolist.TestingTextArea("This should get pushed into the AllTasks Array");
		TestButton.doClick();
		todolist.TestingAllTasksGetter().get(0).getCheckButton().doClick();
		TestButton = todolist.TestClearButton();
		TestButton.doClick();
		assertEquals(0,todolist.TestingAllTasksGetter().size());
	}
	
	@Test
	public void testRemoveSelectedMultiAL() {
		TestButton = todolist.TestAddButton();
		todolist.TestingTextArea("This should get pushed into the AllTasks Array");
		TestButton.doClick();
		TestButton = todolist.TestAddButton();
		todolist.TestingTextArea("This should also get pushed into the AllTasks Array");
		TestButton.doClick();
		for (Task i : todolist.TestingAllTasksGetter()) {
			i.getCheckButton().doClick();
		}
		TestButton = todolist.TestClearButton();
		TestButton.doClick();
		assertEquals(0,todolist.TestingAllTasksGetter().size());
	}


}
