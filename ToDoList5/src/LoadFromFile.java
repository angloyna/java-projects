import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class LoadFromFile implements ActionListener {
		private ToDoList5 todolist;
		private ArrayList<Task> AllTasks;
		private JPanel curTaskPanel;
		private ArrayList<Task> SelectedTasks;
		private Task newtask;
		
		public LoadFromFile(ToDoList5 todoList, ArrayList<Task> AllTasks, JPanel curTaskPanel, ArrayList<Task> SelectedTasks) {
			this.todolist = todoList;
			this.AllTasks = AllTasks;
			this.curTaskPanel = curTaskPanel;
			this.SelectedTasks = SelectedTasks;
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {
			final JFileChooser fc = new JFileChooser();
			int returnVal = fc.showOpenDialog(todolist);
			

	        if (returnVal == JFileChooser.APPROVE_OPTION) {
	            File file = fc.getSelectedFile();
	            BufferedReader br = null;
	            
	    		try {
	     
	    			String sCurrentLine;
	     
	    			br = new BufferedReader(new FileReader(file));
	    			curTaskPanel.removeAll();
	    			AllTasks.clear();
	    			SelectedTasks.clear();
	     
	    			while ((sCurrentLine = br.readLine()) != null) {
	    				if (AllTasks.size() >= 17) {
	    					JOptionPane.showMessageDialog(curTaskPanel, "This to-do list only allows you to enter as many as 17 tasks at a time." + '\n' +
	    							"Please consider finishing the tasks already in progress and removing them from the to-do list.");
	    				}
	    				if (AllTasks.size() < 17) { 					
	    						Task tempLoadTask = createNewTaskItem(curTaskPanel, sCurrentLine, SelectedTasks);
	    						AllTasks.add(tempLoadTask);
	    						refreshToDoList();
	    				}
	    			}
	     
	    		} catch (IOException e) {
	    			e.printStackTrace();
	    		} finally {
	    			try {
	    				if (br != null)br.close();
	    			} catch (IOException ex) {
	    				ex.printStackTrace();
	    			}
	    		}
	            
	        } else {
	            JOptionPane.showMessageDialog(todolist,
	            		"Open command cancelled by user.");
	        }
			
		}
		
		private void refreshToDoList() {
			curTaskPanel.revalidate();
			curTaskPanel.repaint();
		}
		
		private Task createNewTaskItem(JPanel curTaskPanel, String tasktext, ArrayList<Task> selectedTasks) {
			newtask = new Task(curTaskPanel, tasktext);
			newtask.getCheckButton().addActionListener(new SelectListener(newtask, selectedTasks));
			return newtask;
		}
		
	}