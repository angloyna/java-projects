import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

	public class SaveToFile implements ActionListener {
		ToDoList5 todoList;
		ArrayList<Task> AllTasks;
		
		public SaveToFile(ToDoList5 todoList, ArrayList<Task> TaskList) {
			this.todoList = todoList;
			this.AllTasks = TaskList;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			String s = (String)JOptionPane.showInputDialog(
                    todoList,
                    "Enter New File Name:\n",
                    "Save Task List to File",
                    JOptionPane.QUESTION_MESSAGE);
				try {
		 
					File file = new File(s + ".txt");
		 
					if (!file.exists()) {
						file.createNewFile();
					}
		 
					FileWriter fw = new FileWriter(file.getAbsoluteFile());
					BufferedWriter bw = new BufferedWriter(fw);
					for (int k = 0; k < AllTasks.size(); k++) {
						bw.write(AllTasks.get(k).getTextArea().getText());
						bw.newLine();
					}
					bw.close();
		 
				} catch (IOException e) {
					e.printStackTrace();
				}
			
		}
	}