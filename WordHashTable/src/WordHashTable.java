import java.util.ArrayList;
import java.util.Iterator;


public class WordHashTable {
	private ArrayList<ArrayList<WordFreq>> table;  //an ArrayList of ArrayList references
	private double loadFactor;
	private int numEntries;
	
	public WordHashTable( int num_buckets, double ratio ) {
		numEntries = 0;
		loadFactor = ratio;
		table = new ArrayList<ArrayList<WordFreq>>(num_buckets);//This instantiates the list of buckets
		for( int idx = 0; idx < num_buckets; idx++ )
			table.add( new ArrayList<WordFreq>() ); //this instantiates the actual buckets themselves !!
	}
	
	public void add( String s ) {
		if( (float)numEntries / table.size() >= loadFactor )
			resize();
		
		int index = bucketVal( s.toLowerCase(), table.size() );
		ArrayList<WordFreq> bucket = table.get( index );
		int bucket_idx = bucketListIndex( s, index );
		if( bucket_idx < 0 ) {
			bucket.add( new WordFreq( s.toLowerCase() ) );
			numEntries++;
		}
		else
			bucket.get( bucket_idx ).increment();
	}
	
	public boolean contains( String s ) {
		int index = bucketVal( s.toLowerCase(), table.size() );
		return bucketListIndex( s, index ) > -1;
	}
	
	public int frequency( String s ) {
		if( contains( s ) ) { 
			int index = bucketVal( s.toLowerCase(), table.size() );
			return table.get(index).get(bucketListIndex( s, index )).frequency();
		}
		return 0;

	}

	private int bucketVal( String s, int numBuckets ) {
		return Math.abs( s.hashCode() ) % numBuckets;
	}
	
	private void resize() {
		ArrayList<ArrayList<WordFreq>> new_table = new ArrayList<ArrayList<WordFreq>>(2*table.size() + 1);
		for( int idx = 0; idx < 2*table.size() + 1; idx++ )
			new_table.add( new ArrayList<WordFreq>() );
		//will need TWO iterators, one to go through the array of buckets and anther to
		// actually go through the given bucket itself!!
		Iterator<ArrayList<WordFreq>> itr = table.iterator();
		while( itr.hasNext() ) {
			ArrayList<WordFreq> bucket_list = itr.next();
			Iterator<WordFreq> b_itr = bucket_list.iterator();
			while( b_itr.hasNext() ) {
				WordFreq entry = b_itr.next();
				int new_bucket_index = bucketVal( entry.word(), new_table.size() );
				new_table.get( new_bucket_index ).add( entry );
			}
		}
		
		table = new_table;				
	}
	
	//returns the index location of the string in bucket bucket_idx
	private int bucketListIndex( String s, int bucket_idx ) {
		ArrayList<WordFreq> bucket = table.get( bucket_idx );
		int index = -1;
		for( int idx = 0; idx < bucket.size(); idx++ ) {
			if( bucket.get(idx).word().equalsIgnoreCase( s ) ) {
				index = idx;
				break;
			}
		}
		return index;		
	}
	
	
	
	
}
