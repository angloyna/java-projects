import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import javax.swing.JOptionPane;

public class Main {

	public static void main(String[] args) throws FileNotFoundException {
		WordHashTable wh_table = new WordHashTable( 11, 0.75 );
		
		File f = new File(args[0]);
		Scanner input = new Scanner(f);  //A Scanner is an object that gives easy opening,traversal through a file, and closing
		ArrayList<String> list = new ArrayList<String>();
		while( input.hasNext() )  //whitespace delimited (space, tab, newline, etc.)
			list.add(input.next().replaceAll("[^a-zA-Z0-9']", "") ); //only keep alphanumeric and apostrophe characters
																	 //The '^' means "not"
		input.close();//through reading from the file
		Iterator<String> iter = list.iterator();
		while (iter.hasNext()) {
			wh_table.add( iter.next() );
		}
			
		
		boolean keep_searching = true;
		String input_string;
		int numInstances;
		input_string = JOptionPane.showInputDialog( "Do you wish to search for a word? (yes -> y , no -> n)");
		if( input_string.equals( "n" ) )
			keep_searching = false;
		
		while( keep_searching ) {
			input_string = JOptionPane.showInputDialog( "What word do you wish to search for?" ).replaceAll("[^a-zA-Z0-9']", "");
			numInstances = wh_table.frequency( input_string );
			String response = "The word " + input_string + " appears " + numInstances + " times.\n Do you wish to search for another word? (yes -> y, no ->n)";
			input_string = JOptionPane.showInputDialog( response );
			if( input_string.equals( "n" ))
				keep_searching = false;
		}
		

	}

}
