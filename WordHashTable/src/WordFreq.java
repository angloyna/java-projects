
public class WordFreq {
	private String word;
	private int freq;
	
	public WordFreq( String s ) {
		word = s;
		freq = 1;
	}
	
	public String word() { return word; }
	public int frequency() { return freq; }
	public void increment() { freq++; }
	public void decrement() { freq--; }
}
