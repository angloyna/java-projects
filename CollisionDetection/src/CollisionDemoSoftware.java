import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import com.threed.jpct.*;
import com.threed.jpct.util.*;

public class CollisionDemoSoftware extends JFrame {

	private static final long serialVersionUID = 1L;

	private static final float DAMPING = 0.1f;

	private static final float SPEED = 1f;

	private static final float MAXSPEED = 1f;

	private Graphics g = null;

	private KeyMapper keyMapper = null;

	private FrameBuffer fb = null;

	private World world = null;

	private Object3D plane = null;

	private Object3D ramp = null;

	private Object3D cube = null;

	private Object3D cube2 = null;

	private Object3D sphere = null;
	
	private Object3D ball = null;

	private boolean up = false;

	private boolean down = false;

	private boolean left = false;

	private boolean right = false;

	private boolean doloop = true;

	private SimpleVector moveRes = new SimpleVector(0, 0, 0);

	private SimpleVector ellipsoid = new SimpleVector(2, 2, 2);

	public CollisionDemoSoftware() {

		int numberOfProcs = Runtime.getRuntime().availableProcessors();

		Config.useMultipleThreads = numberOfProcs > 1;
		Config.useMultiThreadedBlitting = numberOfProcs > 1;
		Config.loadBalancingStrategy = 1;
		Config.maxNumberOfCores = numberOfProcs;
		Config.lightMul = 1;
		Config.mtDebug = true;

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setSize(1024, 768);
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
		g = getGraphics();
	}

	private void initStuff() {
		fb = new FrameBuffer(1024, 768, FrameBuffer.SAMPLINGMODE_NORMAL);
		world = new World();
		fb.enableRenderer(IRenderer.RENDERER_SOFTWARE);
		keyMapper = new KeyMapper(this);

		plane = Primitives.getPlane(20, 10);
		plane.rotateX((float) Math.PI / 2f);
		
		
		Object3D rightWall = Primitives.getPlane(20,10);
		rightWall.rotateY((float) Math.PI / 2f);
		rightWall.translate(new SimpleVector(100,-100,0));
		
		Object3D leftWall = Primitives.getPlane(20,10);
		leftWall.rotateY((float) Math.PI / 2f);
		leftWall.translate(new SimpleVector(-100, -100, 0));
		
		Object3D farWall = Primitives.getPlane(20, 10);
		farWall.rotateZ((float) Math.PI / 2f);
		farWall.translate(new SimpleVector(0,-100,100));
		
		Object3D closeWall = Primitives.getPlane(20, 10);
		closeWall.rotateZ((float) Math.PI / 2f);
		closeWall.translate(new SimpleVector(0,-100,-100));
	

		ramp = Primitives.getCube(20);
		ramp.rotateX((float)  Math.PI / 2f);

		sphere = Primitives.getSphere(30);
		sphere.translate(-50, 10, 50);

		cube2 = Primitives.getCube(20);
		cube2.translate(60, -20, 60);

		//cube = Primitives.getCube(2);
		//cube.translate(-50, -10, -50);
		
		ball = Primitives.getSphere(10);
		ball.translate(-50, -10, -50);

		plane.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS);
		rightWall.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS);
		farWall.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS);
		leftWall.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS);
		closeWall.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS);
		//ramp.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS);
		//sphere.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS);
		//cube2.setCollisionMode(Object3D.COLLISION_CHECK_OTHERS);
		//cube.setCollisionMode(Object3D.COLLISION_CHECK_SELF);
		ball.setCollisionMode(Object3D.COLLISION_CHECK_SELF);
		

		world.addObject(plane);
		world.addObject(rightWall);
		//world.addObject(ramp);
		//world.addObject(cube);
		//world.addObject(sphere);
		world.addObject(ball);
		world.addObject(farWall);
		world.addObject(leftWall);
		//world.addObject(closeWall);
		Light light = new Light(world);
		light.setPosition(new SimpleVector(0, -80, 0));
		light.setIntensity(140, 120, 120);
		light.setAttenuation(-1);

		world.setAmbientLight(20, 20, 20);

		world.buildAllObjects();
		
		
	}

	private void move() {
		KeyState ks = null;
		while ((ks = keyMapper.poll()) != KeyState.NONE) {
			if (ks.getKeyCode() == KeyEvent.VK_UP) {
				up = ks.getState();
			}
			if (ks.getKeyCode() == KeyEvent.VK_DOWN) {
				down = ks.getState();
			}
			if (ks.getKeyCode() == KeyEvent.VK_LEFT) {
				left = ks.getState();
			}
			if (ks.getKeyCode() == KeyEvent.VK_RIGHT) {
				right = ks.getState();
			}

			if (ks.getKeyCode() == KeyEvent.VK_ESCAPE) {
				doloop = false;
			}
		}

		// move the cube
		if (up) {
			SimpleVector t = ball.getYAxis();
			t.scalarMul(-SPEED);
			moveRes.add(t);
		}

		if (down) {
			SimpleVector t = ball.getYAxis();
			t.scalarMul(SPEED);
			moveRes.add(t);
		
		}

		if (left) {
			SimpleVector t = ball.getXAxis();
			t.scalarMul(-SPEED);
			moveRes.add(t);
			//ball.rotateY((float) Math.toRadians(-1));
		}

		if (right) {
			SimpleVector t = ball.getXAxis();
			t.scalarMul(SPEED);
			moveRes.add(t);
			//ball.rotateY((float) Math.toRadians(1));
		}

		// avoid high speeds
		if (moveRes.length() > MAXSPEED) {
			moveRes.makeEqualLength(new SimpleVector(0, MAXSPEED, 0));
		}

		//cube.translate(0, -0.02f, 0);
		ball.translate(0, -0.02f, 0);

		moveRes = ball.checkForCollisionEllipsoid(moveRes, ellipsoid, 8);
		//cube.translate(moveRes);
		ball.translate(moveRes);

		// finally apply the gravity:
		SimpleVector t = new SimpleVector(0, 0, 0);
		//t = cube.checkForCollisionEllipsoid(t, ellipsoid, 1);
		SimpleVector p = ball.checkForCollisionEllipsoid(t, ellipsoid, 1);
		//cube.translate(t);
		ball.translate(p);

		// damping
		if (moveRes.length() > DAMPING) {
			moveRes.makeEqualLength(new SimpleVector(0, DAMPING, 0));
		} else {
			moveRes = new SimpleVector(0, 0, 0);
		}
	}

	private void doIt() throws Exception {

		Camera cam = world.getCamera();
		cam.moveCamera(Camera.CAMERA_MOVEOUT, 100);
		cam.moveCamera(Camera.CAMERA_MOVEUP, 100);
		cam.lookAt(ramp.getTransformedCenter());

		long start = System.currentTimeMillis();
		long fps = 0;

		while (doloop) {
			move();

			cam.setPositionToCenter(ball);
			cam.align(ball);
			cam.rotateCameraX((float) Math.toRadians(30));
			cam.rotateCameraY((float) Math.toRadians(30));
			cam.moveCamera(Camera.CAMERA_MOVEOUT, 500);

			fb.clear(Color.BLUE);
			world.renderScene(fb);
			world.draw(fb);

			fb.update();
			fb.display(g);
			fps++;
			if (System.currentTimeMillis() - start >= 1000) {
				start = System.currentTimeMillis();
				System.out.println(fps);
				fps = 0;
			}

		}
		fb.disableRenderer(IRenderer.RENDERER_SOFTWARE);
		System.exit(0);
	}

	public static void main(String[] args) throws Exception {
		CollisionDemoSoftware cd = new CollisionDemoSoftware();
		cd.initStuff();
		cd.doIt();
		
		
	}
}
