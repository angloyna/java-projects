

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class PixelPanel extends JPanel {
	private PixelArray pix;
	private Mode mode;
	
	public PixelPanel() {
		super();
		pix = new PixelArray(80, 40);
		mode = Mode.FLIP;
		addMouseListener(new Clicker());
	}
	
	public void setMode(Mode m) {
		mode = m;
	}
	
	public Mode getMode() {return mode;}
	
	@Override
	protected void paintComponent(Graphics g) {
		for (int x = 0; x < pix.getWidth(); ++x) {
			int xStart = xFromArray(x);
			int xEnd = xFromArray(x + 1);
			
			for (int y = 0; y < pix.getHeight(); ++y) {
				int yStart = yFromArray(y);
				int yEnd = yFromArray(y + 1);
				g.setColor(pix.on(x, y) ? Color.YELLOW : Color.BLUE);
				g.fillRect(xStart, yStart, xEnd - xStart, yEnd - yStart);
			}
		}
	}
	
	public int xFromArray(int x) {
		return x * getWidth() / pix.getWidth();
	}
	
	public int yFromArray(int y) {
		return y * getHeight() / pix.getHeight();
	}
	
	public int xFromGrid(int x) {
		return pix.getWidth() * x / getWidth();
	}
	
	public int yFromGrid(int y) {
		return pix.getHeight() * y / getHeight();
	}
	
	private class Clicker extends MouseAdapter {
		private int lastX, lastY;

		@Override
		public void mouseClicked(MouseEvent e) {
			int x = xFromGrid(e.getX());
			int y = yFromGrid(e.getY());
			
			if (e.getButton() == MouseEvent.BUTTON3 || e.isControlDown()) {
				pix.changeRegion(lastX, lastY, x, y, mode);
			} else if (e.getButton() == MouseEvent.BUTTON1) {
				pix.change(x, y, mode);
			} 
			
			lastX = x;
			lastY = y;
			
			repaint();
		}
	}
}
