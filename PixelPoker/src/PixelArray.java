

public class PixelArray {
	private int width, height;
	private boolean[][] pixels;
	
	public PixelArray(int width, int height) {
		this.width = width;
		this.height = height;
		
		pixels = new boolean[width][height];
	}
	
	public PixelArray(PixelArray other) {
		this(other.width, other.height);
		for (int x = 0; x < width; ++x) {
			for (int y = 0; y < height; ++y) {
				this.pixels[x][y] = other.pixels[x][y];
			}
		}
	}
	
	@Override
	public boolean equals(Object other) {
		if (other instanceof PixelArray) {
			PixelArray that = (PixelArray)other;
			if (this.width != that.width || this.height != that.height) {
				return false;
			}
			for (int x = 0; x < width; ++x) {
				for (int y = 0; y < height; ++y) {
					if (this.pixels[x][y] != that.pixels[x][y]) {
						return false;
					}
				}
			}
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				sb.append(on(x, y) ? 'X' : 'O');
			}
			sb.append('\n');
		}
		return sb.toString();
	}
	
	public boolean on(int x, int y) {
		return pixels[x][y];
	}
	
	public boolean off(int x, int y) {
		return !on(x, y);
	}
	
	public boolean inBounds(int x, int y) {
		return 0 <= x && x < width && 0 <= y && y < height;
	}
	
	public void change(int x, int y, Mode m) {
		pixels[x][y] = m.getValue(pixels[x][y]);
	}
	
	public void changeRegion(int xStart, int yStart, int xEnd, int yEnd, Mode m) {
		for (int x = xStart; x <= xEnd; ++x) {
			for (int y = yStart; y <= yEnd; ++y) {
				change(x, y, m);
			}
		}
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
}
