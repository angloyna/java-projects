import static org.junit.Assert.*;

import org.junit.Test;


public class PixelPokerTest {
	PixelArray testPixelArray = new PixelArray(5,10);
	PixelArray copyPixelArray = new PixelArray(testPixelArray);
	PixelPanel pixelPanel = new PixelPanel();
	
	@Test
	public void testPixelArray() {
		//These should all be separate test functions. Sorry.
		
		
		//Test copied arrayed to make sure unchanged
		assertTrue(copyPixelArray.off(2,1));
		assertTrue(!copyPixelArray.on(2, 1));
		
		//Test inBounds()
		assertTrue(testPixelArray.inBounds(4,5));
		assertTrue(!testPixelArray.inBounds(15,15));
		
		
		testPixelArray.change(2, 1, pixelPanel.getMode());
		pixelPanel.setMode(Mode.SET);
		
		//Test changeRegion()
		testPixelArray.changeRegion(0, 0, 3, 5, pixelPanel.getMode());
		
		for (int testx = 0; testx <= 3; testx++) {
			for (int testy = 0; testy <= 5; testy++) {
				assertTrue(testPixelArray.on(testx, testy));
			}
		}
		
		for (int testx = 4; testx < 5 ; testx++) {
			for (int testy = 6; testy < 10; testy++) {
				assertTrue(testPixelArray.off(testx, testy));
			}
		}
		
		//Test clear mode
		testPixelArray.changeRegion(0, 0, 3, 5, Mode.CLEAR);
		
		for (int testx = 0; testx <= 3; testx++) {
			for (int testy = 0; testy <= 5; testy++) {
				assertTrue(testPixelArray.off(testx, testy));
			}
		}
		
		for (int testx = 4; testx < 5 ; testx++) {
			for (int testy = 6; testy < 10; testy++) {
				assertTrue(testPixelArray.off(testx, testy));
			}
		}
		
		//Test Mode.FLIP UnSet to Set
		testPixelArray.changeRegion(0, 0, 3, 5, Mode.FLIP);
		
		for (int testx = 0; testx <= 3; testx++) {
			for (int testy = 0; testy <= 5; testy++) {
				assertTrue(testPixelArray.on(testx, testy));
			}
		}
		
		for (int testx = 4; testx < 5 ; testx++) {
			for (int testy = 6; testy < 10; testy++) {
				assertTrue(testPixelArray.off(testx, testy));
			}
		}
		
		//Test Mode.FLIP Set to UnSet
		testPixelArray.changeRegion(0, 0, 3, 5, Mode.FLIP);
		
		for (int testx = 0; testx <= 3; testx++) {
			for (int testy = 0; testy <= 5; testy++) {
				assertTrue(testPixelArray.off(testx, testy));
			}
		}
		
		for (int testx = 4; testx < 5 ; testx++) {
			for (int testy = 6; testy < 10; testy++) {
				assertTrue(testPixelArray.off(testx, testy));
			}
		}
		
		//Testing Mode.advance().
		pixelPanel.setMode(Mode.FLIP);
		pixelPanel.setMode(pixelPanel.getMode().advance());
		System.out.println(pixelPanel.getMode());
		assertEquals(Mode.CLEAR, pixelPanel.getMode());
		pixelPanel.setMode(pixelPanel.getMode().advance());
		System.out.println(pixelPanel.getMode());
		assertEquals(Mode.SET, pixelPanel.getMode());
		pixelPanel.setMode(pixelPanel.getMode().advance());
		System.out.println(pixelPanel.getMode());
		assertEquals(Mode.FLIP, pixelPanel.getMode());
		
	}
	
	
	
	public void getHeight() {
		assertEquals(10, testPixelArray.getHeight());
	}
	
	public void getWidth() {
		assertEquals(5, testPixelArray.getWidth());
	}
	
	public void testCopyPixelArrayConstructor() {
		assertTrue(copyPixelArray.equals(testPixelArray));
	}
	
	public void TestgetMode() {
		assertEquals(Mode.FLIP, pixelPanel.getMode());
	}
	
	public void TestsetMode() {
		pixelPanel.setMode(Mode.SET);
		assertEquals(Mode.SET, pixelPanel.getMode());
		pixelPanel.setMode(Mode.FLIP);
	}
	
	public void TestChange() {
		testPixelArray.change(2, 1, pixelPanel.getMode());
		assertTrue(testPixelArray.on(2, 1));
		
		assertTrue(!testPixelArray.off(2, 1));
	}

}
