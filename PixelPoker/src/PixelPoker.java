
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class PixelPoker extends JFrame {
	private JButton swap;
	private JLabel current;
	private PixelPanel pix;
	
	public PixelPoker() {
		setTitle("Pixel Poker");
		setSize(400, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		setLayout(new BorderLayout());
		pix = new PixelPanel();
		add(pix, BorderLayout.CENTER);
		
		current = new JLabel("Mode: " + pix.getMode().toString());
		
		swap = new JButton(pix.getMode().advance().toString());
		swap.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				pix.setMode(pix.getMode().advance());
				current.setText("Mode: " + pix.getMode().toString());
				swap.setText(pix.getMode().advance().toString());
			}});
		
		JPanel top = new JPanel();
		top.setLayout(new FlowLayout());
		top.add(current);
		top.add(swap);
		add(top, BorderLayout.NORTH);
	}
	
	public static void main(String[] args) {
		new PixelPoker().setVisible(true);
	}
}
