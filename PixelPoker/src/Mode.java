

public enum Mode {
	
	FLIP {
		@Override
		public boolean getValue(boolean current) {return !current;}
		public String toString() {return "Flip";}
	}, 
	CLEAR {
		@Override
		public boolean getValue(boolean current) {return false;}
		public String toString() {return "Clear";}
	},
	SET {
		@Override
		public boolean getValue(boolean current) {return true;}
		public String toString() {return "Set";}
	};
	
	abstract public boolean getValue(boolean current);
	
	public Mode advance() {
		int next;
		if (this.ordinal() < 2) {
			next = this.ordinal() + 1;
		}
		else {
			next = 0;
		}
		return Mode.values()[next];
	}
}
