package mush;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DbInterface {
	Connection conn;
	ResultSet rows;
	
	public DbInterface() {
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e1) {
			System.err.println("SqliteJDBC library not found. "
					+ "Perhaps you need to set the build path?");
			System.exit(-1);
		}
			
		try {
			conn = DriverManager.getConnection("jdbc:sqlite:mush.db");
			
				
	
		} catch (SQLException e) {
			System.err.println("Could not connect to DBMS. Maybe DBMS isn't running?");
			System.exit(-1);
		}
	}

	public void close() {
		try {
			conn.close();
		} catch (SQLException e) {
			System.err.println("Error closing connection");
		}
	}

	public boolean userExists(String userId) throws SQLException {
		PreparedStatement query = conn.prepareStatement(
                "SELECT userid FROM users WHERE userid = ?");
		query.setString(1, userId);
        rows = query.executeQuery();
        boolean found = false;
        while (rows.next()) {
            found = true;
        }
        if (!found) {
            return false;
        }
		return found;
	}

	public boolean createUser(String userId) {
		PreparedStatement query;
		String user = userId;
		try {
			PreparedStatement stmt = conn.prepareStatement("SELECT roomid FROM rooms WHERE roomid = ?");
			stmt.setInt(1, 0);
			ResultSet rows = stmt.executeQuery();
			if ((!rows.next())) {
				System.out.println("Got Here");
				stmt = conn.prepareStatement("INSERT INTO rooms(roomid, creator, description) VALUES (?,?,?)");
				int room = 1;
				stmt.setInt(1,room);
				stmt.setString(2, user);
				stmt.setString(3, "This is the lobby");
				stmt.executeUpdate();
			}
			query = conn.prepareStatement(
			        "INSERT INTO users(userid, userloc) VALUES(?, ?)");
			
			query.setString(1, user);
			query.setInt(2, 1);
	        int worked = query.executeUpdate();
	        if (worked != -1) {
	        	return true;
	        } else {
	        	return false;
	        }
	        
		} catch (SQLException e) {
			return false;
		}
	}

	public Room getCurrentRoom(String userId) {
		PreparedStatement query;
		ArrayList<String> usersList = new ArrayList<String>();
		ArrayList<String> actionsList = new ArrayList<String>();
		Integer roomid = null;
		String description = null;
		String creator = null;
		try {
			query = conn.prepareStatement("SELECT userloc FROM users WHERE userid = ?");
			query.setString(1, userId);
			rows = query.executeQuery();
			while (rows.next()) {
				roomid = rows.getInt(1);
			}
			query = conn.prepareStatement(
			        "SELECT roomid, description, creator FROM rooms WHERE roomid = ?");
			query.setInt(1, roomid);
	        rows = query.executeQuery();
	        while (rows.next()) {
	        	roomid = rows.getInt(1);
	        	description = rows.getString(2);
	        	creator = rows.getString(3);
	        }
	        query = conn.prepareStatement(
			        "SELECT userid FROM users JOIN rooms ON users.userloc = rooms.roomid WHERE roomid = ?");
			query.setInt(1, roomid);
			rows = query.executeQuery();
			while (rows.next()) {
				usersList.add(rows.getString("userid"));
			}
			query = conn.prepareStatement(
			        "SELECT action FROM passages WHERE sourceid = ?");
			query.setInt(1, roomid);
			rows = query.executeQuery();
			while (rows.next()) {
				actionsList.add(rows.getString("action"));
			}
			return new Room(roomid , description, creator, usersList, actionsList);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new Room(roomid, description, creator, usersList, actionsList);
		
        
	}

	public boolean doAction(String userId, String action) {
		PreparedStatement query;
		Integer destid = null;
		
		try {
			query = conn.prepareStatement(
			        "SELECT destid FROM passages WHERE action = ?");
			query.setString(1, action);
	        rows = query.executeQuery();
	        while (rows.next()) {
	        	destid = rows.getInt("destid");
	        }
			query = conn.prepareStatement(
			        "UPDATE users SET userloc = ? WHERE userid = ?");
			query.setInt(1, destid);
			query.setString(2, userId);
	        int worked = query.executeUpdate();
	        if (worked == 0) {
	        	return false;
	        } else {
	        	return true;
	        }
	        
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
		
	}

	public boolean createRoom(String userId, String action, String description) {
		PreparedStatement query;
		Integer sourceid = null;
		String Description = description;
		String Action = action;
		try {
			query = conn.prepareStatement("SELECT userloc FROM users WHERE userid = ?");
			query.setString(1, userId);
			rows = query.executeQuery();
			while (rows.next()) {
				sourceid = rows.getInt(1);
			}
			int max = 9999;
			int min = 0;
			int range = (max - min) + 1; 
			int randomRoomid = (int) (Math.random()*range);
			query = conn.prepareStatement("SELECT roomid FROM rooms WHERE roomid = ?");
			query.setInt(1, randomRoomid);
			rows = query.executeQuery();
			while (rows.next()) {
				randomRoomid = (int) (Math.random()*range);
				query.setInt(1, randomRoomid);
				rows = query.executeQuery();
			}
			query = conn.prepareStatement("INSERT INTO rooms(roomid, creator, description) VALUES(?,?,?)");
			query.setInt(1, randomRoomid);
			query.setString(2, userId);
			query.setString(3, Description);
			int inserted = query.executeUpdate();
			if (inserted != -1) {
				query = conn.prepareStatement("INSERT INTO passages VALUES(?,?,?)");
				query.setInt(1, sourceid);
				query.setString(2, Action);
				query.setInt(3, randomRoomid);
				int works = query.executeUpdate();
				if (works != -1) {
						return true;				
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
		
	}

	public boolean createPassage(String userId, String action, int dst) {
		PreparedStatement query;
		Integer sourceid = null;
		String userid = userId;
		String Action = action;
		int Destination = dst;
		
		try {
			query = conn.prepareStatement("SELECT userloc FROM users WHERE userid = ?");
			query.setString(1, userid);
			rows = query.executeQuery();
			while (rows.next()) {
				sourceid = rows.getInt(1);
			}
			query = conn.prepareStatement("SELECT roomid FROM rooms WHERE roomid = ?");
			query.setInt(1, Destination);
			ResultSet working = query.executeQuery();
			if (working.next()) {
				query = conn.prepareStatement("INSERT INTO passages(sourceid, action, destid) VALUES(?,?,?)");
				query.setInt(1, sourceid);
				query.setString(2, Action);
				query.setInt(3, Destination);
				Integer worked = query.executeUpdate();
				if (worked != -1) {
						return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
}
