package mush;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;


public class JdbcSample {
    
    public static void main(String[] args) {
        Connection conn = openConnection();
        try {
            createTables(conn);
            System.out.println("get it");
           
        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if(conn != null) conn.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
    }
    
    private static Connection openConnection() {
        try {
            Class.forName("org.sqlite.JDBC"); // This loads the driver
        } catch (ClassNotFoundException e1) {
            System.err.println("SqliteJDBC library not found. "
                    + "Perhaps you need to set the build path?");
            System.exit(-1);
        }
            
        try {
            // Note that the URL below specifies the filename for storing the DB
            return DriverManager.getConnection("jdbc:sqlite:mush.db");
        } catch (SQLException e) {
            System.err.println("Could not connect to DBMS.");
            System.exit(-1);
        }
        return null; // we'll never get here, but the compiler insists
    }

    private static void createTables(Connection conn) throws SQLException {
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("DROP TABLE IF EXISTs users");
        stmt.executeUpdate("DROP TABLE IF EXISTS rooms");
        stmt.executeUpdate("DROP TABLE IF EXISTS passages");
        stmt.executeUpdate("CREATE TABLE users(userid STRING, userloc INTEGER)");
        stmt.executeUpdate("CREATE TABLE rooms(roomid INTEGER, creator STRING, description STRING)");
        stmt.executeUpdate("CREATE TABLE passages(sourceid INTEGER, action STRING, destid INTEGER)");
        
		
    }

    
}