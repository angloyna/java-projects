package mush;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

public class DbInterfaceTest {

	@Test
	public void testCreateRoom() {
		DbInterface dbint = new DbInterface();
		boolean test = dbint.createRoom("angela", "kick ass", "sometimes you just need to kick ass.");
		assertEquals(true, test);
		PreparedStatement query;
		try {
			query = dbint.conn.prepareStatement("SELECT roomid, creator, description FROM rooms");
			ResultSet rows;
			rows = query.executeQuery();
			while(rows.next()) {
				System.out.println(rows.getInt(1) + " " + rows.getString(2) + " " + rows.getString(3));
			}
			query = dbint.conn.prepareStatement("SELECT * FROM passages");
			ResultSet rowtwo = query.executeQuery();
			while (rowtwo.next()) {
				System.out.println("This should work.");
				System.out.println(rowtwo.getInt(1) + " " + rowtwo.getString(2) + rowtwo.getInt(3));
			}
			//query = dbint.conn.prepareStatement("INSERT INTO rooms(roomid, creator, description) VALUES(2, 'angela', 'this room is a bullshit one i made up.')");
			//query.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Test
	public void testGetCurRoom() {
		DbInterface dbint = new DbInterface();
		//boolean test = dbint.createUser("angela");
		//if (test == false) {
			//System.out.println("Your create user did not work");
		//} else {
			Room curRoom = dbint.getCurrentRoom("angela");
			System.out.println("This is the current room of angela: " + curRoom.getDescription());
			assertEquals("This room is a bullshit one i made up.", curRoom);
		//}
	}
	
	@Test
	public void testDoAction() throws SQLException {
		DbInterface dbint = new DbInterface();
		//PreparedStatement makeRoom = dbint.conn.prepareStatement("INSERT INTO rooms(roomid, creator, description) VALUES(3, 'angela', 'this is a second room i made up so as to create a viable action.')");
		//makeRoom.executeUpdate();
		//PreparedStatement makeAction = dbint.conn.prepareStatement("INSERT INTO passages(sourceid, action, destid) VALUES(2, 'motherfucker', 3)");
		//makeAction.executeUpdate();
		dbint.doAction("angela", "motherfucker");
		Room curRoom = dbint.getCurrentRoom("angela");
		assertEquals(3, curRoom.getId());
	}
	
	@Test
	public void testCreatePassage() throws SQLException {
		DbInterface dbint = new DbInterface();
		//dbint.createPassage("angela", "fuck", 2);
		//dbint.doAction("angela", "fuck");
		assertEquals(2, dbint.getCurrentRoom("angela").getId());
	}
	
	@Test
	public void testRoomCreate() throws SQLException {
		Connection conn = DriverManager.getConnection("jdbc:sqlite:mush.db");
		PreparedStatement stmt;
		int defaultroom = 0;
		stmt = conn.prepareStatement("INSERT INTO rooms(roomid, description) VALUES(?, ?)");
		stmt.setInt(1,defaultroom);
		stmt.setString(2, "this is the lobby.");
		int executed = stmt.executeUpdate();
		System.out.println("exected: " + executed);
	}

}
